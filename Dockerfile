FROM monstercommon

ADD lib /opt/MonsterDocker/lib/
ADD etc /etc/monster/
RUN INSTALL_DEPS=1 /opt/MonsterDocker/lib/bin/docker-install.sh && /opt/MonsterDocker/lib/bin/docker-test.sh

ENTRYPOINT ["/opt/MonsterDocker/lib/bin/docker-start.sh"]
