var mountPath = process.argv[2];
if(!mountPath)
   throw new Error("Usage: $0 mountPath");

var fuse;

var worker;
var app = {
    config: {},
    fuse: fuse,
    mknodTable: {},
};
const operations = [
"access", "truncate", 
"ftruncate", "getattr", 
"fgetattr", "flush", 
"fsync", "fsyncdir", 
"readdir", "readlink", 
"chown", "chmod", 
"mknod", "setxattr", 
"getxattr", "listxattr", 
"removexattr", "statfs", 
"open", "opendir", 
"read", "write", 
"release", "releasedir", 
"create", "utimens", 
"unlink", "rename", 
"link", "symlink", 
"mkdir", "rmdir", 
];

var fuseOptions = {
  options: ['allow_other,nonempty'],
};
operations.forEach(op=>{
   fuseOptions[op] = function(){
      if(app.config.debug) {
         const fs = require('fs');
         fs.appendFile('/tmp/fuse.txt', op+": "+ JSON.stringify(arguments)+"\n");
      }

      if(worker[op])
          return worker[op].apply(null, arguments);
      var cb = arguments[arguments.length-1];
      return cb(); // fuse.EINVAL;
   }
})

reinit();
fuse.mount(mountPath, fuseOptions, function (err) {
  if (err) throw err
  console.log('filesystem mounted on ' + mountPath)
})

process.on('SIGINT', function () {
  fuse.unmount(mountPath, function (err) {
    if (err) {
      console.log('filesystem at ' + mountPath + ' not unmounted', err)
    } else {
      console.log('filesystem at ' + mountPath + ' unmounted')
    }
  })
})

process.on('SIGHUP', function () {  
    console.log('HUP received, reloading worker');
    reinit();
})

function reinit() {
  const path = require("path");
  try{
     var workerPath = require.resolve(path.join( __dirname, "fuse-worker.js"));
     delete require.cache[workerPath];
     var lib = require(workerPath);
     worker = lib(app);    
  }
  catch(ex){
     console.error("Unable to load the fuse-worker library", ex);
  }

  try{
     var configPath = require.resolve("/etc/monster/fuse.json");
     delete require.cache[configPath];
     var config = require(configPath);
     app.config = config;    
  }
  catch(ex){
     console.error("Unable to load the config file", ex);
  }

  if(!fuse)
     fuse = app.fuse = require('/opt/MonsterDocker/lib/fuse-bindings-'+app.config.debian_version);
}
