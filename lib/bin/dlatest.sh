#!/bin/bash

IMAGE=$1
if [ -z "$IMAGE" ]; then
   echo "Usage: $0 imagename"
   exit 1
fi

LATEST=$(/usr/local/sbin/json-key.py /etc/monster/docker.json latest_images_path)
VERSION=$(/usr/local/sbin/json-key.py $LATEST $IMAGE)
if [ ! -z "$VERSION" ]; then
   echo "$IMAGE:$VERSION"
fi
