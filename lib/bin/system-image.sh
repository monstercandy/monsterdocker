#!/bin/bash

if [ -z "$1" ]; then
  echo "Usage: $0 image-name [force]"
  exit 1
fi

payload='{}'
if [ ! -z "$2" ]; then
	payload='{"force":1}'
fi

perl /opt/client-libs-for-other-languages/internal/perl/generic-light.pl docker POST /docker/root/system/$1/start $payload
