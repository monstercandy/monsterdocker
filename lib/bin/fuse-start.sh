#!/bin/bash

MOUNTPOINT=/var/run/mc-app-sockets
DIR=$(realpath "$(dirname $0)/..")



while true; do
  if [ ! -d "$MOUNTPOINT" ]; then
    echo "Mount point does not exists, creating: $MOUNTPOINT"
    mkdir "$MOUNTPOINT"
    if [ $? != 0 ]; then
      echo "Creating the mount point failed, even though it was not resolved as a directory. Endpoint is probably stale. Unmounting"
      fusermount -u "$MOUNTPOINT"
      umount "$MOUNTPOINT"
    fi
  fi

  pgrep -f fuse.js >/dev/null
  if [ $? == 1 ]; then
     echo "fuse.js not running, starting it in the background"
     nohup setsid node "$DIR/fuse.js" "$MOUNTPOINT" </dev/null >/dev/null 2>/dev/null &
  fi
  sleep 60
done
