#!/bin/bash

set -e

. /opt/MonsterCommon/lib/common.inc

mkdir -p "/var/lib/monster/$INSTANCE_NAME/unix-server"
mkdir -p "/var/lib/monster/$INSTANCE_NAME/dsh"
mkdir -p "/var/log/monster/$INSTANCE_NAME"
