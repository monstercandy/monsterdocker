module.exports = function(app) {

    const MError = require("MonsterError")
    var ValidatorsLib = require("MonsterValidators")
    var vali = ValidatorsLib.ValidateJs()

    var router = app.ExpressPromiseRouter();

    const userId = getUserIdLib();

    const default_timeout = app.config.get("exec_timeout_ms");

    const token = require("Token");
    const path = require("path");
    const dotq = require("MonsterDotq");
    const fs = dotq.fs();
    const del = require("MonsterDel");

    const SLEEP_CMD = "/bin/sleep";

    const proftpd_group_gid = userId ? userId.gid("proftpd") : 65533;

    const tech_admin_email = app.config.get("tech_admin_email");
    const webapp_gid = app.config.get("webapp_gid");

    const monster_email_path = "/var/lib/monster/email";

    const docker_hosts_file_webserver_app_only = app.config.get("docker_hosts_file_webserver_app_only");

    const dsh_server_listen_dir = app.config.get("dsh_server_listen_dir");

    const restartNever = {
          "Name": "no",
          "MaximumRetryCount": 0,
    };

    var fqdn = app.config.get("mc_server_name")+"."+app.config.get("service_primary_domain");

    const mc_socket_dir = app.config.get("mc_socket_dir");

    var webstoreConfigsSystemImages = {};


    var runningTasks = {};
    var timers = {};

    var public_ip = app.config.get("public_ip");
    var extraHosts = [];
    extraHosts.push(app.config.get("mc_server_name")+":"+public_ip);
    extraHosts.push(app.config.get("mc_server_name")+"."+app.config.get("service_primary_domain")+":"+public_ip);
    for(var q of app.config.get("docker_extra_hosts")) {
      extraHosts.push(q+":"+public_ip);
    }

// we do not offer a temporary filesystem anymore
//         Tmpfs: { "/tmp": "rw,noexec,nosuid,size="+app.config.get("container_tmpfs_size_kb")+"k" },
    const basicContainerConstraints = {
       HostConfig: {
         Dns: app.config.get("docker_dns_resolvers"),
         DnsSearch: ["."],
         NetworkMode: "host",
         ExtraHosts: extraHosts,
         LogConfig: { "Type": "json-file", "Config": {"max-size": "3m", "max-file": "2"} },
         ShmSize: app.config.get("container_shm_size_mb")*1024*1024,
         Memory: app.config.get("container_memory_limit_mb")*1024*1024,
         MemorySwap: app.config.get("container_memory_swap_limit_mb")*1024*1024,
         Ulimits: app.config.get("container_ulimits"),        
       }
    };

    const defaultBindMounts = [
       '/opt/MonsterDocker/lib/misc/cpuinfo.txt:/proc/cpuinfo:ro'
    ];


    const imageParamConstraints = {
       "-globalssh": {
          preCreate: function(req, re){
              re.containerCreateParams.HostConfig.Binds.push(app.config.get("global_ssh_path")+":/host");
              addStorageByConfig(req, re, "docker_socket_path");
              return stopOldContainerWhenUpgrading(req, re)                
          },
       },
       "-dropbear": {
          validate: function(req, re){

              return shellCommonConstraints(req, re)
                .then(()=>{

                   return vali.async(req.body.json, {publicKeys:{presence: true, isString: true, isSshKeys: true}})
                })
                .then(a=>{
                   extend(re.volumeParams.createdAs, a);
                   return re;
                })
          },
          preCreate: function(req, re){

               // shall save the public key
               var hostDir;
               return addHostRun(re)
                 .then(ahostDir=>{
                    hostDir = ahostDir;

                    return fs.mkdirAsync(path.join(hostDir, "dropbear"))
                 })
                 .then(()=>{
                    return fs.mkdirAsync(path.join(hostDir, "home"))
                 })
                 .then(()=>{
                    return fs.mkdirAsync(path.join(hostDir, "home", ".ssh"))
                 })
                 .then(()=>{
                    var fp = path.join(hostDir, "home", ".ssh", "authorized_keys");
                    return fs.writeFileAsync(fp, re.volumeParams.createdAs.publicKeys);
                 })
                 .then(()=>{
                    return createPasswordEntry(req, re);
                 })
                 .then(()=>{
                    // creating the ssh keys on the volume
                    if((re.upgradeInfo)&&(re.upgradeInfo.volume)) {
                       // we need to copy the original host ssh keys
                       return copyOriginalHostSshKeys(re);
                    }

                    var p = {
                      image: re.basicParams.image,
                      timeout_ms: default_timeout,
                      command: ["/usr/local/bin/genkeys", ""+req.webhosting.wh_id],
                      autoRemove: true
                    }

                    // console.log("SHIIIIIIIIIT", re)

                    // this one will be executed as root,
                    // so that it can write the volume and add the ssh keys
                    return app.runInNewContainer_fast(p, {
                      HostConfig: {
                        Binds: [
                          hostDir+":/host"
                        ]
                      }
                    })
                    .then((r)=>{
                       console.log("result of the SSH key generation", r);
                       if(r.StatusCode != 0)
                         throw new MError("SSH_KEY_GEN_FAILED");

                       re.finalResult.ssh = r;
                    })

                 })
                 .then(()=>{
                     return findFreePort(re);
                  })
                 .then((external)=>{
                    re.containerCreateParams.Cmd.push(""+external);

                    return stopOldContainerWhenUpgrading(req, re);
                 })

               
          },
          preStart: function(container){
               // shall start socat when in attach mode
               return Promise.resolve()
                 .then(()=>{
                    return startSocatWhenNeeded(container);                  
                 })
          },
          postStop: function(container){
               // shall kill socat when in attach mode
               return stopSocat(container);
          }
       },
       "-shellinabox": {
          validate: function(req, re){
               return shellCommonConstraints(req, re)
                 .then(re=>{
                    re.containerCreateParams.HostConfig.RestartPolicy = restartNever;
                    return re;
                 })
          },
          preCreate: function(req, d){
               // shall create the passwd entry when it is in attach mode
               return createPasswordEntry(req, d)
                 .then(()=>{
                    d.containerCreateParams.Env.push("SERVICE_URL=/docker/shellinabox/"+d.volumeParams.volumeInfo.name+"/");
                    var socketInfo = addMountForSocketDir(d);
                    d.volumeParams.shellinaboxUnixSocket = socketInfo.socket;

                    if(d.volumeParams.createdAs.type != "attach") return;

                    return addHostRun(d);
                 })
                 .then(()=>{
                    return addDsh(req, d);
                 })
          },
          preStart: function(container){
               // shall start socat when in attach mode
               return startSocatWhenNeeded(container)
                 .then(()=>{
                    // shall invoke shutdown with x minutes of countdown - along with removal
                    return shutdownContainer(container, {autoRemove: true, countdownSeconds: app.config.get("webshell_countdown_seconds")});
                 })

          },
          postStop: function(container){
               // shall kill socat when in attach mode
               return stopSocat(container);
          }
       },

       // note this is after -shellinabox so they will match correctly
       "-shell": {
          preCreate: function(req, re){

                return addDsh(req, re);
          },
       },


       // and thats it ;-)
       "-node": {
          validate: function(req, re){
               return vali.async(req.body.json, {
                  appdir: {presence: true, isPath: true}
               })
               .then(d=>{
                   extend(re.volumeParams.createdAs, d);
                   var homedir = "/web"+d.appdir;
                   re.containerCreateParams.Env.push("HOME="+homedir);
                   re.containerCreateParams.Cmd = [homedir];

                   re.volumeDirectoryNeeded = true;
               })               
          },
          preCreate: function(req, d){
             return Promise.resolve()
               .then(()=>{
                  addOsTruststores(req, d);
                  addMountForSocketDirAndRemember(d);
               })
          }
       },

       "-python": {
          validate: function(req, re){
               return vali.async(req.body.json, {
                  appdir: {presence: true, isPath: true},
                  subdir: {isString: true, default: {value: ""}},
                  wsgi: {presence: true, isString: true},
               })
               .then(d=>{
                   extend(re.volumeParams.createdAs, d);
                   var homedir = "/web"+d.appdir;
                   re.containerCreateParams.Env.push("HOME="+homedir);
                   re.containerCreateParams.Cmd = [homedir, d.subdir, d.wsgi];

                   re.volumeDirectoryNeeded = true;
               })
          },
          preCreate: function(req, d){
             return Promise.resolve()
               .then(()=>{
                  addOsTruststores(req, d);
                  addMountForSocketDirAndRemember(d);
               })
          }
       },


       "-php-": {
          /*
          // we dont need to validate anything, really
          // maybe later some additional flags like opcache on/off
          */
          validate: function(req, re){
               return Promise.resolve()
                 .then(()=>{
                    re.volumeDirectoryNeeded = true;

                    const php_memory_limit = app.config.get("php_container_memory_limit_mb");
                    const php_swap_limit = app.config.get("php_container_memory_swap_limit_mb");
                    if((php_memory_limit)||(php_swap_limit)) {
                       if(!re.containerCreateParams.HostConfig) {
                          re.containerCreateParams.HostConfig = {};                        
                       }
                       if(php_memory_limit) {
                          re.containerCreateParams.HostConfig.Memory = php_memory_limit*1024*1024;
                       }
                       if(php_swap_limit) {
                          re.containerCreateParams.HostConfig.MemorySwap = php_swap_limit*1024*1024;                        
                       }
                    }
                 })
          },
          preCreate: function(req, d) {
              // the caller expects a promise as result, but we are quite synchronous here
              return Promise.resolve()
                .then(()=>{
                    addMountForSocketDirAndRemember(d);

                    addOsTruststores(req, d);

                    if(!d.containerCreateParams.HostConfig.CapAdd) {
                      d.containerCreateParams.HostConfig.CapAdd = [];
                    }
                    d.containerCreateParams.HostConfig.CapAdd.push("SYS_PTRACE");

                    // we need to create some tweaks in the php-fpm.config
                    return createPhpFpmConfig(req.webhosting, d);
                })
                .then(()=>{
                    return createMsmtpConfig(req.webhosting, d);
                })
          },
          postCreate: function(req, d, container){

             if((!container.inspect.GraphDriver)||(!container.inspect.GraphDriver.Data)||(!container.inspect.GraphDriver.Data.UpperDir))
                return;

             var hostMainDir = container.inspect.GraphDriver.Data.UpperDir;
             var symlinks = {};
             return Promise.resolve()
               .then(()=>{
                   extend(symlinks, req.webhosting.wh_docker_hints.php_legacy_symlinks);
                   extend(symlinks, d.volumeParams.php_legacy_symlinks);
                   return dotq.linearMap({array: Object.keys(symlinks), catch: true, action: (src)=>{

                       var dst = symlinks[src];
                       var fullSrc = path.join(hostMainDir, src);
                       console.log("PHP symlinking from", fullSrc, "to", dst);
                       return fs.symlinkAsync(dst, fullSrc);

                   }})
               })
          }
       },
       "installatron": {
          preCreate: function(req,d){
             addStorageByConfig(req, d, "installatron_lib_dir");

             return stopOldContainerWhenUpgrading(req, d);      
          },
       },
       "apache": {
          upgradeSlowStopSignal: "SIGWINCH",

          preCreate: function(req,d){
             
             addFqdn(d);
             d.containerCreateParams.Env.push("SERVERADMIN="+tech_admin_email);

             processHungry(d);
             // same as with nginx; the number of processes could hit the default 256 MB limit:
             memoryHungry(d);

             addStorageByConfig(req, d, "apache_lib_dir");
             addStorageByConfig(req, d, "apache_log_dir");

             addEtcPasswdAndGroup(d);

             addAllWebStoragesAsBindMounts(d);

             // and it also needs the php sockets to be able to dispatch the web requests
             addHostDirToBindsAsIs(d, mc_socket_dir);
          },

       },
       "szar-backup-server": {
          preCreate: function(req,d){
             szarStuff(req, d);
          }
       },
       "szar-backup-web": {
          preCreate: function(req,d){
             szarStuff(req, d);
          }
       },       
       "nginx": {
          upgradeSlowStopSignal: "SIGQUIT",

          preCreate: function(req,d){

             addOsTruststores(req, d);

             addStorageByConfig(req, d, "nginx_lib_dir");
             addStorageByConfig(req, d, "nginx_log_dir");
             addStorageByConfig(req, d, "frontend_lib_dir");
             addStorageByConfig(req, d, "mrtg_lib_dir");
             addStorageByConfig(req, d, "ssl_docroot_dir");
       

             addCertificatesMountDir(req, d);

             addEtcPasswdAndGroup(d);

             addAllWebStoragesAsBindMounts(d);

             // and it also needs the sockets (to communicate with python webapps)
             addHostDirToBindsAsIs(d, mc_socket_dir);

             // nginx can eat up 30*number of cpus, which could be more than the default 256MB limit
             // and it could lead to swapping on old kernels (and new kernels would just OOM-kill the process)
             memoryHungry(d);
          },

       },

       "redis": {
          preCreate: function(req,d){

             return token.GetTokenAsync(16, "hex")
               .then(id=>{
                   d.containerCreateParams.Env.push("CONTAINER_ID="+id);
               })
          }
       },

       "mariadb": {
          preCreate: function(req,d){

             addCertificatesMountDir(req, d);

             addStorageByConfig(req, d, "mariadb_data_dir");
             addStorageByConfig(req, d, "mariadb_run_dir");
             addStorageByConfig(req, d, "mariadb_lib_dir");
             addStorageByConfig(req, d, "mariadb_log_dir");
             addStorageByConfig(req, d, "mariadb_include_dir");

             addEtcPasswdAndGroup(d);

             processHungry(d);

             memoryHungry(d);

             return stopOldContainerWhenUpgrading(req, d);
          }
       },


       "dovecot": {
          preCreate: function(req,d){

             d.containerCreateParams.Env.push("MM_FQDN="+fqdn);
             Array("db-postfix-dovecot", "db-dovecot-dict").forEach(n =>{
                addEmailDirToBinds(d, n);
             });
             addAllMailStoragesAsBindMounts(d);

             Array("lmtp", "sasl-auth").forEach(x=>{
                 addMountForSocketDir(d, {groupMode: "sys-dovecot-"+x, target: "/var/spool/postfix/private/dovecot-"+x});              
             })

             addStorageByConfig(req, d, "dovecot_lib_dir");

             addCertificatesMountDir(req, d);

             addEtcPasswdAndGroup(d);

             processHungry(d);

             memoryHungry(d);

             // reuse_port is broken in dovecot 2.2
             return stopOldContainerWhenUpgrading(req, d);
          }
       },

       "postfix": {
          preCreate: function(req,d){

             d.containerCreateParams.Env.push("MM_FQDN="+fqdn);
             d.containerCreateParams.Env.push("MM_PUBLIC_IP="+public_ip);

             addOsTruststores(req, d);

             addEmailDirToBinds(d, "db-postfix-dovecot");
             addStorageByConfig(req, d, "postfix_conf_d_dir");
             addStorageByConfig(req, d, "postfix_spool_dir");
             addStorageByConfig(req, d, "postfix_lib_dir");
             

             Array("lmtp", "sasl-auth").forEach(name=>{
                 var hostDir = path.posix.join(mc_socket_dir, "sys-dovecot-"+name);
                 var target = "/var/spool/postfix/private/dovecot/"+name;
                 addBindMount(d, hostDir, target);
             })

             addCertificatesMountDir(req, d);

             addEtcPasswdAndGroup(d);

             processHungry(d);

             memoryHungry(d);

             return stopOldContainerWhenUpgrading(req, d);
          }
       },

       "proftpd": {
          preCreate: function(req,d){
             
             addFqdn(d);
             d.containerCreateParams.Env.push("PROFTPD_GROUP_GID="+proftpd_group_gid);

             // proftpd doesnt need them (otherwise MasqueradeAddress/PASV will be broken):
             d.containerCreateParams.HostConfig.ExtraHosts = [];

             addStorageByConfig(req, d, "mc_ftp_databases_dir");
             addStorageByConfig(req, d, "proftpd_lib_dir");
             addStorageByConfig(req, d, "proftpd_log_dir");
             addCertificatesMountDir(req, d);

             addEtcPasswdAndGroup(d);

             addAllWebStoragesAsBindMounts(d);

             // log lines will go to dedicated files, turning it off at docker level:
             turnOffLogging(d);

             return stopOldContainerWhenUpgrading(req, d);
          }
       },
       "amavisd-new": {
          preCreate: function(req,d){
             addAmavisdStorage(req,d);

             Array("spamassassin_storage_directory",
                   "amavisd_spamassassin_conf",
                   "amavisd_dkim_keys_path").forEach(x=>{
                addStorageByConfig(req,d, x);
             })

             
             d.containerCreateParams.Env.push("FQDN="+fqdn);
             Array("db-postfix-dovecot", "db-amavis").forEach(n =>{
                addEmailDirToBinds(d, n);
             });
             d.containerCreateParams.Env.push("AMAVISD_NO_UPDATE=1");

             // using local dnscache to resolve stuff (needed for uribl to avoid URIBL_BLOCKED spamassassin hits)
             d.containerCreateParams.HostConfig.Dns = ["127.0.0.1"];

             // this is a hungry application
             memoryHungry(d);
          }
       },
       "clamav-freshclam": {
          preCreate: function(req,d){
             addStorageByConfig(req,d, "clamav_lib_dir");

             // this db upgrader (!) crap could eat up hundreds of mbytes of memory as well
             // for now, lets grant more memory to it until the per cgroup memory limitation
             // goes online in all the kernels we are running
             memoryHungry(d);
          }
       },
       "clamav-clamd": {
          preCreate: function(req,d){
             // this includes the temp dir of amavisd, where the files to be scanned will be saved
             addAmavisdStorage(req,d); 
             
             // this is where the virus definitions are stored
             addStorageByConfig(req,d, "clamav_lib_dir");

             // this is needed so that proftpd can send some files to be scanned
             addAllWebStoragesAsBindMounts(d);
             
             // this is a hungry application
             memoryHungry(d);

             return stopOldContainerWhenUpgrading(req, d);
          }
       },
       "amavisd-milter": {
          preCreate: addAmavisdStorage,
       },
       "anti-backscatter": {},
       "cluebringer": {
          preCreate: function(req, d){
              addEmailDirToBinds(d, "db-cluebringer");
              d.containerCreateParams.Env.push("CLUEBRINGER_NO_CLEANUP=1");
          }
       },

       "dnscache": {
          preCreate: function(req, d){
              // logging the request of dns resolution makes no sense, turning it off.
              addEtcPasswdAndGroup(d);
              turnOffLogging(d);

              // tcp 53 is reused, must stop the old first
              return stopOldContainerWhenUpgrading(req, d);
          }
       },

       "tinydns": {
          preCreate: function(req, d){
              addEtcPasswdAndGroup(d);
              addStorageByConfig(req,d, "tinydns_lib_dir");
              turnOffLogging(d);
          }
       },
       "axfrdns": {
          preCreate: function(req, d){
              addEtcPasswdAndGroup(d);
              addStorageByConfig(req,d, "axfrdns_lib_dir");
              addStorageByConfig(req,d, "tinydns_lib_dir");
              turnOffLogging(d);

              // tcp 53 is reused, must stop the old first
              return stopOldContainerWhenUpgrading(req, d);
          }
       },
    }

    function szarStuff(req, d){
         addEtcPasswdAndGroup(d);
         addOsTruststores(req, d);
         addCertificatesMountDir(req, d);
         addStorageByConfig(req, d, "szar_server_storage_directory");
         addStorageByConfig(req, d, "szar_server_log_directory");
         return addStorageByConfig(req, d, "szar_backup_config_dir");
    }

    function addOsTruststores(req, d){
        app.config.get("os_truststore_dirs").forEach(dir=>{
            addHostDirToBindsAsIs(d, dir);
        })
    }

    function addDsh(req, re){
        if(!dsh_server_listen_dir) return;

        return token.GetTokenAsync(16, "hex")
         .then(dsh_token=>{
            dsh_token = req.webhosting.wh_id+"-"+dsh_token;
            re.volumeParams.dsh_token = dsh_token;
            re.containerCreateParams.Env.push("DSH_SECRET="+dsh_token);
            re.containerCreateParams.HostConfig.Binds.push(dsh_server_listen_dir+":/var/run/dsh");
          })
    }

    function addCertificatesMountDir(req, d) {
         addStorageByConfig(req, d, "certificates_active_dir");
         addStorageByConfig(req, d, "certificates_mc_dir");
    }

    function addFqdn(d){
        d.containerCreateParams.Env.push("FQDN="+fqdn);
    }

    function addEtcPasswdAndGroup(d){
             addHostDirToBindsAsIs(d, "/etc/passwd", true);
             addHostDirToBindsAsIs(d, "/etc/group", true);
    }

    function addAllStoragesAsBindMounts(d, k){
             app.config.get("storages").forEach(strg=>{
                var v = strg[k];
                if(!v) return;

                addHostDirToBindsAsIs(d, v);
             })
    }

    function addAllWebStoragesAsBindMounts(d){
        return addAllStoragesAsBindMounts(d, "web");
    }
    function addAllMailStoragesAsBindMounts(d){
        return addAllStoragesAsBindMounts(d, "mail");
    }

    function stopOldContainerWhenUpgrading(req,d){
         if(!d.upgradeInfo) return;

         // this is a replacement dropbear/proftpd/whatever container.
         //
         // this service is exposed on the host network
         // and it is supposed to come back on the same
         // port, so we need to stop the original container
         // instead of running a parallel instance of it
         // as it does not support SO_REUSEPORT
         console.log("Stopping container before upgrade")
         return stopContainer(d.upgradeInfo.oldContainer)
    }

    function addHostDirToBindsAsIs(d, path, readonly){      
       var expression = path+":"+path;
       if(readonly) expression += ":ro";
       d.containerCreateParams.HostConfig.Binds.push(expression);
    }
    function turnOffLogging(id){
       id.containerCreateParams.HostConfig.LogConfig = {Type: "none"};
    }

    function addAmavisdStorage(req, d){
       return addStorageByConfig(req,d, "amavisd_storage_directory");
    }

    function addStorageByConfig(req, d, name){
       var hostDir = app.config.get(name);
       addHostDirToBindsAsIs(d, hostDir);
       return hostDir;
    }
    function processHungry(d){
         // apache/mariadb cant start enough threads otherwise
         d.containerCreateParams.HostConfig.Ulimits = [];      
    }
    function memoryHungry(d){
         d.containerCreateParams.HostConfig.Memory = 0;
         d.containerCreateParams.HostConfig.MemorySwap = -1;
    }

    function addEmailDirToBinds(d, name){
        var fullfp = path.join(monster_email_path, name);
        d.containerCreateParams.HostConfig.Binds.push(fullfp+":"+fullfp);      
    }

    function addHostDirToBinds(re){
       var hostDir = path.join(re.volumeParams.volumeInfo.path, "host");
       var hostDirLine = hostDir+":/host";
       if(re.containerCreateParams.HostConfig.Binds.indexOf(hostDirLine) < 0)
          re.containerCreateParams.HostConfig.Binds.push(hostDirLine);
       return hostDir;      
    }

    function addHostDirToBindsAndCreate(re){
       var hostDir = path.join(re.volumeParams.volumeInfo.path, "host");
       var hostDirLine = hostDir+":/host";
       if(re.containerCreateParams.HostConfig.Binds.indexOf(hostDirLine) < 0)
          re.containerCreateParams.HostConfig.Binds.push(hostDirLine);
       return fs.mkdirAsync(hostDir)
         .catch(ex=>{
            if(ex.code == "EEXIST") return;
            throw ex;
         })
         .then(()=>{
            return hostDir;
         })
    }

    function createMsmtpConfig(webhosting, d) {

       var hostDir;
       return addHostDirToBindsAndCreate(d)
         .then(aHostDir=>{
             hostDir = aHostDir;
             return app.GetEmail().postAsync("/s/[this]/email/accounts/mail-service-account/fetch", {wh_id: webhosting.wh_id})
         })
         .then(re=>{
            var email = re.result;
            var this_server_name = app.config.get("mc_server_name");
            var service_primary_domain = app.config.get("service_primary_domain");
            var host = `${this_server_name}.${service_primary_domain}`;
            var data = `
account provider
host ${host}
auth plain
from ${webhosting.wh_id}@${host}
user ${email.ea_email}
password ${email.ea_password}

# Set a default account
account default : provider
`
            return fs.writeFileAsync(path.join(hostDir, "msmtp.conf"), data);
         })
         .catch(ex=>{
            console.error("We could not learn the password of the mail service account, skipping msmtp config generation", ex)
         })
    }

    function getPhpFpmLogFilePathes(webhosting){
        return token.GetTokenAsync(16, "hex")
          .then(token=>{
             var mainDir = "/web/php-fpm-logs";
             return {
              slowlog: `${mainDir}/${token}-php-fpm.slow.log`,
              accesslog: `${mainDir}/${token}-php-fpm.access.log`,
              errorlog: `${mainDir}/${token}-php-fpm.error.log`,
             }
          })
    }

    function createPhpFpmConfig(webhosting, d) {
       var hostDir;
       return addHostDirToBindsAndCreate(d)
         .then(aHostDir=>{
             hostDir = aHostDir;
             return getPhpFpmLogFilePathes(webhosting)
         })
         .then(phpPathes=>{

             var defaultExtraLines = `
slowlog = ${phpPathes.slowlog}
access.log = ${phpPathes.accesslog}

`;

             var defaultExtraLinesGlobal = `
[global]
error_log = ${phpPathes.errorlog}
`;

             var externalExtraLines = webhosting.wh_php_fpm_conf_extra_lines || "";
             if(webhosting.wh_max_execution_second) {
                externalExtraLines += `
request_terminate_timeout = ${webhosting.wh_max_execution_second}s
php_value[max_execution_time] = ${webhosting.wh_max_execution_second}
`
             }

             if((d.upgradeInfo)&&(d.upgradeInfo.volume)&&(d.upgradeInfo.volume.wh_php_fpm_conf_extra_lines)) {
                externalExtraLines = d.upgradeInfo.volume.wh_php_fpm_conf_extra_lines;
             }

             // saving it for later use (will be in upgradeInfo):
             if(externalExtraLines) {
                d.volumeParams.wh_php_fpm_conf_extra_lines = externalExtraLines;
             }

             var extraLines = defaultExtraLines + externalExtraLines + defaultExtraLinesGlobal;
             return fs.writeFileAsync(path.join(hostDir, "php-fpm.conf"), extraLines);
         })
    }

    function addHostRun(re) {
       // shall save the public key
       return addHostDirToBindsAndCreate(re)
         .then(hostDir=>{
            return fs.mkdirAsync(path.join(hostDir, "run")).then(()=>{
               return hostDir;
            })          
         })
    }

    function addMountForSocketDirAndRemember(d){
        var socketInfo = addMountForSocketDir(d);
        // saving info about the listening unix socket
        d.volumeParams.docrootParams = {
          unix_socket_path: socketInfo.socket,
        };

        return socketInfo;
    }

    function addMountForSocketDir(d, extra){
        var socketInfo = buildSocketDir(d, extra);
        addBindMount(d, socketInfo.dir, (extra||{}).target || "/var/run/mc-app-socket")
        return socketInfo;
    }

    function addBindMount(d, source, target, extra){
        var mountDetails = extend({
               "Type":     "bind", 
               "Source":   source,
               "Target":   target,
        }, extra);
        d.containerCreateParams.HostConfig.Mounts.push(mountDetails);
    }

    function buildSocketDir(d, extra){
        var name = (extra && extra.groupMode) ? path.posix.join(extra.groupMode, d.volumeParams.volumeInfo.name) : d.volumeParams.volumeInfo.name;
        var socketDir = path.posix.join(mc_socket_dir, name);
        var socketPath = path.posix.join(socketDir, "socket");
        return {dir:socketDir, socket:socketPath};
    }

    function findFreePort(re){
       return Promise.resolve()
         .then(()=>{
           if(re.volumeParams.hostPort)
              return re.volumeParams.hostPort;
           if((re.upgradeInfo)&&(re.upgradeInfo.volume)&&(re.upgradeInfo.volume.hostPort))
              return re.upgradeInfo.volume.hostPort;

           const fp = require("find-free-port");
           return fp(app.config.get("open_ports_min"), app.config.get("open_ports_max"))
             .then(ports=>{
               return ports[0];
             })
         })
         .then((external)=>{

            re.finalResult.hostPort = external;
            re.containerCreateParams.Labels[app.mcImageLabelPrefix+"hostPort"] = ""+external;
            re.volumeParams.hostPort = external;

            return external;
         })
    }

    function copyOriginalHostSshKeys(re) {
       var dstDir = path.join(re.volumeParams.volumeInfo.path, "host", "dropbear");
       var srcDir = path.join(re.upgradeInfo.volume.volumeInfo.path, "host", "dropbear");
       return fs.readdirAsync(srcDir)
         .then(files=>{
             return dotq.linearMap({array: files, action: function(f){
                var srcFile = path.join(srcDir, f);
                var dstFile = path.join(dstDir, f);
                return copyFileSameOwner(srcFile, dstFile);
             }})

         })
    }

    function copyFileSameOwner(srcFile, dstFile){
        var s;
        return fs.statAsync(srcFile)
          .then(as=>{
             s = as;
             return fs.copyFileAsync(srcFile, dstFile);      
          })
          .then(()=>{
             return fs.chownAsync(dstFile, s.uid, s.gid);
          })
    }


    router.getWebstoreConfig = function(container){
        // console.inspect("trying to resolve webstore config of", container);
        var wh_id = container.info.Labels[app.mcImageLabelPrefix+"webstore"];
        if(!wh_id) {
          if(container.info.Labels[app.mcImageLabelPrefix+app.mcAppLabel] == "system") {
            if(!webstoreConfigsSystemImages[container.Image])
              webstoreConfigsSystemImages[container.Image] = {};
            return webstoreConfigsSystemImages[container.Image];
          }
          
          return;
        }
        return app.getWebstoreConfig(wh_id, container.info.Id);
    }

    function startSocatWhenNeeded(container){
        // attach type?
        var shellType = container.info.Labels[app.mcImageLabelPrefix+"shellType"];
        console.log("startSocatWhenNeeded:", container.info.Id, shellType)
        if(shellType != "attach") return Promise.resolve();

        var targetContainer = container.info.Labels[app.mcImageLabelPrefix+"targetContainer"];

        var webstoreConfig = router.getWebstoreConfig(container);
        var volumePath = webstoreConfig.volumeInfo.path;
        var chain = app.config.get("cmd_socat_unix_listen");
        var params = [{
            socat_path: app.config.get("socat_path"), 
            target_container_id: targetContainer,
            unix_listen_socket: path.join(volumePath, "host", "run", "docker-attach.sock"),
            user: webstoreConfig.wh_id,
        }];
        console.log("spawning new socat instance", chain, params);
        return app.commander.spawn({executeImmediately:true, omitControlMessages:true,removeImmediately:true, chain: chain}, params)
            .then(h=>{
               if(!runningTasks[webstoreConfig.wh_id]) runningTasks[webstoreConfig.wh_id] = [];
               runningTasks[webstoreConfig.wh_id].push(h.id);
               h.executionPromise.catch(ex=>{
                 console.error("Socat exited:", ex)
              })
            })
    }
    function stopSocat(container){

        return Promise.resolve()
          .then(()=>{
              var webstoreConfig = router.getWebstoreConfig(container);
              if(!webstoreConfig) return; // some robustness

              console.log("aborting any running backend tasks, if any; for container", container.info.Id);
              (runningTasks[webstoreConfig.wh_id] || []).forEach(task_id=>{
                 console.log("there is a running socat instance with task_id, aborting it", task_id);
                 app.commander.abortTask(task_id);
              })

              runningTasks[webstoreConfig.wh_id] = [];

          })        
    }

    function shutdownContainer(container, params){
          if(!params) params = {};

          if(!params.shutdownAt) {
             const moment = require("MonsterMoment");
             var now = moment.nowUnixtime();
             params.shutdownAt = now + params.countdownSeconds;
          }

          // implement shutdown handler; need to save it as a volume parameter
          var webstoreConfig = router.getWebstoreConfig(container);
          webstoreConfig.timer = {autoRemove: params.autoRemove, shutdownAt: params.shutdownAt};


          // need to install a timer   
          if(params.countdownSeconds <= 0)   
             return shutDownLogic();

          params.timer = setTimeout(shutDownLogic, params.countdownSeconds*1000);

          // container
          timers[container.info.Id] = params;

          function shutDownLogic(){
              return stopContainer(container, true)
                .catch(ex=>{
                  console.error("We were unable to stop container, proceeding with removal", container.info.Id)
                })
                .then(()=>{
                    if(params.autoRemove)
                       return removeContainer(container, true);
                })
                .catch(ex=>{
                   console.error("Unable to shutdown container", container.info.Id, ex);
                })

          }
    }

    function createPasswordEntry(req, d){

        var shell = (d.volumeParams.createdAs.type != "attach" ? "/bin/sh" : "/bin/socketsh");

        var fullPasswdPath;
        // shall create the passwd entry when it is in attach mode
        var etcDir = path.join(d.volumeParams.volumeInfo.path, "etc");
        return fs.mkdirAsync(etcDir)
           .then(()=>{
              var uid = req.webhosting.wh_id;
              fullPasswdPath = path.join(etcDir, "passwd");
              return fs.writeFileAsync(
                 fullPasswdPath, 
                 `${uid}:x:${uid}:${webapp_gid}::/host/home:${shell}\n`
              );
           })
           .then(()=>{
               d.containerCreateParams.HostConfig.Binds.push(fullPasswdPath+":/etc/passwd:ro");
           })
    }

    function shellCommonConstraints(req, re){
       re.volumeDirectoryNeeded = true;

       var d;
       return vali.async(req.body.json, {
          type: {presence: true, inclusion: ["simple", "attach"]},
       })
       .then(ad=>{
          d = ad;
          extend(re.volumeParams.createdAs, d);
          re.containerCreateParams.Labels[app.mcImageLabelPrefix+"shellType"] = d.type;

          if(d.type == "attach") {
             return vali.async(req.body.json, {container:{presence: true, isString: true}})
              .then(()=>{
                 return listContainers(req)                
              })
              .then(cs=>{
                 return webstoreContainerAuthorizationRaw(req, req.body.json.container);
              })
              .then(()=>{
                 re.volumeParams.createdAs.container = req.body.json.container;
                 re.containerCreateParams.Labels[app.mcImageLabelPrefix+"targetContainer"] = re.volumeParams.createdAs.container;
              })
          }
       })
       .then(()=>{
          return re;
       })
    }


    vali.validators.isSshKeys = app.isSshKeys;

    vali.validators.webhostingCommandTimeout = function(value, options, key, attributes) {
        if(!value) return;
        if(!options.req) return;
        if(!options.req.webhosting) return;
        if(!options.req.webhosting.template.t_container_cmd_timeout_s) return;
        if(options.req.webhosting.template.t_container_cmd_timeout_s < value)
           return "too high";
    }

    vali.validators.isAllowedImage = function(value, options, key, attributes) {
        if(!value) return "image missing";
        if(!options.req) return "request is missing";
        if(options.req.preVerified) return;
        if(!options.req.container) return "source container not found";

        return getCompatibleImages(options.req.container)
          .then(compatibleImagesHash=>{
              options.req.destinationImage = compatibleImagesHash[value];
              if(!options.req.destinationImage) return "destination image is not allowed";
          })

    }

    vali.validators.canImageBeStarted = function(value, options, key, attributes) {
        if(!value) return "image missing";

        return canCreateLogic(options.req)
          .then(d=>{
              if(!d.canCreate) return "container limit reached";

              d.image = d.images[value];
              if(!d.image) {
                 if((!d.tags[value])||(!d.tags[value].latest))
                    return "unsupported image";

                 value = attributes[key] = d.tags[value].latest.RepoTags[0];
                 d.image = d.images[value];
              }

              var imageParams = d.image.Labels[app.mcParamsFull] || {};
              if((options.execType) && (imageParams.execTypes) && (imageParams.execTypes.indexOf(options.execType) < 0)) {
                 return options.execType+" is not allowed";
              }

              if((options.req.body.json.debug) && (!imageParams.debug))
                return "debug not supported";

              options.req.canCreateLogic = d;
          })
    }

    function attachWebappCommon(req, urlSuffix){
          var labels = req.container.info.Labels;
          var type = labels[app.mcImageLabelPrefix+app.mcWebAppLabel];
          if(!type)
             throw new MError("NOT_WEBAPP");

          var volume = router.getWebstoreConfig(req.container);

          var payload = extend({
            type: type,
            container: req.container.info.PrimaryName
          }, volume.docrootParams);

          if(!urlSuffix) urlSuffix = "";

          return req.sendOk(
            app.GetDocroot().postAsync("/s/[this]/docrootapi/docroots/webapps/"+req.webhosting.wh_id+"/"+urlSuffix, payload)
             .then(()=>{
                app.InsertEvent(req, {e_event_type: "docker-docrootapi"})
             })
          )
    }

      router.post("/container/:container_id/set-as-default-web", webstoreContainerAuthorization, function(req,res,next){
         return attachWebappCommon(req, "");
      })
      router.post("/container/:container_id/attach-to-vhost", webstoreContainerAuthorization, function(req,res,next){
         return attachWebappCommon(req, req.body.json.vhost);
      })

      router.post("/container/:container_id/upgrade", webstoreContainerAuthorization, function(req,res,next){
          return req.sendPromResultAsIs(router.upgradeContainerLogic(req));
      })

      router.get("/container/:container_id/info", webstoreContainerAuthorization, function(req,res,next){
          return getCompatibleImages(req.container)
            .then(compatibleHash=>{
               var containerInfo = router.containerStripper([req.container.info])[0];
               containerInfo.compatibleImages = Object.keys(compatibleHash);
               return req.sendResponse(containerInfo);
            })
      })

      router.post("/container/:container_id/shutdown", webstoreContainerAuthorization, function(req,res,next){
          return vali.async(req.body.json, {
             countdownSeconds: {
               isInteger: { isInteger: true, lessThanOrEqualTo: 60*60, default: 5*60 },
               autoRemove: { isInteger: true, lessThanOrEqualTo: 60*60, default: 5*60 },
             }
          })
          .then(d=>{
             return shutdownContainer(req.container, d);
          })
          .then(()=>{
              app.InsertEvent(req, {e_event_type: "shutdown-container"});
          })
          .then(()=>{
             return req.sendOk();
          })
      })

      router.post("/container/:container_id/logs", webstoreContainerAuthorization, function(req,res,next){
          return req.sendPromResultAsIs(
             req.container.fn.logs({stdout: true, stderr: true, tail: 1000})
          );
      })        

      router.post("/container/:container_id/stats", webstoreContainerAuthorization, function(req,res,next){
          return req.sendPromResultAsIs(
             req.container.fn.stats({stream: false})
          );
      })        

      router.post("/container/:container_id/kill", webstoreContainerAuthorization, function(req,res,next){
          return req.sendOk(
            killContainer(req.container, req.body.json)
            .then(()=>{
                app.InsertEvent(req, {e_event_type: "kill-container"});
            })
          );
      })
      router.post("/container/:container_id/restart", webstoreContainerAuthorization, function(req,res,next){
          return req.sendOk(
            restartContainer(req.container)
            .then(()=>{
                app.InsertEvent(req, {e_event_type: "restart-container"});
            })            
          );
      })
      router.post("/container/:container_id/stop", webstoreContainerAuthorization, function(req,res,next){
          return req.sendOk(
            stopContainer(req.container)
            .then(()=>{
                app.InsertEvent(req, {e_event_type: "stop-container"});
            })            
          );
      })
      router.post("/container/:container_id/start", webstoreContainerAuthorization, function(req,res,next){
          return req.sendOk(
             startContainer(req.container)
             .then(()=>{
                app.InsertEvent(req, {e_event_type: "start-container"});
             })            
          );
      })
      router.post("/container/:container_id/remove", webstoreContainerAuthorization, function(req,res,next){
          return req.sendOk(remove());

          function remove(){
             var p = req.body.json.tryToStopFirst ? stopContainer(req.container, true) : Promise.resolve();
             return p.then(()=>{
                return removeContainer(req.container)              
             })
             .then(()=>{
                app.InsertEvent(req, {e_event_type: "remove-container"});
             })   

          }

      })

    router.upgradeContainerLogic = function(req){
          var newResult;
          var imageParamsHandler;
          return vali.async(req.body.json, { 
             destination_image: { presence: true, isString: true, isAllowedImage: { req: req} } 
          })
          .then(d=>{
               var originalContainerIsRunning = req.container.info.State == "running";
               var originalImage = req.container.info.Image;

               var originalCreatePayload = {};
               var volume = router.getWebstoreConfig(req.container);
               if((volume)&&(volume.createdAs)) {
                 originalCreatePayload = simpleCloneObject(volume.createdAs);
               }
               originalCreatePayload.image = d.destination_image;
               if(originalContainerIsRunning) originalCreatePayload.start = true;

               imageParamsHandler = imageParamLookup(originalImage);

               var mreq = {
                  body: { json: originalCreatePayload },
                  webhosting: req.webhosting, 
                  canCreateLogic: {
                      image: d.destination_image,
                      canCreate: true,
                  },
                  preVerified: req.preVerified,
               };

               if((req.container.info.Labels)&&(req.container.info.Labels[app.mcImageLabelPrefix+app.mcAppLabel] == "system")) {
                     mreq.systemImage = true;          
                     var imageName = router.getImageAndTagAlways(d.destination_image);      
                     mreq.body.json.forceNameLabel = imageName.image;
               }


               // phase 1: creating the new container
               return router.newContainerLogic(mreq, {
                  oldContainer: req.container, 
                  destinationImage: d.destination_image, 
                  volume: volume
               })
          })
           .then((aResult)=>{
              newResult = aResult;

              // phase 2: replaceing the docroot references
              // decide if this is a web application and skip if it is not
              var labels = newResult.inspectedContainer.info.Labels;
              // console.inspect("XXXX", newResult);
              if(!labels[app.mcImageLabelPrefix+app.mcWebAppLabel])
                 return false;

              var oldContainer = router.containerStripper([newResult.upgradeInfo.oldContainer.info], true)[0];
              var newContainer = router.containerStripper([newResult.inspectedContainer.info], true)[0];
              var newParams = { 
                   container:newContainer.PrimaryName,
              };
              var payload = {
                oldContainer: oldContainer.PrimaryName, 
                newParams: extend(newParams, newResult.volumeParams.docrootParams),
              };
              console.log("upgrading container (the new has been created)", newResult);
              return app.GetDocroot().postAsync("/s/[this]/docrootapi/docroots/webapps/"+req.webhosting.wh_id+"/upgrade", payload);
           })
           .then(()=>{

             // phase 3: shutting down the old container

             // is this an image that requires graceful stop?
             if(imageParamsHandler.upgradeSlowStopSignal) {
                 // note: we do not return with this promise here as we want the process being asynchronous
                 // and independent from the main execution flow
                 slowlyStopOldContainerWhenNewIsStillUp(req, newResult, newResult.inspectedContainer, imageParamsHandler.upgradeSlowStopSignal)
                   .catch(ex=>{
                      if(ex.message.match(/new container is not running/)) {
                          console.log("removing the new container instead (and keep running the old one)");
                          shutdownContainer(newResult.inspectedContainer, {
                            countdownSeconds: 5, 
                            autoRemove: true
                          })
                          throw ex;
                      }

                      console.error("Error during graceful stopping the old container! Trying to shut it down anyway.", req.body.json, ex)
                   })
                   .then(()=>{
                        return shutdownAndRemove(app.config.get("upgrade_shutdown_old_container_sec"));
                   })
                   .catch(ex=>{
                      console.error("Error while disposing old container the graceful way.", req.body.json, ex)
                   })

                 return Promise.resolve();
             }

             return shutdownAndRemove(req.body.json.removeOldContainerImmediately ? 0 : app.config.get("upgrade_shutdown_old_container_sec"));

             function shutdownAndRemove(removeSeconds){
                return shutdownContainer(req.container, {
                    countdownSeconds: removeSeconds, 
                    autoRemove: true
                })

             }
           })
           .then(()=>{
              return app.globalSsh.ChangeTargetContainerIfPresent(
                  req.webhosting.wh_id, 
                  req.container.info.Id,
                  newResult.inspectedContainer.Id
              );
           })
           .then(()=>{
              app.InsertEvent(req, {e_event_type: "upgrade-container"});
           })
           .then(()=>{
              return newResult.finalResult;
           })
    }

    router.newContainerLogic = function(req, upgradeInfo){
       var d;
       var id;
       var container;
       var imageParamsHandler;
       var mcImageLabels;
       // console.inspect("upgradeInfo", upgradeInfo)
       return vali.async(req.body.json, {
            debug: {isInteger: {greaterThan: 0}},
            image: {presence: true, isString: true, canImageBeStarted: {req: req, execType: "new"}}, 
            start: {isBooleanLazy: true},
            forceNameLabel: {isString: true},
         })
         .then(ad=>{
              d = ad;

              imageParamsHandler = imageParamLookup(d.image);
              mcImageLabels = app.filterMcLabels(req.canCreateLogic.image.Labels);

              // if this is a new container create request and the same soft name already exists:
              if((!upgradeInfo)&&(d.forceNameLabel)&&(!req.systemImage)) {
                var filters = ["containerName="+d.forceNameLabel];
                filters.push("webstore="+req.webhosting.wh_id);                  

                return app.docker.listMcContainersByLabels(filters)
                  .then(containers=>{
                     if(containers.length > 0)
                      throw {safeToProceed: containers[0]};
                  })
              }

         })
         .then(ad=>{
              id = { 
                 basicParams: d,
                 containerCreateParams: {
                    Labels: {},
                    Env: [],
                    Cmd: [],
                    ExposedPorts: {},
                    HostConfig: {
                       Binds: [],
                       PortBindings: {},
                       Mounts: [],
                    }
                 },
                 volumeParams: { 
                    createdAs: d,
                    volumeInfo: {},
                 },
                 finalResult: {},
                 upgradeInfo: upgradeInfo
              };


              if((upgradeInfo)&&(upgradeInfo.cloneForWie)) {
                extend(id.volumeParams, upgradeInfo.volumeParams);
                id.upgradeInfo = null;
                id.containerCreateParams.name = upgradeInfo.name;
              }

              if(d.forceNameLabel) {
                return d.forceNameLabel;
              } 
              else if((upgradeInfo)&&(upgradeInfo.volumeParams)&&(upgradeInfo.volumeParams.containerToken)) {
                return upgradeInfo.volumeParams.containerToken;
              }
              else {
                return token.GetTokenAsync(16, "hex");
              }              
         })
         .then(nameToken=>{
            id.containerCreateParams.Labels[app.mcContainerNameLabel] = nameToken;              
            id.volumeParams.containerToken = nameToken;


            if(!imageParamsHandler.validate) return {};

            return imageParamsHandler.validate(req, id);
         })
         .then(()=>{
              // creating the volume directory

              return Promise.resolve()
                .then(()=>{
                    if(id.volumeParams.volumeInfo.name) return;

                    return token.GetTokenAsync(16, "hex")
                      .then(token=>{
                         id.volumeParams.volumeInfo.name = (req.webhosting ? req.webhosting.wh_id+"-" : "") + token;
                         id.volumeParams.volumeInfo.path = path.join(app.config.getVolumePath(), id.volumeParams.volumeInfo.name);
                      })

                })
                .then(()=>{
                   if(!id.volumeDirectoryNeeded) return;

                   return fs.mkdirAsync(id.volumeParams.volumeInfo.path)
                })

         })
         .then(()=>{
              if(!imageParamsHandler.preCreate) return;

              return imageParamsHandler.preCreate(req, id);
         })
         .then(()=>{
              // some global steps.
              var imageName = router.getImageAndTagAlways(d.image);

              // eg. running user can be overridden/forced this way
              extend(id.containerCreateParams, app.config.get("image-params-"+imageName.image));
              extend(id.containerCreateParams, app.config.get("image-params-"+d.image));
              extend(id.containerCreateParams.HostConfig, app.config.get("image-hostconfig-"+imageName.image));
              extend(id.containerCreateParams.HostConfig, app.config.get("image-hostconfig-"+d.image));

              var overrideSrc = [];              
              if((req)&&(req.webhosting)&&(req.webhosting.wh_docker_hints)) {
                 overrideSrc.push(req.webhosting.wh_docker_hints);
              }
              if((upgradeInfo)&&(upgradeInfo.volume)) {
                 overrideSrc.push(upgradeInfo.volume);
              }

              overrideSrc.forEach(src=>{
                 var v = src.DockerConfig;
                 if(v) {
                      extend(id.containerCreateParams, v);
                      id.volumeParams.DockerConfig = v;
                 }

                 v = src.DockerHostConfig;
                 if(v) {
                      extend(id.containerCreateParams.HostConfig, v);
                      id.volumeParams.DockerHostConfig = v;
                 }
              });


              var ip = mcImageLabels[app.mcParamsFull];
              if(ip) {
                  if(ip['unix-socket-groupname']) {
                     // need to mount a unix socket via fuse, using the groupname syntax.
                     addMountForSocketDir(id, {groupMode: "sys-"+ip['unix-socket-groupname']});
                  }
                  if(ip['unix-socket-deps']) {
                     ip['unix-socket-deps'].forEach(name=>{
                         var hostDir = path.posix.join(mc_socket_dir, "sys-"+name);
                         addBindMount(id, hostDir, "/var/run/"+name);
                     });

                  }
                  if((ip['log-max-size'])||(ip['log-max-file'])) {
                    var lc = simpleCloneObject(basicContainerConstraints.HostConfig.LogConfig)
                    Array('max-size','max-file').forEach(x=>{
                      var n = 'log-'+x;
                      if(ip[n])
                        lc.Config[x] = ip[n];
                    })                    
                    id.containerCreateParams.HostConfig.LogConfig = lc;
                  }

                  if((ip["user"])&&(ip["group"])&&(userId)) {
                     var uid = userId.uid(ip["user"]);
                     var gid = userId.gid(ip["group"]);
                     id.containerCreateParams.User = `${uid}:${gid}`;
                  }
              }

              if((req.systemImage)&&(!id.containerCreateParams.User)) {
                 throw new Error("Runtime User not configured for this container.");                
              }
                
         })
         .then(()=>{
              // creating the container

              var containerParams = extend(
                {
                  AttachStdin: false,
                  AttachStdout: false,
                  AttachStderr: false,
                  Tty: false,
                  OpenStdin: false,
                  StdinOnce: false,
                  Labels: {},
                },
                buildDockerContainerConstraints(req, extend({}, id.containerCreateParams, id.volumeParams.containerCreateParams)), 
                {
                  Image: d.image,
                }
              );


              if(!containerParams.HostConfig.RestartPolicy) {
                containerParams.HostConfig.RestartPolicy = {Name: "unless-stopped"};
              }


              Object.keys(mcImageLabels).forEach(l=>{
                 if(!containerParams.Labels[l])
                    containerParams.Labels[l] = mcImageLabels[l];
              })

              // NOTE: the same function ncould be used to upgrade generic containers as well
              if(req.webhosting) {
                 // NOTE: docker accepts strings only:
                 containerParams.Labels[app.mcImageLabelPrefix+"webstore"] = ""+req.webhosting.wh_id;                
                 containerParams.Labels[app.mcImageLabelPrefix+"user"] = ""+req.webhosting.wh_user_id;                
              }

              if(typeof containerParams.Labels[app.mcParamsFull] == "object") {
                 containerParams.Labels[app.mcParamsFull] = JSON.stringify(containerParams.Labels[app.mcParamsFull]);                
              }

              if(containerParams.Labels[app.mcImageLabelPrefix+"mc-webapp"]) {
                 const mysqlPath = app.config.get("mount_mysql_socket_for_webapps_path");
                 if(mysqlPath) {
                    containerParams.HostConfig.Binds.push(`${mysqlPath}:${mysqlPath}`);                  
                 }
              }

              if(d.debug) {
                  var sleepSec = d.debug + 120;
                  // note: parameters must be passed as strings, otherwise docker daemon complains
                  id.containerCreateParams.Entrypoint = [SLEEP_CMD, ""+sleepSec];
                  id.containerCreateParams.Cmd = [];
              }

              console.inspect("creating new container", containerParams);
              return app.docker.createContainer(containerParams);
         })
         .then(acontainer=>{
            // note: this is fucking discrepancy in the docker api
            return getContainerInfo(acontainer);
         })
         .then(acontainer=>{
            container = acontainer;
            id.inspectedContainer = container;

            if(!imageParamsHandler.postCreate) return;

            return imageParamsHandler.postCreate(req, id, container);
         })
         .then(()=>{

            // saving some info about the container
            if(!req.webhosting)  return;

            if(!app.webstoreConfigs[req.webhosting.wh_id])
              app.webstoreConfigs[req.webhosting.wh_id] = {};
            app.webstoreConfigs[req.webhosting.wh_id][container.info.Id] = id.volumeParams;
            return app.saveWebstoreConfigs();
         })
         .then(()=>{

            app.volumeNameToContainer[id.volumeParams.volumeInfo.name] = {
              containerId: container.info.Id,
            };
            if(req.webhosting)
              app.volumeNameToContainer[id.volumeParams.volumeInfo.name].wh_id = req.webhosting.wh_id;

            if(d.start) {
               return startContainer(container, imageParamsHandler);              
            }

         })
         .then(()=>{
              if(!d.debug) return;

              return shutdownContainer(container, {autoRemove: true, countdownSeconds: d.debug});
         })
         .then(()=>{
            extend(id.finalResult, {container: container.info.Id});
            return id;
         })
         .catch(ex =>{
            if(ex.safeToProceed) {
               return {finalResult: {alreadyAdded: true, container: ex.safeToProceed.Id}};
            }

            console.error("Error while creating container", ex);

            throw ex;
         })
    }


    router.post("/containers/new", function(req,res,next){
        return router.newContainerLogic(req)
         .then((result)=>{
            app.InsertEvent(req, {e_event_type: "new-container"})
            return req.sendResponse(result.finalResult);
         })

    })

    // this is for short running one-time executions
    Array("fast", "stream").forEach(function(cat){
        router.post("/containers/run/"+cat, function(req,res,next){

           return vali.async(req.body.json, {
                image: {presence: true, isString: true, canImageBeStarted: {req: req, execType: "run"}}, 
                timeout_ms: {isInteger: true, webhostingCommandTimeout: {req: req}, default: default_timeout},
                command: {presence: true, isArray: {lazyString: true}}
             })
             .then(d=>{

                // forcing autoremoval via this interface
                d.autoRemove = true;

                return app["runInNewContainer_"+cat](d, buildDockerContainerConstraints(req))
             })
             .then((re)=>{
                 app.InsertEvent(req, {e_event_type: "runcmd-container"})
                 return req.sendResponse(re)
             })


        })
    })


    Array("fast", "stream").forEach(function(cat){
        router.post("/container/:container_id/exec/"+cat, webstoreContainerAuthorization, execLogic);
        router.post("/token/:token/exec/"+cat, webstoreContainerAuthorizationTokenBased, execLogic);

        function execLogic(req,res,next){
            var d;
            return vali.async(req.body.json, {
                command: {presence: true, isArray: {lazyString: true}},
                timeout_ms: {isInteger: true, webhostingCommandTimeout: {req: req}, default: default_timeout},
            })
              .then(ad=>{
                  d = ad;
                  return app.docker.getMcImageByID(req.container.info.ImageID);
              })
              .then(image=>{

                  router.imageStripper([image], true);

                  if(image.Labels[app.mcParamsFull]) {
                    if((!image.Labels[app.mcParamsFull]["exec"])&&(!image.Labels[app.mcParamsFull]["debug"])) {
                       throw new MError("VALIDATION_ERROR", {"exec": "exec is not allowed"})
                    }
                  }

                  return app["executeCommandInContainer_"+cat](req.container.fn, d);
              })
              .then(x=>{
                 app.InsertEvent(req, {e_event_type: "execcmd-container"})
                 return req.sendResponse(x);
              })    
        }
    })


    // to show stopped containers, use {all: true}, thats the default here
    /*
[ { Id: '73b2040f3d2da845c6f1f285820f9753d16378cc00aac02a729069ef162782a9',
    Names: [ '/adoring_leavitt' ],
    Image: 'alpine',
    ImageID: 'sha256:3fd9065eaf02feaf94d68376da52541925650b81698c53c6824d92ff63f98353',
    Command: '/bin/sh',
    Created: 1529237902,
    Ports: [],
    Labels: {},
    State: 'running',
    Status: 'Up 31 seconds',
    HostConfig: { NetworkMode: 'default' },
    NetworkSettings: { Networks: [Object] },
    Mounts: [] } ]
    */
    router.post("/containers/list", function(req,res,next){

        return req.sendPromResultAsIs(
          listContainers(req)
            .then(cs=>{
               return router.containerStripper(cs);
            })
        );
    })

    function isSshAccessAllowed(req){
        var t = ((req.webhosting||{}).template||{}).t_allowed_dynamic_languages = [];
        return 0 >= t.indexOf("ssh");
    }

    function authorizeSshAccess(req){
        if(!isSshAccessAllowed(req))
            throw new MError("PERMISSION_DENIED");
    }
    router.post("/ssh/allowed", function(req,res,next){
        return req.sendResponse({allowed: isSshAccessAllowed(req)});
    });

    router.post("/ssh/target", function(req,res,next){
       return Promise.resolve()
         .then(()=>{
            return authorizeSshAccess(req);
         })
         .then(()=>{
             if(!req.body.json.targetContainer) return;

             if((!app.webstoreConfigs[req.webhosting.wh_id])
               ||
               (!app.webstoreConfigs[req.webhosting.wh_id][req.body.json.targetContainer])) {
               throw new MError("PERMISSION_DENIED");              
             }
         })
         .then(()=>{
             return app.globalSsh.SaveTargetContainer(req.webhosting.wh_id, req.body.json.targetContainer)
         })
         .then(()=>{
            return req.sendOk();
         })
    })

    router.post("/ssh/authz", function(req,res,next){
       return Promise.resolve()
         .then(()=>{
            return authorizeSshAccess(req);
         })
         .then(()=>{
            return app.globalSsh.SaveAuthorizedKeys(req.webhosting.wh_id, req.body.json.authz);
         })
         .then(()=>{
            return req.sendOk();

         })
    })
    router.get("/ssh/", function(req,res,next){
        return req.sendPromResultAsIs(app.globalSsh.GetConfig(req.webhosting.wh_id));
    });

    router.post("/ssh/cleanup", function(req,res,next){
       return req.sendOk(app.globalSsh.CleanupWebhosting(req.webhosting.wh_id));
    })

    router.delete("/containers", function(req,res,next){

       var emitter = app.commander.EventEmitter();
       var errors = 0;
       return emitter.spawn()
         .then(h=>{
            req.sendTask(Promise.resolve(h))

            return listContainers(req)
         })
          .then(containers=>{
              return dotq.linearMap({array: containers, catch: ex =>{
                console.error("error while removing container", ex);
              }, action: container=>{
                  if(emitter)
                     emitter.send_stdout_ln("Removing container: "+container.Id);
                  var inspectedContainer;
                  return getContainerInfo(container)
                    .then(aInspectedContainer=>{
                       inspectedContainer = aInspectedContainer;
                       return stopContainer(inspectedContainer, true)
                       .catch(ex=>{
                          console.error("Unable to stop container, trying to proceed", ex);                          
                       })
                    })
                    .then(()=>{
                       return getContainerInfo(container);
                    })
                    .then(aInspectedContainer=>{                      
                       inspectedContainer = aInspectedContainer;
                       return removeContainer(inspectedContainer, true);
                    })
              }})

          })
          .then(()=>{
              if(emitter)
                emitter.send_stdout_ln("Cleaning up SSH settings");
              return app.globalSsh.CleanupWebhosting(req.webhosting.wh_id);
          })
          .then(()=>{
              if(errors > 0) 
                 throw new Error("There were some errors: "+errors);
               
              emitter.close();
          })
          .catch(ex=>{
             console.error("Some generic error while removing containers", ex);
             if(emitter)
               emitter.close(1);
          })
    })


/*
// a typical response by the lister:
[ { Containers: -1,
    Created: 1529237078,
    Id: 'sha256:e189adde95ba093f3a41e918c4734ae6a0cf04f9be962a8ee4d85dc6bfa61543',
    Labels: { foo1: 'bar2', foo2: 'bar2' },
    ParentId: 'sha256:3fd9065eaf02feaf94d68376da52541925650b81698c53c6824d92ff63f98353',
    RepoDigests: null,
    RepoTags: [ 'test-image:latest' ],
    SharedSize: -1,
    Size: 4147781,
    VirtualSize: 4147781 } ]
*/
    router.post("/images/available", function(req,res,next){
         return req.sendPromResultAsIs(
            canCreateLogic(req)
              .then(cre=>{
                 cre.images = router.imageStripper(Object.values(cre.images)); 
                 delete cre.tags;
                 return cre;
              })
         );
    })


    processWebstoreConfigs();


    router.processImages = function(images, allowedTemplates){
              var re = {images: {}, tags: {}};
              images.forEach(image=>{
                 var found = allowedTemplates ? false : true;
                 if(!found) {
                     allowedTemplates.some(tag=>{
                        // console.log("XXX", image.Labels, tag)
                        if(image.Labels[app.mcImageLabelPrefix+app.mcAppLabel] == tag) {
                            found = true;
                            return true;
                        }

                     });
                 }
                 if(found) {
                    image.RepoTags.forEach(tag=>{
                       re.images[tag] = image;
                       var n = router.getImageAndTag(tag);
                       if(n) {
                          if(!re.tags[n.image])
                            re.tags[n.image] = {};
                          re.tags[n.image][n.tag] = image;
                       }

                    })
                 }
              });

              Object.keys(re.tags).forEach(imageName=>{
                if(re.tags[imageName].latest) return;
                var tags = Object.keys(re.tags[imageName]).sort();
                re.tags[imageName].latest = re.tags[imageName][tags[tags.length - 1]];
              })

              return re;
    }

    router.getImageAndTagAlways = function(image){
       var m = router.getImageAndTag(image);
       if(!m) m = {image: image, tag: "latest"};

       m.containerName = m.image;
       if(m.tag != "latest")
          m.containerName += ":"+m.tag;

       return m;
    }
    router.getImageAndTag = function(image){
       var imageName;
       var tag;
       var r = /(.+):(.+)/;
       var m = r.exec(image);
       if(m)
         return {image:m[1], tag: m[2]};
    }

    router.stopAndRemoveContainer = function(container){
        return stopContainer(container, true)
         .catch(ex=>{
            console.error("Unable to stop container, trying to remove", container, ex)
         })
         .then(()=>{
              return removeContainer(container);
          })
    }

    router.wrapContainerFromList = function(container){
       return { info : container, fn: app.docker.getContainer(container.Id) }
    }

    router.getContainerInfo = function(containerFnOrContainerId){
       return getContainerInfo(containerFnOrContainerId);
    }

    router.getContainerHumanName = function(name){
       if(!name) return;
       return name.substr(1);
    }

    router.containerStripper = function(cArr, dontDrop){

      cArr.forEach(c=>{
        var labels = c.Labels;
        Object.keys(c).forEach(n=>{
           if(!dontDrop) {
               if(["Id", "Names", "Image", "Command", "Labels", "State", "Status"].indexOf(n) < 0) {
                  delete c[n];
               }            
           }

        });
        
        // console.log("stripping", c)
        if((c.Name)||(c.Names)) {
          c.PrimaryName = router.getContainerHumanName(c.Name || c.Names[0]);
        }

        // append info about shutdown timers
        if(timers[c.Id]) {
            c.shutdownAt = timers[c.Id].shutdownAt;
        }

        if(labels) {
           // console.log("shit?x2", labels,app.mcImageLabelPrefix+"mc-app" )
           if(labels[app.mcImageLabelPrefix+"mc-app"] == "webshell") {
              var volume = router.getWebstoreConfig({info:c});
              if(volume)
                 c.webshellSecret = volume.volumeInfo.name;
           }

           c.c_webhosting = labels[app.mcImageLabelPrefix+"webstore"];
           c.c_user_id = labels[app.mcImageLabelPrefix+"user"];
        }

      });
      return cArr;
    }

    router.imageStripper=function(iArr, dontDrop){
      iArr.forEach(i=>{
        if(!dontDrop){
          Object.keys(i).forEach(n=>{
             if(["Id","Labels","RepoTags","VirtualSize"].indexOf(n) < 0)
                delete i[n];
          })          
        }

        if(i.Labels) {
            if((i.Labels[app.mcParamsFull])&&(typeof i.Labels[app.mcParamsFull] == "string")) {
               try{
                  i.Labels[app.mcParamsFull] = JSON.parse(i.Labels[app.mcParamsFull]);            
               }catch(ex){
                  console.error("Invalid mcParams label", i, i.Labels);
                  i.Labels[app.mcParamsFull] = {};
               }
            }
          
        }


        if(i.RepoTags)
          i.PrimaryTag = i.RepoTags[0];
      });
      return iArr;
    }

    router.getHostLibInfo = function(inspectedContainer, nameIsEnough) {
         var container = inspectedContainer.inspect;
         /*
         if(inspectedContainer.inspect.Id.match(/^php/))
            console.log("SHIIIIIIIIIIIIIIIIIIT2", inspectedContainer)
          */

         var rawName = container.Name;
         if((!rawName)&&(container.Names))
           rawName = container.Names[0];
         if(!rawName)
            return;
         var hostname = router.getContainerHumanName(rawName);
         if(!hostname)
            return;
         if(nameIsEnough)
            return {hostname: hostname};

         if(!container.NetworkSettings) return;
         if(!container.NetworkSettings.IPAddress) return;
         var ip = container.NetworkSettings.IPAddress;

         return {ip: ip, hostname: hostname};
    }
    router.isHostLibInfoNeeded = function(inspectedContainer) {
         if(!docker_hosts_file_webserver_app_only) return true;
         if(
             (inspectedContainer.inspect.Config) &&
             (inspectedContainer.inspect.Config.Labels) &&
             (inspectedContainer.inspect.Config.Labels[app.mcImageLabelPrefix+app.mcWebAppLabel] == "webserver-http")
           ) 
         {
            return true;          
         }

         return false;
    }

    router.getHostLibInfoWhenNeeded = function(inspectedContainer, nameIsEnough) {
         if(!router.isHostLibInfoNeeded(inspectedContainer)) return;
         return router.getHostLibInfo(inspectedContainer, nameIsEnough);
    }


    return router

    function getContainerInfo(containerFnOrContainerId){
       var re = {};


       var isObject = false;
       if(typeof containerFnOrContainerId == "object") {
          isObject = true;
          if(containerFnOrContainerId.fullyInspected)
             return Promise.resolve(containerFnOrContainerId);
       }

       return Promise.resolve()
         .then(()=>{
            if(isObject) {

               if(containerFnOrContainerId.fn)
                  re.fn = containerFnOrContainerId.fn;
               else if(containerFnOrContainerId.inspect)
                  re.fn = containerFnOrContainerId;
               else
                  re.fn = app.docker.getContainer(containerFnOrContainerId.Id);

            }
            else
               re.fn = app.docker.getContainer(containerFnOrContainerId);
            return re.fn.inspect();
         }) 
         .then(inspect=>{
            re.fullyInspected = true;
            re.inspect = inspect;
            re.info = inspect.Config;
            re.info.Id = inspect.Id;
            re.info.State = inspect.State.Status;
            re.info.Name = inspect.Name;
            re.info.Names = inspect.Names;
            re.info.PrimaryName = router.getContainerHumanName(re.info.Name || (re.info.Names||[])[0]);
            return re;
         })
    }

    function processWebstoreConfigs(){
       return app.loadWebstoreConfigs()
         .then(()=>{
            console.log("killing any potential left-over socat instances")
            var chain = {"executable": "[pkill_path]", args: ["-f", "socat UNIX-LISTEN:/var/lib/monster/docker/"]};
            var params = [{
              pkill_path: app.config.get("pkill_path"), 
            }];
            return app.commander.spawn({executeImmediately:true, chain: chain}, params).then(h=>{
               return h.executionPromise;
            }).catch(ex=>{
               console.error("Unable to kill socat instances", ex)
            })
         })
         .then(()=>{
            // invoke preStart hooks where needed (to start socats and install shutdown timers)
            return dotq.linearMap({array: Object.keys(app.webstoreConfigs), action: (wh_id)=>{
               var containers = app.webstoreConfigs[wh_id];
               return dotq.linearMap({array: Object.keys(containers), catch: true, action: containerId =>{
                  return getContainerInfo(containerId)
                    .then(container=>{

                        var volume = containers[containerId];

                        app.volumeNameToContainer[volume.volumeInfo.name] = {
                           wh_id: wh_id,
                           containerId: containerId,
                        };

                        var imageParamsHandler = imageParamLookup(container.info.Image);
                        if(imageParamsHandler.preStart) {
                          console.log("Prestarting container", containerId, container.info.Image);
                          return imageParamsHandler.preStart(container);
                        }
                    })
               }});
            }});
         })
         .catch(console.error)
    }


    function canCreateLogic(req) {
        var cs;

        var p = req.systemImage ? Promise.resolve([]) : listContainers(req);
        return p
          .then(acs=>{
              cs = acs;
              return app.docker.listMcImagesByLabels();
          })
          .then(images=>{
              // filter out abstract images
              router.imageStripper(images, true);

              images = images.filter(i => !i.PrimaryTag.match(/-base:/));


              var tagFilters = !req.systemImage ? req.webhosting.template.t_allowed_dynamic_languages||[] : undefined;
              var re = router.processImages(images, tagFilters);

              if(req.preVerified)
                re.canCreate = true;
              else
                re.canCreate = cs.length < req.webhosting.template.t_max_container_apps;


              return re;
          });
    }


    function listContainers(req, token){
       var filters = ["webstore="+req.webhosting.wh_id];
       if(req.body.json.webapp)
          filters.push(app.mcWebAppLabel);
       if(token)
         filters.push("containerName="+token);
       return app.docker.listMcContainersByLabels(filters, {all: true});
    }

    function webstoreContainerAuthorizationRawCommon(req, containers, container_id){
        req.containers = containers;

        var found = false;

        if(container_id) {
          containers.some(c=>{
             if(c.Id == container_id) {
                found = c;
                return true;
             }
          });
        } else {
          found = containers[0];
        }

        if(!found) throw new MError("PERMISSION_DENIED");

        found = router.containerStripper([found], true)[0];
        req.container = router.wrapContainerFromList(found);


        // not returning anything
    }
    function webstoreContainerAuthorizationRawTokenBased(req, token){
       return listContainers(req, token)
         .then(containers=>{

            return webstoreContainerAuthorizationRawCommon(req, containers);
         })
    }
    function webstoreContainerAuthorizationRaw(req, container_id){
       return listContainers(req)
         .then(containers=>{
            return webstoreContainerAuthorizationRawCommon(req, containers, container_id);
         })
    }

    function webstoreContainerAuthorizationTokenBased(req,res,next){
       return webstoreContainerAuthorizationRawTokenBased(req, req.params.token).then(next);
    }
    function webstoreContainerAuthorization(req,res,next){
       return webstoreContainerAuthorizationRaw(req, req.params.container_id).then(next);
    }

    function buildDockerContainerConstraints(req, extraConstraints){
       var basicContainerConstraintsCopy = simpleCloneObject(basicContainerConstraints);
       var oHostConfig = extraConstraints.HostConfig;
       delete extraConstraints.HostConfig;

       
       var userDetails = (!req.systemImage || req.webhosting) ? {
            User: req.webhosting.wh_id+":"+webapp_gid,
       } : {};

       var re = extend(
          {},
          basicContainerConstraintsCopy, 
          userDetails, 
          extraConstraints
       );

       if(oHostConfig) {
          extend(re.HostConfig, oHostConfig)
       }

       if(!req.systemImage) {
         re.HostConfig.Binds = re.HostConfig.Binds.concat(defaultBindMounts);
         re.HostConfig.Binds.push(req.webhosting.extras.web_path+":/web");
       }

       return re;
    }


    function imageParamLookup(image){
       if(imageParamConstraints[image])
          return imageParamConstraints[image];


       var imageName;
       var tag;
       var m = router.getImageAndTag(image);
       if(m){
          imageName = m.image;
          tag = m.tag;

          if(imageParamConstraints[imageName])
           return imageParamConstraints[imageName];

       } else
         imageName = image;

       // still attempting to match, based on suffixes
       var re = {};

       if(!imageName) return re;

       var found = false;
       Object.keys(imageParamConstraints).some(q=>{
          if(imageName.endsWith(q)) {
            re = imageParamConstraints[q];
            found = true;
            return true;
          }

       })

       if(found) return re;

       // one more attempt, based on partial matches
       Object.keys(imageParamConstraints).some(q=>{
          if(imageName.indexOf(q) > -1) {
            re = imageParamConstraints[q];
            return true;
          }

       })

       return re;
    }

    function startContainer(container, imageHandler){
       if(!imageHandler) 
         imageHandler = imageParamLookup(container.info.Image);

       return Promise.resolve()
         .then(()=>{
             if(!imageHandler.preStart) return;

             return imageHandler.preStart(container);
         })
         .then(()=>{
             return container.fn.start();
         })
         .then(()=>{
             // global poststart handler: updating the system hostsfile
             return addHostLibInfoWhenNeededWithRetries(container);
         })
    }

    function addHostLibInfoWhenNeededWithRetries(container) {
       var attempts = 0;

       return doit();

       function doit(){
          attempts++;
          delete container.fullyInspected;
          return getContainerInfo(container)
         .then(inspectedContainer=>{
             /*
             if(inspectedContainer.info.Id.match(/^php/))
                console.log("SHIIIIIIIIIIIIIIIIIIT", inspectedContainer)
              */
             if(!router.isHostLibInfoNeeded(inspectedContainer))
               return false;

             var r = router.getHostLibInfoWhenNeeded(inspectedContainer);
             if(!r) {
                if(attempts >= app.config.get("docker_max_ip_lookup_attempts")) {
                    console.error("ip address is not availabie in 5 attempts, giving up", attempts, container)
                    return;
                }
                console.log("ip address is not yet availabile, retrying", attempts, container.info.Id);
                return setTimeout(doit, 2000);
             }

             return app.hostsLib.appendEntry(r.hostname, r.ip);
         })
         .catch(ex=>{
            console.error("There was some unkonwn error while dealing with docker container hosts entry query", container, ex);
         })

       }      
    }

    function containerStopProcessing(inspectedContainer, promise, force){
         console.log("containerStopProcessing", inspectedContainer.info.Id);
         var imageHandler = imageParamLookup(inspectedContainer.info.Image);

         // need to shut down timers if any
         if(timers[inspectedContainer.info.Id]) {
            clearTimeout(timers[inspectedContainer.info.Id].timer);
            delete timers[inspectedContainer.info.Id];
         }

         return promise
         .catch(ex=>{
            if(ex.reason == 'container already stopped') {
               console.error("We were unable to stop container before upgrading, but proceeding because it was already stopped", inspectedContainer, ex);                
               return;
            }
            if((force)&&(imageHandler.postStop)) {
               console.error("Unable to stop container, proceeding with postStop handlers", ex);
               return;
            } 
            throw ex;
         })
         .then(()=>{
             if(!imageHandler.postStop) return;

             return imageHandler.postStop(inspectedContainer);
         })

    }

    function killContainer(container, options){
       console.log("killing container", container.info.Id, options);
       return getContainerInfo(container)
         .then(inspectedContainer=>{
            return containerStopProcessing(inspectedContainer, inspectedContainer.fn.kill(options));          
         })
    }
    function restartContainer(container){
        return container.fn.restart({t: app.config.get("stop_container_wait_before_kill_s")})
    }

    function stopContainer(container, force){
       console.log("stopping container", container.info.Id);
       return getContainerInfo(container)
         .then(inspectedContainer=>{
            return containerStopProcessing(inspectedContainer, inspectedContainer.fn.stop({t: app.config.get("stop_container_wait_before_kill_s")}), force);
         })
    }

    function slowlyStopOldContainerWhenNewIsStillUp(req, d, newContainer, signal){

         var oldContainer;

         return Promise.resolve()
           .then(()=>{
               // is this an upgade?
               if(!d.upgradeInfo) return;

               // giving the new container 30 seconds chance to initialize
               return dotq.sleep(30000)
                 .then(()=>{
                    return getContainerInfo(newContainer.info.Id)
                 })                 
                 .then(newContainer2=>{
                    if((!newContainer2.inspect.State.Running)||(newContainer2.inspect.State.Status!='running')) {
                       throw new Error("The new container is not running, not stopping the old!")                      
                    }

                    // ok, the old one is ready to depart. We need its pid first
                    return getContainerInfo(d.upgradeInfo.oldContainer)
                 })
                 .then((aOldContainer)=>{
                    oldContainer = aOldContainer;

                    // at first we need to stop the restart policy otherwise docker might bring it back from the dead
                    console.log("slowlyStopOldContainerWhenNewIsStillUp: Updating restart policy to not restart");
                    return oldContainer.fn.update({ RestartPolicy : restartNever })
                })
                .then(()=>{

                    console.log("slowlyStopOldContainerWhenNewIsStillUp: Restart policy has been updated");
                    var pid = oldContainer.inspect.State.Pid;

                    console.log("slowlyStopOldContainerWhenNewIsStillUp: Sending signal to ", oldContainer.info.Id, "the signal is", signal);
                    process.kill(pid, signal);
                 })

           })
    }


    function removeContainer(container, force){
          console.log("Removing container", container.info.Id);
          if(!force) {
              if(!container.info.State.match(/^(restarting|stopped|exited|created)$/)) {
                console.error("Cannot remove container due to its current state", container.info)
                throw new MError("NOT_STOPPED");
              }
          }


          var volume;
          var inspectedContainer;
          return getContainerInfo(container)
            .then(a=>{
                inspectedContainer = a;
  
                // decide if this is a web application and skip if it is not
                var labels = inspectedContainer.inspect.Config.Labels;
                // console.log("XXX", inspectedContainer.inspect);
                // console.inspect("XXXX", newResult);
                if((!labels)||(!labels[app.mcImageLabelPrefix+app.mcWebAppLabel]))
                   return;

                return app.GetDocroot().postAsync("/s/[this]/docrootapi/docroots/webapps/detach", {oldContainer: container.info.PrimaryName})
            })
            .then(()=>{
                return container.fn.remove()
            })
            .then(()=>{
                 // global poststop handler: updating the system hostsfile
                 var r = router.getHostLibInfoWhenNeeded(inspectedContainer, true);
                 // console.log("SSSSSSSSSSSSSSSS", r, inspectedContainer)
                 if(!r) return;
                 return app.hostsLib.removeEntry(r.hostname);
            })
            .then(()=>{


                // need to remove the volume directory, if exists
                volume = router.getWebstoreConfig(container);

                if(!volume) return;
                if(!volume.volumeInfo) return;

                const fp = volume.volumeInfo.path;
                if(!fp) return; // some containers do not have a volume created
                const rootPath = app.config.getVolumePath();

                delete app.volumeNameToContainer[volume.volumeInfo.name];

                if((!fp.startsWith("vol.d\\"))&&(fp.indexOf(rootPath) !== 0)) {
                   console.error("Invalid volume path", volume, rootPath);
                   throw new Error("Invalid local path");
                }
                return del(fp, {force: true})
            })
            .then(()=>{

                if(!volume) return;

                // need to remove any volume parameters, if any
                var wh_id = volume.wh_id;
                if(!wh_id) return;
                if(!app.webstoreConfigs[wh_id]) return;
                delete app.webstoreConfigs[wh_id][container.info.Id];
                if(Object.keys(app.webstoreConfigs[wh_id]).length <= 0)
                   delete app.webstoreConfigs[wh_id];

                return app.saveWebstoreConfigs();
            })
    }

    function getCompatibleImages(container){
        return app.docker.listMcImagesByLabels()
          .then(images=>{
             var re = {};

             var sourceImage = router.getImageAndTag(container.info.Image);
             // console.inspect("XXX", container.info, "Y", sourceImage);

             images.forEach(image=>{
               image.RepoTags.forEach(name=>{
                  var candidate = router.getImageAndTag(name);
                  if(!candidate) return;
                  if(candidate.image != sourceImage.image) return;

                  re[name] = image;
               })
             });

             return re;

          })
    }


    function getUserIdLib(){
       var isWin = process.platform === "win32";
       if(isWin) return;
       return require("/opt/MonsterDocker/lib/userid-"+app.config.get("debian_version"));
    }

}
