module.exports = function(app, webhostingRouter) {

    const MError = require("MonsterError")
    var ValidatorsLib = require("MonsterValidators")
    var vali = ValidatorsLib.ValidateJs()

    var router = app.ExpressPromiseRouter()

    const path = require("path");
    const dotq = require("MonsterDotq");
    const fs = dotq.fs();
    const del = require("MonsterDel");


    var docker_image_cleanup_delay_secs = app.config.get("docker_image_cleanup_delay_days")*86400;


    var imageRemovalDelayLastSeenHelper = {};


/*
// it requires an optsf like:
  var optsf = {
    path: '/containers/json?',
    method: 'GET',
    options: args.opts,
    statusCodes: {
      200: true,
      400: 'bad parameter',
      500: 'server error'
    }
  };
*/
    router.post("/raw", function(req,res,next){

        if(typeof req.body.json.optsf != "undefined") throw new MError("VALIDATION_ERROR");

        app.docker.modem.dial(req.body.json.optsf, function(err, data) {
          if(err) return next(new MError("DOCKER_ERROR", {details: err.message}, err));

          app.InsertEvent(req, {e_event_type: "raw-docker"})
          req.sendResponse(data);
        });

    })

    router.get("/webstores/config", function(req,res,next){
        return req.sendResponse({config: app.webstoreConfigs});
    })

    router.get("/webstore/:webhosting_id/config", function(req,res,next){
        var data = app.webstoreConfigs[req.params.webhosting_id];
        return req.sendResponse({config: data});
    })

    router.post("/webstore/:webhosting_id/config", function(req,res,next){
        app.webstoreConfigs[req.params.webhosting_id] = req.body.json.config;
        app.InsertEvent(req, {e_event_type: "webstore-docker-config"});
        return req.sendOk(app.saveWebstoreConfigs());
    })

    router.post("/images/pull", function(req,res,next){

        return vali.async(req.body.json, {tag: {presence: true, isString: true}})
          .then(d=>{
            return app.docker.pull(d.tag, {authconfig: app.config.get("docker_authconfig")})
              .catch(ex=>{
                  throw new MError("DOCKER_ERROR", {docker: ex.message}, ex)
              })
          })
          .then(stream=>{
             var emitter = app.commander.EventEmitter()
             return emitter.spawn()
               .then(h=>{
                  req.sendTask(Promise.resolve(h))

                  app.InsertEvent(req, {e_event_type: "pull-image"});

                  emitter.relayStream(stream);
               })
          });
    })

    router.get("/container/:container_id/config", function(req,res,next){
        return webhostingRouter.getContainerInfo(req.params.container_id)
         .then(c=>{
              var data = webhostingRouter.getWebstoreConfig(c);
              delete data.wh_id;
              return req.sendResponse({config:data});
         })
    })

    router.post("/container/:container_id/config", function(req,res,next){
        return req.sendOk(webhostingRouter.getContainerInfo(req.params.container_id)
          .then(c=>{
              var data = webhostingRouter.getWebstoreConfig(c);
              var wh_id = data.wh_id;
              app.webstoreConfigs[wh_id][req.params.container_id] = req.body.json.config;

              app.InsertEvent(req, {e_event_type: "container-docker-config"});
              return app.saveWebstoreConfigs();
          }));

    })

    router.post("/container/:container_id/exec", function(req,res,next){
        var container = app.docker.getContainer(req.params.container_id);
        return vali.async(req.body.json, {command: {presence: true, isArray: {lazyString: true}}})
          .then(d=>{
              return app.executeCommandInContainer_fast(container, d);
          })
          .then(x=>{
             app.InsertEvent(req, {e_event_type: "execcmd-container"});
             return req.sendResponse(x);
          })    
    })

    router.post("/container/:container_id/logs", function(req,res,next){
        var container = app.docker.getContainer(req.params.container_id);
        return req.sendPromResultAsIs(
           container.logs({stdout: true, stderr: true, tail: 1000})
        );
    })        

    router.post("/container/:container_id/stats", function(req,res,next){
        var container = app.docker.getContainer(req.params.container_id);
        return req.sendPromResultAsIs(
           container.stats({stream: false})
        );
    })        

    router.post("/containers/run", function(req,res,next){

       return vali.async(req.body.json, {
            image: {presence: true, isString: true}, 
            command: {presence: true, isArray: {lazyString: true}}
         })
         .then(d=>{

            return app.runInNewContainer_fast(d)
         })
         .then((re)=>{
            app.InsertEvent(req, {e_event_type: "runcmd-container"});
            return req.sendResponse(re)
         })

    });

    router.get("/container/:container_id/inspect", function(req,res,next){

       return webhostingRouter.getContainerInfo(req.params.container_id)
         .then(c=>{
            return req.sendResponse(c.inspect);
         })

    });

    router.post("/system/:imagename/start", function(req,res,next){
        return req.sendPromResultAsIs(startNewSystemImage(req));
    })



// {filters: {label: ["foo=bar1", "foo3"]}}
// or simple label: ["foo"]
    router.post("/containers/list", function(req,res,next){
        return req.sendPromResultAsIs(
            app.docker.listContainers(req.body.json)
             .then(containers=>{
                webhostingRouter.containerStripper(containers, true)
                return containers;
             })
        );

    })

    router.post("/containers/list/mc", function(req,res,next){
        return req.sendPromResultAsIs(listMcContainers(req.body.json.labels, req.body.json.filters));
    })

    router.get("/containers/list/upgradable", function(req,res,next){
        return req.sendPromResultAsIs(getListOfUpgradeableContainers(req.body.json));
    })

    router.post("/containers/rebuild", function(req,res,next){
         var emitter = app.commander.EventEmitter();
         var d;
         var failed = 0;
         var success = 0;
         return vali.async(req.body.json, {
            containers: {isArray:{lazyString: true}}
         })
         .then(ad=>{
            d = ad;
            return emitter.spawn();
         })
         .then((h)=>{
            req.sendTask(Promise.resolve(h))

            emitter.send_stdout_ln("Starting the rebuild process");
            if(d.containers) return d.containers;

            return listMcContainers()
              .then(containers=>{
                 return containers.map(x => x.Id);
              });
        })
        .then(containerIds=>{

            return dotq.linearMap({array: containerIds, action: function(containerId){

               // has to query the webhosting at this point
               emitter.send_stdout_ln("Querying info about container "+containerId);

               var wh;
               var inspectedContainer;
               return webhostingRouter.getContainerInfo(containerId)
               .then(ainspectedContainer=>{
                  inspectedContainer = ainspectedContainer;
                  var wh_id = inspectedContainer.info.Labels[app.mcImageLabelPrefix+"webstore"];
                  var p = wh_id ? app.MonsterInfoWebhosting.GetInfo(wh_id) : Promise.resolve();

                 return p
               })
               .then(awh=>{
                   wh = awh;

                   var mreq = {
                      body: {json:{destination_image: inspectedContainer.info.Image}},
                      container: inspectedContainer,
                      preVerified: true,
                   };
                   if(wh)
                      mreq.webhosting = wh;

                   return webhostingRouter.upgradeContainerLogic(mreq)
               })
               .then(()=>{
                    console.log("Container rebuilt successfully", containerId);
                    success++;
                    emitter.send_stdout_ln("Container "+containerId+" rebuilt successfully.");
                 })
                 .catch(ex=>{
                    console.error("Error while rebuilding a container", containerId, ex);
                    failed++;
                    emitter.send_stdout_ln("Rebuilding container "+containerId+" failed.");
                 })
            }})
         })
         .then(()=>{
             console.log("Container rebuild process has finished. Succeeded: "+success+" Failed: "+failed);
             emitter.send_stdout_ln("Container rebuild process has finished. Succeeded: "+success+" Failed: "+failed);
             emitter.close(0);                        
         })
         .catch(ex=>{
            console.error("Unexpected error while doing this mass container rebuild stuff", ex)
            emitter.send_stdout_ln("Unexpected error arose while rebuilding the containers");
            emitter.close(1);
         })
         .then(()=>{
             app.InsertEvent(req, {e_event_type: "rebuild-containers"});
         })

    });

    router.post("/containers/upgrade", function(req,res,next){
         var emitter = app.commander.EventEmitter();
         var d;
         return vali.async(req.body.json, {
            containers: {isArray:{lazyString: true}}
         })
         .then(ad=>{
            d = ad;
            return emitter.spawn();
         })
         .then((h)=>{
            req.sendTask(Promise.resolve(h))

            return doUpgradeContainers(req.body.json, emitter);
         })
         .then(()=>{
            app.InsertEvent(req, {e_event_type: "upgrade-containers"});
            return getLatestImagesAndSave();
         })
    })


    router.get("/image/:image_id/inspect", function(req,res,next){
       var image = app.docker.getImage(req.params.image_id);
       return req.sendPromResultAsIs(image.inspect());

    });

    router.delete("/image/:image_id", function(req,res,next){
       var image = app.docker.getImage(req.params.image_id);
       return req.sendPromResultAsIs(
          image.remove(req.body.json)
          .then((re)=>{
            app.InsertEvent(req, {e_event_type: "delete-image"});
            return re;
          })
       );
    });


// same label filtering as above
    router.post("/images/list", function(req,res,next){

        return req.sendPromResultAsIs(
           app.docker.listImages(req.body.json)
             .then(images=>{
                webhostingRouter.imageStripper(images, true)
                return images;
             })
        );

    })

    router.post("/images/latest/rebuild", function(req,res,next){

        return req.sendPromResultAsIs(getLatestImagesAndSave());

    })

    router.post("/images/latest", function(req,res,next){

        return req.sendPromResultAsIs(getLatestImages());

    })

    router.post("/images/list/mc", function(req,res,next){
        return req.sendPromResultAsIs(app.docker.listMcImagesByLabels(req.body.json.labels));
    })

    router.get("/images/list/upgradable", function(req,res,next){

        return req.sendPromResultAsIs(getListOfUpgradeableImages());
    })

    router.post("/images/cleanup", function(req,res,next){

         var emitter;
         var d;
         return vali.async(req.body.json, {
            images: {isArray:{lazyString: true}}
         })
         .then(ad=>{
            d = ad;
            emitter = app.commander.EventEmitter()
            return emitter.spawn()
         })
         .then((h)=>{
            req.sendTask(Promise.resolve(h))
            app.InsertEvent(req, {e_event_type: "cleanup-images"});
            return doCleanupImages(d, emitter, req.body.json);
         })
    })


    router.post("/local/php-fpm/rotate-logs", function(req,res,next){
        return req.sendOk(doRotatePhpFpmLogs());
    })

    router.post("/local/cleanup/list", function(req,res,next){
        return req.sendPromResultAsIs(listStuffToCleanup());
    })

    router.post("/local/cleanup/do", function(req,res,next){
        return req.sendOk(doCleanupAllKindOfStuff(req.body.json));
    })

    router.post("/local/hostsfile/rebuild", function(req,res,next){
        return req.sendOk(rebuildHostsFile());
    })

    installHostfileRebuild();
    installContainerUpgradeCron();
    installImageCleanupCron();
    installLatestImageCron();

    installPhpFpmLogRotation();

    return router

    function startNewSystemImage(req){
      var d;

      var imageName = webhostingRouter.getImageAndTagAlways(req.params.imagename);
      return vali.async(req.body.json, {
         force: {isBooleanLazy: true},
         payload: {isSimpleObject: true},
      })
      .then(ad=>{
         d = ad;

         if(d.force) {
           console.log("Force flag was specified, discarding whether some containers already running with image", req.params.imagename)
           return [];
         }

         return app.docker.listMcContainersByLabels(["containerName="+imageName.containerName]);
      })
      .then(c=>{
          if(c.length > 0) {
             console.log("Containers already found!", c.length);
             return {container:c[0].Id};
          }

          var mreq = {
              body: { json: extend({forceNameLabel: imageName.image, start:true, image: req.params.imagename}, d.payload) },
              canCreateLogic: {
                  image: req.params.imagename,
                  canCreate: true,
              },
              preVerified: true,
              systemImage: true,
          };

          // need to start a new container
          return webhostingRouter.newContainerLogic(mreq)
            .then(r=>{
                return r.finalResult;
            })
      })

    }

    function installPhpFpmLogRotation(){
        if(app.php_fpm_log_rotation) return;
        app.php_fpm_log_rotation = true;

        var csp = app.config.get("php_fpm_log_rotation");
        if(!csp) return;

        const cron = require('Croner');
 
        cron.schedule(csp, function(){
            return doRotatePhpFpmLogs()
              .catch(ex=>{
                 console.error("Error during rotating PHP-FPM logs", ex);
              })
        });
    }

    function doRotatePhpFpmLogs(){
       const retentionDays = app.config.get("php_fpm_log_rotation_retention_days");
       var nowStr = require("MonsterMoment").nowRaw();
       return listMcContainers([app.mcWebAppLabel+"=php-fpm"], {all:false})
         .then(containers=>{
            return dotq.linearMap({array:containers, catch: true, action: containerInfo=>{
                console.log("Rotating PHP-FPM logs for", containerInfo.Id);
                var containerFn = app.docker.getContainer(containerInfo.Id);
                var cmd = `
cat /host/php-fpm.conf|grep php-fpm-logs|awk '{print $3}'|xargs -I XXX mv XXX XXX.${nowStr}
find /web/php-fpm-logs/ -name '*.log*' -mtime +${retentionDays} -delete
kill -USR1 1
`;
                return containerFn.exec([
                  "/bin/sh", 
                  "-c",
                  cmd
                ]);
            }})
         })
    }

    function doCleanupImages(d, emitter, forcer){

         if(!forcer) forcer = {};

         if(!d) d = {};

         var failed = 0;
         var success = 0;

         return Promise.resolve()
          .then(()=>{

            if(emitter)
               emitter.send_stdout_ln("Fetching the list of images that have compatible newer versions");

            return getListOfUpgradeableImages()
         })
         .then(images=>{
            return dotq.linearMap({array: images, action: function(i){

               console.log("Image cleanup, checking image", i);

               // when only the selected containers were requested to upgrade
               if((d.images)&&(d.images.indexOf(i.PrimaryTag) < 0)) {
                  return;
               }

               if(!forcer[i.PrimaryTag]) {
                  // this is an auto triggered request, lets put some delay in here.
                  var now = require("MonsterMoment").nowUnixtime();
                  if(!imageRemovalDelayLastSeenHelper[i.Id])
                     imageRemovalDelayLastSeenHelper[i.Id] = now;
                  // if it is new there might be some users still, who need to downgrade here
                  console.log("checking image age (when was it seen the first time)", now, imageRemovalDelayLastSeenHelper[i.Id], docker_image_cleanup_delay_secs)
                  if(now - imageRemovalDelayLastSeenHelper[i.Id] < docker_image_cleanup_delay_secs) {
                     console.log("not removing this image for the time being, it is too fresh")
                     return;                  
                  }                
               }

               // has to query the webhosting at this point
               if(emitter)
                 emitter.send_stdout_ln("Removing image "+i.PrimaryTag+"...");

               console.log("Removing image "+i.PrimaryTag+"...");

               var image = app.docker.getImage(i.Id);
               return image.remove({force:true}) // some images might be references by multiple repositories, so the force flag is needed
                 .then(function(r){
                    console.log("Image removed successfully", i);
                    success++;
                    if(emitter) {
                      emitter.send_stdout_ln("Image removed successfully");
                      emitter.send_stdout_ln(JSON.stringify(r));                      
                    }
                 })
                 .catch(ex=>{
                    console.error("Error while removing image", i, ex);
                    failed++;
                    if(emitter) {
                      emitter.send_stdout_ln("Removing image "+i.PrimaryTag+" failed.");
                    }
                 })
            }})
         })
         .then(()=>{
             console.log("Image cleanup operation has finished. Succeeded: "+success+" Failed: "+failed);
             if(emitter){
               emitter.send_stdout_ln("Image cleanup operation has finished. Succeeded: "+success+" Failed: "+failed);
               emitter.close(0);                        
             }
         })
         .catch(ex=>{
            failed++;
            console.error("Unexpected error while doing this mass image cleanup stuff", ex)
            if(emitter) {
               emitter.send_stdout_ln("Unexpected error arose while cleaning up images");
               emitter.close(1);
            }
         })

    }

    function doUpgradeContainers(d, emitter){

       var failed = 0;
       var success = 0;
       if(!d) d = {};

       return Promise.resolve()
         .then(()=>{
            if(emitter)
               emitter.send_stdout_ln("Fetching the list of containers that are upgradable");

            return getListOfUpgradeableContainers()
         })
         .then(containers=>{
            return dotq.linearMap({array: containers, action: function(c){

               // when only the selected containers were requested to upgrade
               if((d.containers)&&(d.containers.indexOf(c.Id) < 0)) {
                  return;
               }

               // has to query the webhosting at this point
               if(emitter) {
                   emitter.send_stdout_ln("Upgrading container "+c.Id+" from "+c.Image+" to "+c.upgradeableTo+"...");                
               }

               var wh_id = c.Labels[app.mcImageLabelPrefix+"webstore"];
               var p = wh_id ? app.MonsterInfoWebhosting.GetInfo(wh_id) : Promise.resolve();
               var wh;
               return p
                 .then(awh=>{
                     wh = awh;
                     return webhostingRouter.getContainerInfo(c);
                 })
                 .then(inspectedContainer=>{
                     var mreq = {
                        body: {json:{destination_image: c.upgradeableTo}},
                        container: inspectedContainer,
                        preVerified: true,
                     };
                     if(wh)
                        mreq.webhosting = wh;

                     return webhostingRouter.upgradeContainerLogic(mreq)
                 })
                 .then(()=>{
                    console.log("Container upgraded successfully", c);
                    success++;
                    if(emitter) {
                       emitter.send_stdout_ln("Container "+c.Id+" upgraded from "+c.Image+" to "+c.upgradeableTo+" successfully.");                      
                    }
                 })
                 .catch(ex=>{
                    console.error("Error while upgrading a container", c, ex);
                    failed++;
                    if(emitter)
                      emitter.send_stdout_ln("Upgrading container "+c.Id+" from "+c.Image+" to "+c.upgradeableTo+" failed.");
                 })
            }})
         })
         .then(()=>{
             console.log("Container upgrade process has finished. Succeeded: "+success+" Failed: "+failed);
             if(emitter){
               emitter.send_stdout_ln("Container upgrade process has finished. Succeeded: "+success+" Failed: "+failed);
               emitter.close(0);                        
             }
         })
         .catch(ex=>{
            console.error("Unexpected error while doing this mass container upgrade stuff", ex)
            if(emitter) {
               emitter.send_stdout_ln("Unexpected error arose while upgrading the containers");
               emitter.close(1);
            }
         })
    }

    function installHostfileRebuild(){
        if(app.docker_hosts_file_rebuild_at_startup) return;
        app.docker_hosts_file_rebuild_at_startup = true;

        if(!app.config.get("docker_hosts_file_rebuild_at_startup")) return;

        setTimeout(function(){
           return rebuildHostsFileSafe()
             .then(()=>{
                installHostFilesCron()
             })
        }, app.config.get("docker_hosts_file_rebuild_at_startup_delay_sec")*1000);
    }

    function installContainerUpgradeCron(){
        if(app.docker_container_upgrade_cron) return
        app.docker_container_upgrade_cron = true

        var csp = app.config.get("docker_container_upgrade_cron")
        if(!csp) return

        const cron = require('Croner');

        cron.schedule(csp, function(){
            return doUpgradeContainers()
        });
    }

    function installLatestImageCron(){
        if(app.latest_image_cron) return
        app.latest_image_cron = true

        var csp = app.config.get("latest_image_cron");
        if(!csp) return;

        const cron = require('Croner');

        cron.schedule(csp, function(){
            return getLatestImagesAndSave()
              .catch(ex=>{
                  console.error("Unable to create latest images config file", ex);
              })
        });
    }

    function installImageCleanupCron(){
        if(app.docker_image_cleanup_cron) return
        app.docker_image_cleanup_cron = true

        var csp = app.config.get("docker_image_cleanup_cron")
        if(!csp) return

        const cron = require('Croner');

        cron.schedule(csp, function(){
            return doCleanupImages()
        });
    }

    function installHostFilesCron(){
        if(app.docker_hosts_file_rebuild_cron) return
        app.docker_hosts_file_rebuild_cron = true

        var csp = app.config.get("docker_hosts_file_rebuild_cron")
        if(!csp) return

        const cron = require('Croner');

        cron.schedule(csp, function(){
            return rebuildHostsFileSafe()
        });

    }

    function rebuildHostsFileSafe(){
        return rebuildHostsFile()
        .catch(ex=>{
           console.error("Unable to rebuild system hosts file", ex)
        })      
    }

    function rebuildHostsFile(){
       return app.docker.listMcContainersByLabels()
         .then(containersList=>{
            var entries = {};
            return dotq.linearMap({array: containersList, action: function(c){
               return webhostingRouter.getContainerInfo(c)
                 .then(ci=>{
                     var hostLibInfo = webhostingRouter.getHostLibInfoWhenNeeded(ci);
                     if(hostLibInfo) {
                        entries[hostLibInfo.hostname] = hostLibInfo.ip;
                     }
                 })
            }})
            .then(()=>{
                return app.hostsLib.setHostEntries(entries);
            })
         })
    }

    function listStuffToCleanup() {
        var re = {abandonedConfigEntries: [], abandonedVolumes: [], abandonedContainers: [], oldImages: []};
        var volumePath = app.config.getVolumePath();
        var knownContainers = {};
        var realContainers = {};
        var volumeHelper = {};
        return dotq.linearMap({array: Object.keys(app.webstoreConfigs), catch: true, action: whId=>{
           var containersHash = app.webstoreConfigs[whId];
           return dotq.linearMap({array: Object.keys(containersHash), catch: true, action: containerId=>{
              var container = containersHash[containerId];

              knownContainers[containerId] = container;
              container.wh_id = whId;

              if((!container.volumeInfo)||(!container.volumeInfo.path)) return;

              volumeHelper[container.volumeInfo.name] = container;

              return fs.statAsync(container.volumeInfo.path)
                .then(s=>{
                   if(!s.isDirectory()) throw new Error("not a directory")
                })
                .catch(ex=>{
                    re.abandonedConfigEntries.push({whId: whId, containerId: containerId});

                    if(ex.code != "ENOENT") throw ex;
                    // file does not exists.
                })
           }})
        }})
        .then(()=>{
            return fs.readdirAsync(volumePath)
        })
        .then(files=>{
            return dotq.linearMap({array: files, catch: true, action: file=>{
                if(!file.match(/^[0-9]+-[0-9a-f]+$/)) return;
                var fp = path.join(volumePath, file);
                return fs.statAsync(fp)
                  .then(s=>{
                     if(!s.isDirectory()) return;
                     // so this is a directory, it exists, and has the proper naming convention. lets see if it exists in the volume hash
                     if(!volumeHelper[file])
                       re.abandonedVolumes.push({name: file});
                  })
            }})
        })
        .then(()=>{
            return listMcContainers();
        })
        .then(containers=>{
            return dotq.linearMap({array: containers, catch: true, action: container=>{
               var cId = container.Id;
               realContainers[cId] = container;
               if(!knownContainers[cId])
                  re.abandonedContainers.push({containerId: cId});
            }})
        })
        .then(()=>{
           Object.keys(knownContainers).forEach(cId=>{
              if(!realContainers[cId]) {
                re.abandonedConfigEntries.push({whId: knownContainers[cId].wh_id, containerId: cId});
              }
           })
        })
        .then(()=>{
           return getListOfUpgradeableImages()
             .then(images=>{
                re.oldImages = images.map((x)=>{return {PrimaryTag:x.PrimaryTag}});
             })
        })
        .then(()=>{
           return re;
        })
    }
    function doCleanupAllKindOfStuff(in_data) {
       var d;
       var cleanup;
       return vali.async(in_data, {
          configEntries: {presence: true, isBooleanLazy: true},
          container: {presence: true, isBooleanLazy: true},
          volume: {presence: true, isBooleanLazy: true},
          image: {presence: true, isBooleanLazy: true},
       })
       .then((ad)=>{
          d = ad;
          return listStuffToCleanup()
       }) 
       .then(acleanup=>{
          cleanup = acleanup;
          if(!d.configEntries) return;
          cleanup.abandonedConfigEntries.forEach(e=>{
              delete app.webstoreConfigs[e.whId][e.containerId];
          })

          return app.saveWebstoreConfigs();
       })
       .then(()=>{
          if(!d.container) return;
          return dotq.linearMap({array: cleanup.abandonedContainers, action: c=>{
              return webhostingRouter.getContainerInfo(c.containerId)
                .then(container=>{
                    return webhostingRouter.stopAndRemoveContainer(container);
                })              
          }})
       })
       .then(()=>{
          if(!d.volume) return;
          var volumePath = app.config.getVolumePath();
          return dotq.linearMap({array: cleanup.abandonedVolumes, action: v=>{
             var fp = path.join(volumePath, v.name);
             console.log("Removing volume directory completely", fp);
             return del(fp, {force: true})
          }})
       })
       .then(()=>{
          if(!d.image) return;
          return doCleanupImages();
       })

    }

    function listMcContainers(labels, extra){
       return app.docker.listMcContainersByLabels(labels, extend({all: true}, extra));
    }

    function getLatestImagesAndSave(){
       var images;
       return getLatestImages()
         .then(aimages=>{
            images = aimages;
            var latest_images_path = app.config.get("latest_images_path");
            if(!latest_images_path) return;
            console.log("Writing out version info about latest Docker images to", latest_images_path);
            return fs.writeFileAsync(latest_images_path, JSON.stringify(images))
         })
         .then(()=>{
           return images;
         })
    }

    function getLatestImages(){
       console.log("Retrieving list of latest Docker images");
       return app.docker.listMcImagesByLabels()
         .then(aimages=>{
            var images = webhostingRouter.imageStripper(aimages, true);
            var processedImages = webhostingRouter.processImages(images);
            var re = {};
            Object.keys(processedImages.tags).forEach(imageName=>{
               var processedImageStuff = processedImages.tags[imageName];
               if(!processedImageStuff.latest) return;

               var latestFullname = processedImageStuff.latest.PrimaryTag;
               var r = webhostingRouter.getImageAndTag(latestFullname);
               re[imageName] = r.tag;
            });

            return re;
         });
    }

    function getListOfUpgradeableImages(){
       var images;
       return app.docker.listMcImagesByLabels()
         .then(aimages=>{
            images = webhostingRouter.imageStripper(aimages, true);
            var processedImages = webhostingRouter.processImages(images);
            // console.inspect(processedImages);
            var upgradeable = [];

            Object.keys(processedImages.tags).forEach(imageName=>{
               var processedImageStuff = processedImages.tags[imageName];
               console.log("checking", imageName)
               var tags = Object.keys(processedImageStuff);
               if(!processedImageStuff.latest) {
                 console.error("Latest version of image", imageName, "was not determined");
                 return;
               }
               if(tags.length <= 2) return;

               var latestFullname = processedImageStuff.latest.PrimaryTag;
               tags.forEach(tagName=>{
                  var aimage = processedImages.tags[imageName][tagName];
                  aimage.upgradeableTo = latestFullname;
                  if(aimage.PrimaryTag != latestFullname) {
                     upgradeable.push(aimage);
                  }
               })
            })

            return upgradeable;
         })

    }

    function getListOfUpgradeableContainers(in_data){
       var containers;
       var images;
       return listMcContainers()
         .then(acontainers=>{
            containers = webhostingRouter.containerStripper(acontainers, true);
            return app.docker.listMcImagesByLabels()
         })
         .then(aimages=>{
            images = aimages;
            var processedImages = webhostingRouter.processImages(images);
            // console.inspect(processedImages)

            var upgradeable = [];
            containers.forEach(container=>{
               var c = webhostingRouter.getImageAndTag(container.Image);
               if(!c){
                  console.error("Unable to determine image name and tag for container", container)
                  return;
               }
               if(!processedImages.tags[c.image]) {
                  console.error("Image of the container not found?!", c)
                  return;
               }
               if(!processedImages.tags[c.image].latest) {
                  console.error("Couldnt determine the latest version of an image?!", c)
                  return;
               }

               var latestImageName = processedImages.tags[c.image].latest.RepoTags[0];
               var l = webhostingRouter.getImageAndTag(latestImageName);
               if(!l){
                  console.error("Unable to determine image name and tag for image", processedImages.tags[c.image].latest)
                  return;
               }

               if(c.tag != l.tag) {
                  container.upgradeableTo = latestImageName;
                  upgradeable.push(container);
               }
            })

            return upgradeable;
         })

    }

}
