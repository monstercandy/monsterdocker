var lib = module.exports = function(app, webstoreRouter) {

   var wie = {};

   const dotq = require("MonsterDotq");
   const moment = require("MonsterMoment");

   wie.BackupWh = function(wh, req) {
      var wh_id = wh.wh_id;
      var re = {containers:[]};
      return app.docker.listMcContainersByLabels(["webstore="+wh_id])
        .then(containers=>{
            webstoreRouter.containerStripper(containers, true);
            containers.forEach(c=>{

               var volume = app.webstoreConfigs[wh_id][c.Id];

               var wie = {
                 name: c.PrimaryName,
                 volumeParams: volume,
               };

               re.containers.push(wie);
            })

            return re;
        })

   }

   wie.GetAllWebhostingIds = function(){
      return app.docker.listMcContainersByLabels()
        .then(containers=>{
           var uniqueWhIds = [];
           containers.forEach(c=>{
              var whId = c.Labels[app.mcImageLabelPrefix+"webstore"];
              if(uniqueWhIds.indexOf(whId) < 0)
                uniqueWhIds.push(whId);
           })

           return uniqueWhIds;
        })
   }

   wie.RestoreWh = function(wh, in_data, req) {

      return Promise.resolve()
        .then(()=>{

           return dotq.linearMap({array: in_data.containers, action: c=>{

              var mreq = {
                 body: {
                   json: c.volumeParams.createdAs,
                 },
                 webhosting: wh,
              };

              c.cloneForWie = true;

              return webstoreRouter.newContainerLogic(mreq, c);

           }})

        })
        .then(()=>{
            app.InsertEvent(req, {e_event_type: "restore-wh-docker", e_other: wh.wh_id})

        })


   }



   return wie;

}
