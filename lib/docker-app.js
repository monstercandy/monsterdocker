// permissions needed ["EMIT_LOG","INFO_WEBHOSTING","DOCROOT_ENTRIES"]
module.exports = function(moduleOptions) {
    moduleOptions = moduleOptions || {}
    moduleOptions.subsystem = "installatron"

    var fs = require("MonsterDotq").fs();

    require( "../../MonsterCommon/lib/MonsterDependencies")(__dirname)

    var config = require('MonsterConfig');
    var me = require('MonsterExpress');

    const meter = require("stream-meter");

    config.defaults({
       exec_timeout_ms: 20000,
       exec_stream_limit_byte: 10*1024,
       docker_authconfig: {},
       container_shm_size_mb: 4,
       container_tmpfs_size_kb: 65536,
       container_memory_limit_mb: 256,
       container_memory_swap_limit_mb: 256, // if these two numbers are the same, it disables swapping

       php_container_memory_limit_mb: 1024,
       php_container_memory_swap_limit_mb: 1024,

       container_ulimits: [{"Name": "nproc", "Soft": 64, "Hard": 64}],
       upgrade_shutdown_old_container_sec: 120,

       dsh_server_listen_dir: "/var/lib/monster/docker/dsh",

       mount_mysql_socket_for_webapps_path: "/var/run/mysqld",

       stop_container_wait_before_kill_s: 10,

       open_ports_min: 50001,
       open_ports_max: 60000,

       webshell_countdown_seconds: 60*60,

       tech_admin_email: "unspecified",

       mc_socket_dir: "/var/run/mc-app-sockets",

       debian_version: "stretch",

       docker_socket_path: "/var/run/docker.sock",
       global_ssh_path: "/var/lib/monster/docker/ssh",
       docker_group_id: 999,

       latest_images_path: "/var/lib/monster/docker/latest-images.config",

       installatron_lib_dir: "/var/installatron",
       frontend_lib_dir: "/var/lib/monster/frontend",
       mrtg_lib_dir: "/var/lib/mrtg",
       clamav_lib_dir: "/var/lib/clamav",
       dovecot_lib_dir: "/var/lib/dovecot",
       tinydns_lib_dir: "/var/lib/tinydns",
       axfrdns_lib_dir: "/var/lib/axfrdns",
       amavisd_storage_directory: "/storage/d1/amavisd",
       amavisd_spamassassin_conf: "/var/lib/monster/email/spamassassin",
       amavisd_dkim_keys_path: "/var/lib/monster/email/dkim-keys",
       spamassassin_storage_directory: "/storage/d1/spamassassin",
       spamassassin_watchdir_directory: "/storage/d1/mails/salearn-watchdir",
       apache_lib_dir: "/var/lib/apache",
       apache_log_dir: "/var/log/apache",
       nginx_lib_dir: "/var/lib/nginx",
       nginx_log_dir: "/var/log/nginx",
       proftpd_log_dir: "/var/log/proftpd",
       proftpd_lib_dir: "/var/lib/proftpd",
       mariadb_lib_dir: "/var/lib/mariadb",
       mariadb_log_dir: "/var/log/mysql",
       mariadb_run_dir: "/var/run/mysqld",
       mariadb_data_dir: "/storage/d1/mysql",
       mariadb_include_dir: "/var/lib/monster/dbms/mariadb.d",
       mc_ftp_databases_dir: "/var/lib/monster/ftp/db",
       ssl_docroot_dir: "/var/lib/monster/cert/ssl-docroot/",
       certificates_mc_dir: "/var/lib/monster/cert/mc/",
       certificates_active_dir: "/var/lib/monster/cert/store/active/",
       postfix_conf_d_dir: "/var/lib/monster/email/postfix-conf.d",
       postfix_spool_dir: "/var/spool/postfix",
       postfix_lib_dir: "/var/lib/postfix",

       szar_server_log_directory: "/var/log/szar-backup",
       szar_server_storage_directory: "/storage/d1/szar-server",
       szar_backup_config_dir: "/usr/local/szar-backup/etc",

       os_truststore_dirs: ["/etc/ssl/certs/", "/usr/local/share/ca-certificates/", "/usr/share/ca-certificates/"],

       docker_max_ip_lookup_attempts: 5,
       docker_hosts_file_rebuild_at_startup: false,
       docker_hosts_file_rebuild_at_startup_delay_sec: 5*60,
       docker_hosts_file_rebuild_cron: "0 * * * *",
       docker_container_upgrade_cron: "0 3 * * *",
       docker_image_cleanup_cron: "0 4 * * *",
       docker_image_cleanup_delay_days: 7,
       latest_image_cron: "20 4 * * *",

       docker_dns_resolvers: [
         "1.1.1.1",
         "8.8.8.8",
         "8.8.4.4"
       ],

       docker_extra_hosts: [],

       php_fpm_log_rotation: "20 4 * * *",
       php_fpm_log_rotation_retention_days: 7,
       
       hosts_file: "/etc/hosts",
       docker_hosts_file_webserver_app_only: true,

       pkill_path: "/usr/bin/pkill",
       socat_path: "/usr/bin/socat",
       cmd_socat_unix_listen: {"executable": "[socat_path]", args: [
          "UNIX-LISTEN:[unix_listen_socket],fork,user=[user]", "exec:docker exec -it [target_container_id] /bin/sh,pty,setsid,setpgid,stderr,ctty,sane"
       ]},

       cmd_iptables_restore: {
         executable: "/sbin/iptables-restore", args: ["--noflush"]
       },

       external_ip_addresses: [],
    })

    Array("webapp_gid","service_primary_domain","mc_server_name","public_ip","storages").forEach(k=>{
      if(!config.get(k)) throw new Error("Mandatory configuration option is missing: "+k)
    })


    config.appRestPrefix = "/docker"


    const exec_timeout_ms = config.get("exec_timeout_ms");


    const exec_stream_limit_byte = config.get("exec_stream_limit_byte");


    const Docker = require('simple-dockerode');
    var app = me.Express(config, moduleOptions);
    app.docker = new Docker(config.get("dockerode"));

    app.isSshKeys = function(value, options, key, attributes) {
        if(!value) return;
        var arr = value.split("\n");
        if(!app.isAcceptableAsSshKeyLines(arr))
           return "invalid ssh key"
        attributes[key] = arr.join("\n");
    }

    app.mcAppLabel = "mc-app";
    app.mcWebAppLabel = "mc-webapp";
    app.mcParams = "mc-params";
    app.mcImageLabelPrefix = "com.monster-cloud.";
    app.mcContainerNameLabel = app.mcImageLabelPrefix+"containerName";
    app.mcParamsFull = app.mcImageLabelPrefix+app.mcParams;

    app.volumeNameToContainer = {};

    if(config.get("dsh_server_listen_dir")) {
      require("unix-server")(app);
    }

    if(config.get("global_ssh_path")) {
       app.globalSsh = require("global-ssh")(app); 
       app.globalSsh.InitDirs().catch(console.error)
    }

    app.hostsLib = require("lib-hosts.js")({hostsFile: app.config.get("hosts_file")});

    app.isAcceptableAsSshKeyLines = function(arr) {

        for(let q of arr){
           q = q.trim();
           if(!q) continue;
           //
           if(!q.match(/^ssh-[a-z0-9]+\s+[A-Za-z0-9+/]+\s+[a-zA-Z0-9\-\.@]+$/))
             return false;
        }

        return true;
    }

    app.filterMcLabels = function(labels){
       var re = {};
       Object.keys(labels).forEach(x=>{
          if(x.startsWith(app.mcImageLabelPrefix))
             re[x] = labels[x];
       })
       return re;
    }

    app.docker.listMcContainersByImage = function(image, otherParameters){
       if(!Array.isArray(image)) image = [image]
       var params = extend({}, otherParameters, {filters: {ancestor: image}});
       return app.docker.listContainers(params);
    }
    app.docker.listMcContainersByLabels = function(labels, otherParameters){
       if(!labels) labels = [];
       if(labels.indexOf(app.mcAppLabel) < 0)
         labels.push(app.mcAppLabel);
       
       var params = extend({}, otherParameters, {filters: {label: applyLabelTransformation(labels)}});
       return app.docker.listContainers(params);
    }

    app.docker.listMcImagesByLabels = function(labels, otherParameters){
       if(!labels) labels = [];
       if(labels.indexOf(app.mcAppLabel) < 0)
         labels.push(app.mcAppLabel);

       var params = extend({}, otherParameters, {filters: {label: applyLabelTransformation(labels)}});
       return app.docker.listImages(params);
    }

    app.docker.getMcImageByID = function(ID){
      return app.docker.listMcImagesByLabels([])
        .then(images=>{
            var fimages = images.filter(x => x.Id == ID);
            if(fimages.length != 1) {
               console.error("Image not found", ID, images)
               throw new app.MError("IMAGE_NOT_FOUND");
            }

            return fimages[0];
        })
    }

    app.MError = me.Error

    app.enable("trust proxy")  // this application receives x-forwarded-for headers from other internal systems (typically relayer)

    const webstoreConfigPath = require("path").join(app.config.getVolumePath(), "webstores.config");
    app.webstoreConfigs = {};

    app.loadWebstoreConfigs = function(){
       return fs.readFileAsync(webstoreConfigPath, "utf8")
         .then(x=>{
            app.webstoreConfigs = JSON.parse(x);
         })
         .catch(ex=>{
            console.error("Unable to read webstore config", ex);
         })
    }

    app.saveWebstoreConfigs = function(){
       const lockfile = require("MonsterLockfile");
       const data = JSON.stringify(app.webstoreConfigs, null, 2);
       return lockfile.logic({lockfile: "mc-docker.lock", callback: function(lockfilePath){
            return fs.writeFileAsync(webstoreConfigPath, data, { mode: 0o600 });
       }})
    }



    var router = app.PromiseRouter(config.appRestPrefix)

    // these two lines must be placed after the PromiseRouter call
    const MonsterInfoLib = require("MonsterInfo")
    app.MonsterInfoWebhosting = MonsterInfoLib.webhosting({config: app.config, webhostingServerRouter: app.ServersRelayer})
    app.GetDocroot = app.GetRelayerMapiPool;
    app.GetEmail = app.GetRelayerMapiPool;

    router.RegisterPublicApis(["/tasks/", "/shellinabox/"])
    router.RegisterDontParseRequestBody(["/tasks/", "/shellinabox/"])

    var commanderOptions = app.config.get("commander") || {}
    commanderOptions.routerFn = app.ExpressPromiseRouter
    app.commander = require("Commander")(commanderOptions)
    router.use("/tasks", app.commander.router)

    const webhostingRouter = require("docker-webhosting-router.js")(app);
    router.use("/webhosting/:webhosting", queryWebhosting, webhostingRouter);

    router.use("/root", require("docker-root-router.js")(app, webhostingRouter));

    router.use("/shellinabox/:volumename", function(req,res,next){
       var info = app.volumeNameToContainer[req.params.volumename];
       if(!info)
          throw new app.MError("INVALID_TOKEN");
       var webstoreConfig = app.getWebstoreConfig(info.wh_id, info.containerId);
       var shellinaboxUnixSocket = webstoreConfig.shellinaboxUnixSocket;
       if(!shellinaboxUnixSocket)
          throw new app.MError("SOCKET_NOT_FOUND");

       const request = require("RequestCommon");
       var destReq = request({method: req.method, uri: "http://unix:"+shellinaboxUnixSocket+":"+req.originalUrl, headers: {
         'Host': req.headers["host"]
       }});
       destReq.on("error", function(ex){
            console.error("Unable to relay request to shellinabox (1)", ex);
            return next(ex);
       })

       req.pipe(destReq).pipe(res).on("error", function(ex){
            console.error("Unable to relay request to shellinabox (2)", ex)
            return next(ex);
       });
    });

    require("Wie")(app, router, require("lib-wie.js")(app, webhostingRouter));

    app.getWebstoreConfig = function(wh_id, containerId){
        // console.log("trying to resolve webstore config of", container);
        if(!app.webstoreConfigs[wh_id]) return;
        var rootConfig = app.webstoreConfigs[wh_id];
        if(!rootConfig[containerId]) return;
        var webstoreConfig = rootConfig[containerId];
        webstoreConfig.wh_id = wh_id;
        return webstoreConfig;
    }


    // this is for short running, one time executions with autoremoval
    app.runInNewContainer_fast = function(d, extraConstraints){

       var timer;

       var container;

       return new Promise((resolve, reject)=>{
            var data;
            var streams = {};
            var ready = {};
            var strings = {};
            Array("stdout", "stderr").forEach(cat=>{
               strings[cat] = "";
               streams[cat] = new meter(exec_stream_limit_byte).on("data", function(d){
                  strings[cat] += d;
               })
               .on("error", function(ex){
                  console.error("stream error", cat, d, ex)                
                  doReady();
               })
               .on("end", doReady);

               function doReady(){
                 ready[cat] = true;
               }
            })

            var params = extend({EntryPoint: d.command, Tty:false}, extraConstraints);
            console.log("docker.run (fast)", d.image, params);
            app.docker.run(d.image, [], [streams.stdout, streams.stderr], params, function (err, adata) {
              cleanup();
              if(err) return reject(new app.MError("RUN_ERROR", null, err));

              data = adata;
              ready.main = true;

              readyLogic();
            }).on('container', function (acontainer) {
               container = acontainer;

               timer = setTimeout(function(){
                   console.error("Killing container due to timeout", container.id, d);
                   return container.kill().catch(console.error);
               }, exec_timeout_ms);

            });

            function readyLogic(){
               if(!ready.main) return;

               return resolve({StatusCode: data.StatusCode, stdout: strings.stdout, stderr: strings.stderr})
            }


            function cleanup(){
               clearTimeout(timer);
               if(!container) return;

               if(d.autoRemove) {
                  return container.remove().catch(console.error);
               }
            }
       });

    }
    app.runInNewContainer_stream = function(d, extraConstraints){

       var timer;

       var container;
       var stream;

       var emitter = app.commander.EventEmitter();
       return emitter.spawn()
         .then(h=>{
            stream = new meter();

            var params = extend({EntryPoint: d.command, Tty:false}, extraConstraints);
            console.log("docker.run (stream)", d.image, params);
            app.docker.run(d.image, [], stream, params, function (err, adata) {
              cleanup();

              if(err) {
                 console.error("Failed running container command in stream mode", err);
                 return emitter.close(1);
              }

            }).on('container', function (acontainer) {
               container = acontainer;

               emitter.relayStream(stream);

               timer = setTimeout(function(){
                   console.error("Killing container due to timeout", container.id, d);
                   return container.kill().catch(console.error);
               }, d.timeout_ms || exec_timeout_ms);

            });


            function cleanup(){
               clearTimeout(timer);
               if(!container) return;

               if(d.autoRemove) {
                  return container.remove().catch(console.error);
               }
            }


            return h;
         })


    }

    app.executeCommandInContainer_fast = function(containerFn, d){
        var container = containerFn;
        var timer;
        return Promise.resolve()
          .then(()=>{
              timer = setTimeout(function(){
                 console.error("Killing container due to timeout", container.Id, d);
                 return container.kill().catch(console.error);
              }, exec_timeout_ms);

              console.log("container exec (fast)", d.command);
              return container.exec(d.command, {live: true, stdout: true, stderr: true});
          })
          .then(streamLink=>{
              return new Promise((resolve,reject)=>{

                  var err = false;
                  var strings = {};
                  var streams = {};
                  var ready = {};
                  Array("stdout", "stderr").forEach(cat=>{
                     strings[cat] = "";
                     streams[cat] = new meter(exec_stream_limit_byte).on("data", function(d){
                        strings[cat] += d;
                     })
                     .on("error", function(ex){
                        console.error("stream error", cat, d, ex)
                        err = true;
                        doReady();
                     })
                     .on("end", doReady);

                     function doReady(){
                       ready[cat] = true;
                       readyLogic();
                     }
                  })


                  const stream = streamLink(streams.stdout, streams.stderr);
                  stream.on('error', (ex) => {
                      console.error("stream error main", d, ex)
                      ready.main = true;
                      err = true;
                  });
                  stream.on('end', () => {
                      ready.main = true;
                      readyLogic();
                  });

                  function readyLogic() {
                    if(!ready.main) return;

                    clearTimeout(timer);

                    return resolve({err: err, stdout: strings.stdout, stderr: strings.stderr});
                  }
              })

          })
          .catch(err=>{
             clearTimeout(timer);
             throw new app.MError("EXEC_FAILED", null, err);
          })    
    }

    app.executeCommandInContainer_stream = function(containerFn, d){
        var container = containerFn;
        var timer;
        var h;
        var emitter;
        return Promise.resolve()
          .then(()=>{
              timer = setTimeout(function(){
                 console.error("Killing container due to timeout", container.Id, d);
                 return container.kill().catch(console.error);
              }, d.timeout_ms || exec_timeout_ms);

              emitter = app.commander.EventEmitter();
              return emitter.spawn();
          })
          .then(ah=>{
              h = ah;

              console.log("container exec (stream)", d.command);
              return container.exec(d.command, {live: true, stdout: true, stderr: true})
          })
          .then(streamLink=>{

              const stream = streamLink(streams.stdout, streams.stderr);
              emitter.relayStream(stream, {
                onEnd: function(){
                   clearTimeout(timer);

                }
              })

              return h;
          })
          .catch(err=>{
             if(emitter) emitter.close(1);
             clearTimeout(timer);
             throw new app.MError("EXEC_FAILED", null, err);
          })    
    }

    return app


    function queryWebhosting(req,res,next) {
        return app.MonsterInfoWebhosting.GetInfo(req.params.webhosting)
          .then(webhostingInfo=>{
              req.webhosting = webhostingInfo
              next();
          })
    }

    function applyLabelTransformation(tags){
      var re = tags.map(x => { return app.mcImageLabelPrefix+x; });
      // console.log("re", re, tags);
      return re;
    }


}
