// Note: this is incomplete (interactive pty shell is missing),
//  but non-tty/pty mode is poc'ed and working fine

module.exports = function(app){
    const { spawn } = require('child_process');

    const stream = require('stream');

    const EXIT_CODE = 255;
    const STDIN_CODE  = 0;
    const STDOUT_CODE = 1;
    const STDERR_CODE = 2;

    const CODE_AUTH = 0;
    const MAX_FRAME_SIZE = 2000;


    var docker = app.docker;

    const SOCKETFILE = app.config.get("dsh_server_listen_dir") + '/unix.sock';

    var fs = require('fs')
      , net = require('net')
      , SHUTDOWN = false
      , port = process.env.DSH_LISTEN_SOCKET || SOCKETFILE;


    var server = net.createServer({allowHalfOpen:true}, function(unixStream) {
      console.log('Docker-IPC-shell connection acknowledged.');

      unixStream.on('close', function() {
         console.log('Docker-IPC-shell client disconnected.');
      });
      unixStream.on("error", function(e){
        console.error("error on unix stream", e);
      });
      unixStream.on('data', dataListener);

      var windowBuffer = new Buffer([]);
      var piping = false;

      function processFrame(newBuffer){
         windowBuffer = Buffer.concat([windowBuffer, newBuffer]);
         var n = {};
         n.type = windowBuffer.readUInt8(0);
         n.size = windowBuffer.readUInt32LE(1);
         if(n.size >= MAX_FRAME_SIZE) throw new Error("Frame size is too huge");
         if(n.size + 5 > windowBuffer.byteLength) return; // not enough data yet
         // we are good!
         n.buffer = windowBuffer.slice(5, 5+n.size);

         windowBuffer = windowBuffer.slice(5+n.size);

         return n;
      }

      function processKeyValuePairFrame(frame){
        var lastStart = 0;
        var words = Array();
        for(var i = 0; i < frame.buffer.byteLength; i++) {
           if(frame.buffer[i] == 0) {
              var word = frame.buffer.slice(lastStart, i).toString();
              words.push(word);
              lastStart = i+1;              
           }

           if((i == frame.buffer.byteLength-1)&&(lastStart != i+1))
            throw new Error("Badly formatted key value pairs");
        }

        if(words.length % 2 != 0) {
           throw new Error("Odd number of values in key value structure");
        }

        var re = {};
        for(var i = 0; i < words.length; i+= 2) {
           var k = words[i];
           var v = words[i+1];
           var t = typeof(re[k]);
           if(t != "undefined") {
             if(t == "string")
                re[k] = Array(re[k]);
             re[k].push(v);
           } else {
             re[k] = v;
           }
        }
        return re;
      }

      function processAuthFrame(frame){
        var kvs = processKeyValuePairFrame(frame);
        kvs.cmdExec = kvs["p"];
        delete kvs.p;
        if(typeof(kvs.cmdExec) == "string") 
          kvs.cmdExec = [kvs.cmdExec];
        kvs.whId = parseInt(kvs.secret, 10);
        return kvs;
      }

      function dataListener(buffer) {
         try {
           var n = processFrame(buffer);
           if(!n) return; // not enough data

           switch(n.type){
              case CODE_AUTH:
                var authFrame = processAuthFrame(n);
                console.log("auth frame received", authFrame);
                authorizeCall(authFrame);
                switch(authFrame.cmd) {
                   case "ping": sendOut("pong\n"); break;
                   case "ps": pipeProcess("/usr/local/sbin/dpswh", [authFrame.whId]); break;
                   case "sh":  break; // setup an interactive terminal via dockerode
                   case "exec": dockerExec(authFrame.container, authFrame.cmdExec);  break; // just simply execute the command
                   default:
                     throw new Error("Unknown command");
                }

                break;
              default:
              throw new Error("Unknown frame type "+n.type);
           }

         }catch(e){
            console.error("Error while processing initial message of client", n, e);
            return finish("Invalid command or unknown error\n");            
         }


         if(piping) {
            unixStream.removeListener("data", dataListener);
         } else {
            finish();
         }



         function getWrapperStream(writer) {
            var echoStream = new stream.Writable();
            echoStream._write = function (chunk, encoding, done) {
              return writer(chunk, done);
            };
            echoStream.on("error", function(e){
               console.error("error on the echo stream", e);
            });
            return echoStream;
         }

         function int8ToBuf(value) {
            var buf = new Buffer(1);
            buf.writeUInt8(value, 0);
            return buf;
         }
         function int32ToBuf(value) {
            var buf = new Buffer(4);
            buf.writeUInt32LE(value, 0);
            return buf;
         }

         function sendMsg(type, buf, cb) {
            if(typeof buf  === "string") buf = new Buffer(buf);
            var a = [
              int8ToBuf(type),
              int32ToBuf(buf.byteLength),
              buf,
            ];
            var merged = Buffer.concat(a);
            try {
               unixStream.write(merged, cb);
            } catch(e) {
               console.error("Error while sending to unix stream", e);
               finish();
            }
         }
         function sendIn(cb) {
            sendMsg(STDIN_CODE, "", cb);
         }
         function sendErr(buf, cb) {
            sendMsg(STDERR_CODE, buf, cb);
         }
         function sendOut(buf, cb) {
            sendMsg(STDOUT_CODE, buf, cb);
         }
         function sendExitCode(value, cb) {
            sendMsg(EXIT_CODE, int32ToBuf(value), function(){
               finish();
               if(cb) cb();
            });
         }
         function finish(optionalMsg){
           if(optionalMsg)
              sendErr(optionalMsg);
           unixStream.destroy();
         }

         function isStringArray(d){
            if(!Array.isArray(d)) return false;
            var re = true;
            d.some(function(x){
               if(typeof x !== "string") {
                   re = false;
                   return true;
               }
            })
            return re;
         }

         function dockerExec(targetContainer, execArr){
           if(!isStringArray(execArr)) throw new Error("Must be called with a simple string array");

           // get the container based on d.container
           if(!targetContainer) throw new Error("Container not specified")
           console.log("DSH: Executing in", targetContainer, execArr)

           var container = docker.getContainer(targetContainer);
           if(!container) return finish("Container not found");

           piping = true;

           container.execRaw({Cmd: execArr,  AttachStdin: true, AttachStdout: true, AttachStderr: true}, function(err, exec) {
              if(err) {
                 console.error("err exec", err);
                 return finish("Error during exec");
              }
              exec.start({hijack: true, stderr:true, stdin: true, Detach: false}, function(err, execStream) {
                if(err) {
                    console.error("err start", err);
                    return finish("Error during start");
                }

                sendIn();

                execStream.on("error", function(e){
                  console.error("error on docker exec stream", e);
                });

                // input is piped directly
                unixStream.pipe(execStream);

                // Fortunately, we have a regular TCP socket now, so when the readstream finishes and closes our
                // stream, it is still open for reading and we will still get our results :-)
                docker.modem.demuxStream(execStream, getWrapperStream(sendOut), getWrapperStream(sendErr));

                execStream.on("end", function(){
                    exec.inspect((err, ins)=>{
                       console.log("exec inspect returned", err, ins);
                       sendExitCode(ins.ExitCode);
                    });
                });
              });
           });
         }

         function pipeProcess(cmd, args){
            piping = true;
            const child = spawn(cmd, args);

            child.stdout.on("error", function(e){
               console.error("error on pipe stdout", e);
            })
            child.stdout.pipe(getWrapperStream(sendOut))

            child.stderr.on("error", function(e){
               console.error("error on pipe stderr", e);
            })
            child.stderr.pipe(getWrapperStream(sendErr))

            child.on("error", function(e){
              console.error("error on spawn child", e);
            });
            child.on("close", function(code){
               sendExitCode(code);
            });
         }
      }
    })
    .listen(port, function() {
      console.log("Docker-IPC-shell listening on " + port);
    })
    .on('listening', function() {
        // set permissions
        return fs.chmod(port, 0777, function(){});
    })
    // double-check EADDRINUSE
    .on('error', function(e) {
        if (e.code !== 'EADDRINUSE') throw e;
        net.connect({ path: port }, function() {
          // really in use: re-throw
          throw e;
        }).on('error', function(e) {
          if (e.code !== 'ECONNREFUSED') throw e;
          // not in use: delete it and re-listen
          console.log("Removing stalled socket")
          fs.unlinkSync(port);
          server.listen(port);
        });
    });

    process.on('SIGINT', cleanup);
    process.on('SIGTERM', cleanup);


    function cleanup(){
      if(SHUTDOWN) return;
      SHUTDOWN = true;
      console.log('\n',"Terminating.",'\n');
      server.close();
      process.exit(0);
    }

    function authorizeCall(d){
      if(!d.whId) throw new Error("whId is not present");
      if(!d.secret) throw new Error("secret is not present");

      d.whId = ""+d.whId;

      // check if it is part of the known containers
      if(!app.webstoreConfigs[d.whId]) throw new Error("no such wh id");
      var found = false;
      var targetContainerFound = false;
      Object.keys(app.webstoreConfigs[d.whId]).forEach(function(cid){
         var params = app.webstoreConfigs[d.whId][cid].dsh_token;
         if((params == d.secret)) {
            found = true;
         }

         if(d.container) {
            // verify d.container belongs to the same wh
            if(cid.indexOf(d.container) === 0) {
               targetContainerFound = true;
               d.container = cid;
            }
         }

      })

      if(!found) throw new Error("Invalid authorization code");
      if((d.container)&&(!targetContainerFound)) throw new Error("Target container not found")

    }

}
