We are hosting a prebuilt fuse-bindings package here as we want to avoid the webhosting servers becoming build machines.
To upgrade, you can rebuild this directory by typing npm install fuse-bindings; make, g++ and their dependencies must be installed.
