module.exports = function(app){
    const dotq = require('MonsterDotq');
    const fs = dotq.fs();

    const path = require("path");

    var ssh_dir = app.config.get("global_ssh_path");
    var docker_group_id = app.config.get("docker_group_id");

    const containerTargetFilename = "container-target.txt";

    var ValidatorsLib = require("MonsterValidators")
    var vali = ValidatorsLib.ValidateJs()
    vali.validators.isSshKeys = app.isSshKeys;

    var containerIdToUser = {};
    var userIdToContainer = {};

    var re = {
       InitDirs: function(){

          return mkdir(ssh_dir)
          .then(()=>{
              return dotq.linearMap({array: ["dropbear", "etc", "home"], action: d=>{
                  var p = path.join(ssh_dir, d);

                  return mkdir(p)
              }})

          })
          .then(()=>{
              // read container-target.txt files and build containerIdToUser
              return fs.readdirAsync(path.join(ssh_dir, "home"))
          })
          .then(userIds=>{
             return dotq.linearMap({array:userIds, catch: true, action: userId=>{
                 return fs.readFileAsync(getContainerTargetPath(userId), "utf8")
                   .then(containerId=>{
                      containerIdToUser[containerId] = userId;
                      userIdToContainer[userId] = containerId;
                   })
             }})
          })
       },

       GetConfig: function(userId){
          var re = {};
          var ap;
          var tp;
          return Promise.resolve()
            .then(()=>{
                userId = validateUserId(userId);
                ap = getAuthorizedKeysPath(userId);

                return fs.readFileAsync(ap, "utf8")
                .then(x=>{
                  console.log("hey", x)
                   re.authz = x;
                })
                .catch(ex=>{
                   console.error("Authorized keys files does not exist:", ap)
                })
                .then(()=>{
                   tp = getContainerTargetPath(userId);
                   return fs.readFileAsync(tp, "utf8")
                })
                .then((x)=>{
                  re.targetContainer = x;
                })
                .catch(ex=>{
                   console.error("Container target file does not exist:", tp)
                 })
            })
            .then(()=>{
               return re;
            })
       },

       SaveAuthorizedKeys: function(userId, authorizedKeys) {
          return Promise.resolve()
            .then(()=>{
                userId = validateUserId(userId);

                return validateAuthorizedKeys(authorizedKeys);
             })
             .then((d)=>{
                return mkdir(getHomeDir(userId))
                  .then(()=>{
                      return mkdir(getSshDir(userId))
                  })
                  .then(()=>{
                      return fs.writeFileAsync(getAuthorizedKeysPath(userId), d.authorizedKeys);
                  })
             })
       },

       CleanupWebhosting: function(userId){        
          return re.SaveTargetContainer(userId, null)
            .then(()=>{
               var a = getAuthorizedKeysPath(userId);
               return fs.ensureRemovedAsync(a);
            })
            .then(()=>{
               return fs.ensureRmdiredAsync(getSshDir(userId));
            })
            .then(()=>{
               return fs.ensureRmdiredAsync(getHomeDir(userId));
            })
       },

       GeneratePasswdFileForHash: function(hash){
          // 11005:x:11005:999::/host/home/11005:/bin/dockersh

          var c = "";
          Object.keys(hash).forEach(userId=>{
             c += `${userId}:x:${userId}:${docker_group_id}::/host/home/${userId}:/bin/dockersh\n`;
          })

          return c;
       },

       SaveEtcPasswd: function(){
          var p = path.join(ssh_dir, "etc", "passwd");
          var c = re.GeneratePasswdFileForHash(userIdToContainer);

          return fs.writeFileAsync(p, c)

       },

       ChangeTargetContainerIfPresent: function(userId, oldContainerId, newContainerId){
          return Promise.resolve()
            .then(()=>{
                if(!containerIdToUser[userId]) return;

                return re.SaveTargetContainer(userId, newContainerId);
            })
       },

       SaveTargetContainer: function(userId, containerId) {
           return Promise.resolve()
             .then(()=>{
                 if((containerId)&&(!containerId.match(/^[a-z0-9_]+$/)))
                  throw new Error("Invalid container ID!"); // note underscore is allowed for some mocked unit tests to work
                 userId = validateUserId(userId);

                 var p = getContainerTargetPath(userId);
                 if(!containerId)
                   return fs.ensureRemovedAsync(p);
                 else
                   return creteHomedir(userId).then(()=>{
                      return fs.writeFileAsync(p, containerId);
                   }) 
             })
             .then(()=>{
                 var b = JSON.stringify(Object.values(containerIdToUser));
                 if(containerId) {
                    containerIdToUser[containerId] = userId;
                    userIdToContainer[userId] = containerId;
                 } else {
                    var c = userIdToContainer[userId];
                    if(c){
                       delete containerIdToUser[c];
                       delete userIdToContainer[userId];
                    }
                 }
                 var a = JSON.stringify(Object.values(containerIdToUser));
                 if(b != a) {
                   return re.SaveEtcPasswd();                  
                 }
             })
       }
    };

    return re;

    function getHomeDir(userId){
       return path.join(ssh_dir, "home", ""+userId);
    }
    function getSshDir(userId){
       return path.join(getHomeDir(userId), ".ssh");
    }

    function getAuthorizedKeysPath(userId){
      return path.join(getSshDir(userId), "authorized_keys");
    }

    function validateAuthorizedKeys(authorizedKeys) {
        return vali.async({authorizedKeys:authorizedKeys}, {authorizedKeys:{presence: true, isString: true, isSshKeys: true}});
    }
    function validateUserId(userId){
           userId = "" + userId;
           if(!userId.match(/^[0-9]+$/))
            throw new Error("Invalid user ID");
          return userId;      
    }

    function mkdir(p) {
       return fs.mkdirAsync(p).catch(x=>{
          if(x.code == "EEXIST") return;
          throw x;
       })
    }

    function getContainerTargetPath(userId){
        return path.join(ssh_dir, "home", userId, containerTargetFilename);
    }

    function creteHomedir(userId){
        var p = getHomeDir(userId);
        return mkdir(p);
    }
}
