var eLib = module.exports = function(options){
   if((!options)||(typeof options != "object")) throw new Error("options not procided")
   var hostsFile = options.hostsFile;
   if(!hostsFile) throw new Error("hostsFile not procided");


   const lockFileName = "mc-hosts";

   const dotq = require("MonsterDotq");
   const fs = dotq.fs();
   const lockFile = require("MonsterLockfile");

   var lib = {};


   // returns the parsed entries and the raw content
   function readContent(){
   	  return fs.readFileAsync(hostsFile, "utf8")
   	    .catch(ex=>{
   	    	console.error("hostsFile read error", hostsFile, ex);
   	    	return "";
   	    })
   	    .then(eLib.parse)
   }
   function entriesToStr(entries){
   	  var re = "";
   	  Object.keys(entries).forEach(host=>{
   	  	 var ip = entries[host];
   	  	 re += `${ip} ${host}\n`;
   	  });
   	  return re;
   }
   function writeEntriesWhenNeeded(entries){
   	  var contentStr = entriesToStr(entries);
   	  return readContent()
   	    .then(current=>{
		    if(contentStr == current.mcStr) 
		       return; // nothing to do

		    var beforeStr = current.nonMcStr;
		    if(beforeStr) 
		    	beforeStr = beforeStr.trim() + "\n";
            return fs.writeFileAsync(hostsFile, beforeStr + eLib.wrapper + "\n" + contentStr + eLib.wrapper + "\n");
   	    })
   }

   lib.getEntries = function(){
   	  return readContent()
   	    .then(current=>{
   	    	return current.entries;
   	    })
   }

   lib.setHostEntries = function(entries) {
   	  return lockFile.logic({lockfile: lockFileName, callback: function(){
   	  	  return writeEntriesWhenNeeded(entries);
   	  }});
   }
   lib.appendEntry = function(host, ip){
   	  return typical(current=>{
	    	current.entries[host] = ip;
	        return writeEntriesWhenNeeded(current.entries);
   	  })
   }
   lib.removeEntry = function(host){
   	  return typical(current=>{
    	delete current.entries[host];
        return writeEntriesWhenNeeded(current.entries);
   	  })
   }
   return lib;

   function typical(cb){
   	  return lockFile.logic({lockfile: lockFileName, callback: function(){
   	  	  return readContent().then(cb)
   	  }})
   }
}
eLib.wrapper = "# MC";
eLib.parse = function(str){
	var lines = str.split("\n");
	var re = {mcStr: "", entries: {}, nonMcStr: ""};
	var inside = false;
	lines.forEach(line=>{
		if(line == eLib.wrapper) {
			inside = !inside;
		} else if(inside) {
			re.mcStr += line + "\n";
			var m = line.split(/\s+/);
			re.entries[m[1]] = m[0];
		}
		else {
			re.nonMcStr += line +"\n";
		}
	})

	if(re.nonMcStr)
		re.nonMcStr = re.nonMcStr.trim() +"\n";

	return re;
}
