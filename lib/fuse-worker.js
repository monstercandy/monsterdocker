module.exports = function(app){

  console.log("worker instansiated", app.mknodTable, app.groupDirTable);

  if(!app.groupDirTable) {
    app.groupDirTable = {};
  }

  const statsHelper = {
    directory: {
        mtime: new Date(),
        atime: new Date(),
        ctime: new Date(),
        nlink: 1,
        size: 4096,
        mode: 16877,
        uid: 0,
        gid: 0,
    },
    socket: {
        mtime: new Date(),
        atime: new Date(),
        ctime: new Date(),
        nlink: 1,
        size: 4096,
        mode: 49645,
        uid: 0,
        gid: 0,
    },
    symlink: {
        mtime: new Date(),
        atime: new Date(),
        ctime: new Date(),
        nlink: 1,
        size: 4096,
        mode: 41471, // symlink
        uid: 0,
        gid: 0,
    }
  };

  const dirRegexp = new RegExp('^(/([0-9]+)-([^/]+))/socket$');
  function getDirForSocket(path){
     return dirRegexp.exec(path);
  }

  const groupRegexp = new RegExp('^(/sys-([a-zA-Z0-9\-]+))(?:/([a-z0-9\-]+))?(?:/(.+))?/?$');   
  function getDirForGroup(path){
     var m = groupRegexp.exec(path);
     if(m) {
        m.mknodKey = m[1]+"/"+m[3];
     }
     return m;
  }

  function isGroupDirAccessibleFor(context, groupName){
      console.log("checking group access", groupName, context);
      // for root:
      if(context.uid === 0) return true;

      if(!app.config.groupDirAcl) return false;
      if(!app.config.groupDirAcl[groupName]) return false;
      var groupAccessGids = app.config.groupDirAcl[groupName].gids;
      if((groupAccessGids)&&(-1 < groupAccessGids.indexOf(context.gid)))
         return true;
      var groupAccessUids = app.config.groupDirAcl[groupName].uids;
      if((groupAccessUids)&&(-1 < groupAccessUids.indexOf(context.uid)))
         return true;
      return false;
  }

  function isSocketAccessibleFor(context, user){
      if((context.uid === 0)||(user == ""+context.uid)) return true;
      var socketAccessGids = app.config.socketAccessGids;
      if((socketAccessGids)&&(-1 < socketAccessGids.indexOf(context.gid)))
         return true;
      var socketAccessUids = app.config.socketAccessUids;
      if((socketAccessUids)&&(-1 < socketAccessUids.indexOf(context.uid)))
         return true;

      return false;      
  }

  function isDirlistAvailableFor(context){
      if(context.uid === 0) return true;
      var dirlistUids = app.config.dirlistUids;
      if((dirlistUids)&&(-1 < dirlistUids.indexOf(context.uid)))
         return true;
      var dirlistGids = app.config.dirlistGids;
      if((dirlistGids)&&(-1 < dirlistGids.indexOf(context.gid)))
         return true;

      return false;      
  }

  return {
      readdir: function(path, cb){
         var c = app.fuse.context();
         console.log("readdir on", path, "by", c);
         if(path == "/") {
            // root users are allowed to see the available stuff
            if(!isDirlistAvailableFor(c)) {
                return cb(0, []);
            }

            var dirs = [];
            var h = {};
            Object.keys(app.mknodTable).forEach(t=>{
               var n = t.substr(1);
               var i = n.indexOf("/");
               if(i > 0)
                 n = n.substr(0, i);
               if(h[n]) return;
               h[n] = 1;
               dirs.push(n);
            })

            return cb(0, dirs);
         }
         else {
            // is group dir
            var groupPath = getDirForGroup(path);
            if(groupPath) {

              /*
                this is a group directory, which is more special.
                /sys-amavisd/ver-123456789/socket
                1: /sys-amavisd
                2: amavisd
                3: ver-123456789
                4: socket

                Checking entitlements first.
              */
              if(!isGroupDirAccessibleFor(c, groupPath[2])) {
                 return cb(0, []);
              }

              if(groupPath[4]) {
                 // this is a subdir of the subdir of a group directory, which can never be a directory, so it cannot be readdir'ed
                 return cb(app.fuse.ENOTDIR);
              }
              if(groupPath[3]) {
                 // this is a subdir of a group directory, need to return a socket file if it is present
                 if(!app.mknodTable[groupPath.mknodKey]) return cb(0, []);

                 return cb(0, ["socket"]);
              } else {
                 // this is the root of a group directory, need to return the version entries and a latest symlink
                 var t = app.groupDirTable[groupPath[1]];
                 if(!t) return cb(0, []);
                 var re = [];
                 for(var q of t.apps) {
                    re.push(q);
                 }
                 if(t.latest) re.push("latest");
                 return cb(0, re);
              }
            } else {
              // this is a simple subdirectory, which can be listed by the respective owner or root
              var socketPath = path+"/socket";
              var mpath = getDirForSocket(socketPath);
              if((!mpath)||(!app.mknodTable[mpath[1]])) return cb(0, []);

              if(!isSocketAccessibleFor(c, mpath[2])) {
                 return cb(0, []);
              }

              // all good!
              return cb(0, ["socket"]);
            }
         }
      },
      readlink: function(path, cb) {
         var groupPath = getDirForGroup(path);
         if(!groupPath) return cb(app.fuse.ENOENT);
         var t = app.groupDirTable[groupPath[1]];
         if((!t)||(!t.latest)) return cb(app.fuse.ENOENT);
         return cb(0, t.latest);
      },
      getattr: function (path, cb) {
         if(path == "/") {
            return cb(0, statsHelper.directory);
         }

         var c = app.fuse.context();
         var socketDir = getDirForSocket(path);
         if(socketDir) {
            if(app.mknodTable[socketDir[1]]) {
              // disclosing the socket only for the privileged processes
              if(isSocketAccessibleFor(c, socketDir[2]))
                return cb(0, statsHelper.socket);
            }
         }
         else {

           var socketPath = path+"/socket";
           socketDir = getDirForSocket(socketPath);
           if(socketDir) {
              return cb(0, statsHelper.directory);
           }

           var groupPath = getDirForGroup(path);
           if(groupPath) {


              if(!isGroupDirAccessibleFor(c, groupPath[2]))
                 return cb(app.fuse.EPERM);

              if(groupPath[4]) {
                 if((groupPath[4] != "socket")||(!app.mknodTable[groupPath.mknodKey])) return cb(app.fuse.ENOENT);
                 // this means the socket already exists here!
                 return cb(0, statsHelper.socket);
              }
              else if(groupPath[3]) {
                 if(groupPath[3] == "latest") {
                    // we need to check if the symlink already exists
                    var t = app.groupDirTable[groupPath[1]];
                    if((!t)||(!t.latest)) return cb(0, app.fuse.ENOENT);
                    return cb(0, statsHelper.symlink);
                 }

                 // this is a subdirectory.
                 return cb(0, statsHelper.directory);
              }

              // otherwise it is just the root of an app group dir
              return cb(0, statsHelper.directory);
           }
         }

         return cb(app.fuse.ENOENT);
     },

     mknod: function(path, mode, stuff, cb) {
        console.log("mknod", path, mode, stuff);

        // 140755                140660
        // 1*8^5 = 32768
        // 4*8^4

        // 49645 = 140755
        // 49584 = 140660
        // 49640 = 140750        
        // 49152 = 140000 -- clamav...
        // 49590 = 140666 -- dovecot-lmtp
        if(Array(0o140755,0o140660,0o140750,0o140000,0o140666).indexOf(mode) < 0) {
           return cb(app.fuse.EPERM);
        }

        var c = app.fuse.context();
        var socketDir = getDirForSocket(path);
        if(socketDir) {
          // the classical use case
          if(socketDir[2] != ""+c.uid) {
             return cb(app.fuse.EPERM);
          }

          app.mknodTable[socketDir[1]] = true;

          return cb(0);

        } else {
           var groupPath = getDirForGroup(path);

           if((groupPath)&&(groupPath[4])&&(groupPath[4] == "socket")) {

              if(!isGroupDirAccessibleFor(c, groupPath[2]))
                 return cb(app.fuse.EPERM);

              var t = app.groupDirTable[groupPath[1]];
              if(!t) t = app.groupDirTable[groupPath[1]] = {apps:[]};

              var appVer = groupPath[3];
              if(t.apps.indexOf(appVer) < 0)
                 t.apps.push(appVer);
              t.latest = appVer;
              app.mknodTable[groupPath.mknodKey] = true;
              return cb(0);
           }
        }
        return cb(app.fuse.ENOTDIR);

     },

     chmod: function(path, mode, cb) {
        console.log("chmod", path, mode);
        return cb(0);
     },

     unlink: function(path, cb) {
        console.log("unlink", path);
        var socketDir = getDirForSocket(path);
        if(socketDir) {
            delete app.mknodTable[socketDir[1]];
        }

        var groupPath = getDirForGroup(path);
        if(groupPath) {
            delete app.mknodTable[groupPath.mknodKey];

            // also removing the app
            var appVer = groupPath[3];
            if(appVer) {
               var t = app.groupDirTable[groupPath[1]];
               if(t) {
                  var i = t.apps.indexOf(appVer);
                  if(i > -1)
                     t.apps.splice(i, 1);
                  if((t.latest)&&(t.latest == appVer)) {
                      delete t.latest;
                      for(var q of t.apps) {
                         t.latest = q;
                         break;
                      }
                  }
               }
            }
        }
        return cb(0);
     }
  }
}
