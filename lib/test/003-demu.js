require( "../../../MonsterCommon/lib/MonsterDependencies")(__dirname)

  const assert = require('assert');

  const fs = require("MonsterDotq").fs();

  const demuLib = require("lib-demu.js")();

  var CombinedStream = require('combined-stream');
  const stream = require("stream");

  describe('real example', function() {

        it("one simple message", function(done){
          var i = fs.createReadStream("test/003-test1.bin");
          var o = getBufferedWriter();
          demuLib(i, o, null, function(ec){
              assert.equal(ec, 0);

              assert.equal("bin\n", o.toString());
              done();
          })
        })

/*
// disabled: combinedStream not working as expected
        it("an incomplete message", function(done){

            var combinedStream = CombinedStream.create();
            combinedStream.append(fs.createReadStream('test/003-test2-1.bin'));
            combinedStream.append(fs.createReadStream('test/003-test2-2.bin'));

            var o = getBufferedWriter();
            demuLib(combinedStream, o, null, function(ec){
                assert.equal(ec, 0);

                assert.equal("bin\n", o.toString());
                done();
            })
        })
*/
        it("two messages at once", function(done){
          var i = fs.createReadStream("test/003-test3.bin");
          var o = getBufferedWriter();
          demuLib(i, o, null, function(ec){
              assert.equal(ec, 0);

              var x = fs.readFileSync("test/003-test3.bin.xxx", "utf-8")
              assert.equal("bin\nbin\n", o.toString());
              done();
          })
        });

        it("exit code", function(done){
          var i = fs.createReadStream("test/003-test4.bin");
          var o = getBufferedWriter();
          demuLib(i, o, null, function(ec){
              assert.equal(ec, 2);

              assert.equal("bin\n", o.toString());
              done();
          })
        })


        function getBufferedWriter(){
            var n = "";
            var echoStream = new stream.Writable();
            echoStream._write = function (chunk, encoding, done) {
               n += chunk.toString();
               return done();
            };
            echoStream.toString = function(){
              return n;
            }
            return echoStream;
        }
    })

