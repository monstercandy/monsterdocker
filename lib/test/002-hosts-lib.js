require( "../../../MonsterCommon/lib/MonsterDependencies")(__dirname)

  const assert = require('assert');

  const fs = require("MonsterDotq").fs();

  const hostsLib = require("lib-hosts.js");

  const firstTest = "line1-before\nline2-before\n# MC\n1.1.1.1 hostname1\n2.2.2.2 hostname2\n# MC\nline1-after\nline2-after\n";

	describe('basic stuff', function() {

        it("parsing test #1 (before after)", function(){
            var r = hostsLib.parse(firstTest);

            assert.deepEqual(r, {
              "entries": {
                "hostname1": "1.1.1.1",
                "hostname2": "2.2.2.2"
              },
              "mcStr": "1.1.1.1 hostname1\n2.2.2.2 hostname2\n",
              "nonMcStr": "line1-before\nline2-before\nline1-after\nline2-after\n",
            });
        });

        it("parsing test #2 (no stuff after)", function(){
            var r = hostsLib.parse("line1-before\nline2-before\n# MC\n1.1.1.1 hostname1\n2.2.2.2 hostname2\n# MC\n");

            assert.deepEqual(r, {
              "entries": {
                "hostname1": "1.1.1.1",
                "hostname2": "2.2.2.2"
              },
              "mcStr": "1.1.1.1 hostname1\n2.2.2.2 hostname2\n",
              "nonMcStr": "line1-before\nline2-before\n",
            });
        });

        it("parsing test #3 (no stuff before)", function(){
            var r = hostsLib.parse("# MC\n1.1.1.1 hostname1\n2.2.2.2 hostname2\n# MC\nline1-after\nline2-after\n");

            assert.deepEqual(r, {
              "entries": {
                "hostname1": "1.1.1.1",
                "hostname2": "2.2.2.2"
              },
              "mcStr": "1.1.1.1 hostname1\n2.2.2.2 hostname2\n",
              "nonMcStr": "line1-after\nline2-after\n",
            });
        });

        it("parsing test #4 (no mc)", function(){
            var r = hostsLib.parse("line1-before\nline2-before\nline1-after\nline2-after\n");

            assert.deepEqual(r, {
              "entries": {
              },
              "mcStr": "",
              "nonMcStr": "line1-before\nline2-before\nline1-after\nline2-after\n",
            });
        });
    })


  describe('real example', function() {


        it("full test", function(){
            const testFile = "vol.d/test.txt";
            var hosts = hostsLib({hostsFile: testFile});
            return fs.writeFileAsync(testFile, firstTest)
              .then(()=>{
                 return hosts.appendEntry("hostname3", "3.3.3.3")
              })
              .then(()=>{
                 return fs.readFileAsync(testFile, "utf8")
              })
              .then(str=>{
                 assert.equal(str, "line1-before\nline2-before\nline1-after\nline2-after\n# MC\n1.1.1.1 hostname1\n2.2.2.2 hostname2\n3.3.3.3 hostname3\n# MC\n");
              })
        });

    })

