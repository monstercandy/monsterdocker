require( "../../../MonsterCommon/lib/MonsterDependencies")(__dirname)

var ExpressTesterLib = require("ExpressTester")
var app = require( "../docker-app.js")(ExpressTesterLib.moduleOptions)

ExpressTesterLib(app, function(mapi, assert){

  const goodSshKeyLine = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDP+MqNwhwE6MWI9QlAbpNqJpCCLb34nZ8pT1Ce1TnVK6LGla5CKtZae4A00DRRjgZSs2KgWJAr7NKNG+S4WOMwX4u897YDi9r4HZGL76t/ayXXocfIthVHbTUBaEPTjDAEgF26yYraV8LUaBkoaI+Pwzf3yNpZMIKNXNXDhzQLl0hbqdRuSBVcEoZobfjHywPSgEn2/5hpympWCGiMllN6miEd/zvW2zKKr0r20YRdqQ5T8Ch1370oDOt36LnImnSUQFGnPaJm45e9MYuWMrBnb87EhJIoT9cdn7Y6QnluBceOuV5sP50SDaa/1cKMlwvtzYtDH+u5rHlbukSS1fPj root@Cubian";

  const fs = require("fs");
  const path = require("path");

  const c_user_id = "123123";

  var oCoLi = app.docker.listContainers;
  var oImages = app.docker.listImages;

  describe('system images', function() {
        var amavisd_milter_container_id = "123456789abcdef";

        it('creating a new system image: stretch-amavisd-milter', function() {

            var containerLists = 0;

            app.docker.listContainers = function(optsf) {
               containerLists++;
               // console.inspect(optsf)
               if(containerLists == 1) {
                  assert.deepEqual(optsf, { filters:
   { label:
      [ 'com.monster-cloud.containerName=stretch-amavisd-milter',
        'com.monster-cloud.mc-app' ] } }); 
               }
               else {
                  assert.deepEqual(optsf, { "not-expected": "more-lists" });                
               }
               return Promise.resolve([]);
            }

            app.docker.listImages = function(optsf) {
               // console.inspect(optsf)
               assert.deepEqual(optsf, { filters: { label: [ 'com.monster-cloud.mc-app' ] } });
               return Promise.resolve([{ Containers: -1,
    Created: 1529237078,
    Id: 'sha256:e189adde95ba093f3a41e918c4734ae6a0cf04f9be962a8ee4d85dc6bfa61543',
    Labels: { 
      "com.monster-cloud.mc-app": 'system',
      "com.monster-cloud.mc-params": `{"unix-socket-groupname":"amavisd-milter","unix-socket-deps":["amavisd-new"]}`
    },
    ParentId: 'sha256:3fd9065eaf02feaf94d68376da52541925650b81698c53c6824d92ff63f98353',
    RepoDigests: null,
    RepoTags: [ 'stretch-amavisd-milter:2018-10-04--1' ],
    SharedSize: -1,
    Size: 4147781,
    VirtualSize: 4147781 },

     ]);

            }

            var labels;
            var containerCreated = 0;
            var containerStarted = 0;
            
             var hostAddEntry = 0;
             var oHostsLib = app.hostsLib;
             app.hostsLib = {
                appendEntry: function(hostname, ip){
                   assert.equal(hostname, "fadoring_leavitt");
                   assert.equal(ip, "123.123.123.123");
                   hostAddEntry++;
                   return Promise.resolve();
                }
             };

                     
            var oCreate = app.docker.createContainer;
            app.docker.createContainer = function(aoptsf){

               var optsf = simpleCloneObject(aoptsf);

               // console.inspect(optsf, 123)

               assert.equal(optsf.HostConfig.Mounts.length, 2);
               assert.ok(optsf.HostConfig.Mounts[0].Source.startsWith('/var/run/mc-app-sockets/sys-amavisd-milter/'));

               delete optsf.HostConfig.Mounts[0].Source;



               containerCreated++;

               assert.deepEqual(
                  optsf,
                  { AttachStdin: false,
  AttachStdout: false,
  AttachStderr: false,
  Tty: false,
  OpenStdin: false,
  StdinOnce: false,
  Labels: { 
    'com.monster-cloud.containerName': 'stretch-amavisd-milter',
    'com.monster-cloud.mc-app': 'system',
    'com.monster-cloud.mc-params': '{"unix-socket-groupname":"amavisd-milter","unix-socket-deps":["amavisd-new"]}' 
  },
  ExposedPorts: {},
  HostConfig:
   { 
     Dns: ["1.2.3.4"],
     DnsSearch: ["."],
     NetworkMode: "host",
     ExtraHosts:
      [ 'server-name:9.8.7.6',
        'server-name.fooo.hu:9.8.7.6',
        'extra.foobar.hu:9.8.7.6' ],
     LogConfig: { "Type": "json-file", "Config": {"max-size": "3m", "max-file": "2"} },
     // Tmpfs: { '/tmp': 'rw,noexec,nosuid,size=65536k' },
     ShmSize: 4194304,
     Memory: 268435456,
     MemorySwap: 268435456,
     Ulimits: [ { Name: 'nproc', Soft: 64, Hard: 64 } ] ,
     Binds: [
       "/storage/d1/amavisd:/storage/d1/amavisd"
     ],
     Mounts: [ { Type: 'bind',
          Target: '/var/run/mc-app-socket' },
        { Type: 'bind',
          Source: '/var/run/mc-app-sockets/sys-amavisd-new',
          Target: '/var/run/amavisd-new' } ],
     PortBindings: { },
     RestartPolicy: { Name: 'unless-stopped' },
  },
  User: '1000:1000',
  Env: [],
  Cmd: [],
  Image: 'stretch-amavisd-milter:2018-10-04--1' }

               );


               labels = optsf.Labels;

               return Promise.resolve({
                  id: amavisd_milter_container_id,
                  inspect: function(){
                      return Promise.resolve({
                         Id: amavisd_milter_container_id,
                         Name: "/fadoring_leavitt",
                        State: {
                           Status: "running",
                        },
                           NetworkSettings: {
                              IPAddress: "123.123.123.123",
                           },
                         Config: {
                           Labels: labels,
                         }
                      })
                  },
                  start: function(){
                     containerStarted++;
                     return Promise.resolve();
                  }
               }); // note the lowercased id here
            }

            return mapi.postAsync("/root/system/stretch-amavisd-milter/start", {})
              .then(re => {
                 // note: the docker response is filtered, some of the fields are stripped
                 assert.deepEqual(re.result, {container:amavisd_milter_container_id});
                 assert.equal(containerLists, 1);
                 assert.equal(containerCreated, 1);
                 assert.equal(containerStarted, 1);
                 assert.equal(hostAddEntry, 1);

                 app.hostsLib = oHostsLib;

                 app.docker.listImages = oImages;
                 app.docker.createContainer = oCreate;
              })

        })

        it('starting a system image container thats already running shall not do anything', function() {

            var containerLists = 0;


            app.docker.listContainers = function(optsf) {
               containerLists++;
               // console.inspect(optsf)
               return Promise.resolve(
                  [ { Id: amavisd_milter_container_id,
                      Names: [ '/adoring_leavitt' ],
                      Image: 'stretch-amavisd-milter',
                      ImageID: 'sha256:3fd9065eaf02feaf94d68376da52541925650b81698c53c6824d92ff63f98353',
                      Command: '/bin/sh',
                      Created: 1529237902,
                      Ports: [],
                      Labels: {
                      },
                      State: 'running',
                      Status: 'Up 31 seconds',
                      HostConfig: { NetworkMode: 'default' },
                      NetworkSettings: { Networks: [{foo:"bar"}] },
                      Mounts: [] } ]
                )
               return Promise.resolve([]);
            }


            return mapi.postAsync("/root/system/stretch-amavisd-milter/start", {})
              .then(re => {
                 // note: the docker response is filtered, some of the fields are stripped
                 assert.deepEqual(re.result, {container:amavisd_milter_container_id});
                 assert.equal(containerLists, 1);
                 app.docker.listContainers = oCoLi;
              })

        })
  });


	describe('basic global stuff', function() {

        it("ssh keyline validator", function(){
            var q = app.isAcceptableAsSshKeyLines([
              goodSshKeyLine
            ])
            assert.equal(q, true);
        })

        it('listing all containers', function() {

            const dockerResult = [{
                  foo: "bar"
               }];

            app.docker.listContainers = function(){
               return Promise.resolve(dockerResult)
            }

            return mapi.postAsync("/root/containers/list", {})
              .then(re => {
                 assert.deepEqual(re.result, dockerResult);
                 app.docker.listContainers = oCoLi;
              })

        })


    })


  describe('basic per webhosting stuff', function() {

        const webhosting_id = 10001;
        var oMonsterInfo;
        var oCoLi;
        function setupMonsterInfo(){
            oMonsterInfo = app.MonsterInfoWebhosting;
            app.MonsterInfoWebhosting = {
                GetInfo: function(storageId){
                   assert.equal(storageId, webhosting_id);
                   return Promise.resolve({wh_id: webhosting_id, 
                    wh_user_id: c_user_id,
                    wh_max_execution_second: 300,
                    wh_php_fpm_conf_extra_lines: "some-line\n",
                    extras: {
                      web_path: "/web/w3/12345-abvashgdasda",
                    },
                    template: {
                      t_max_container_apps:2,
                      t_container_cmd_timeout_s:60,
                      t_allowed_dynamic_languages: ["php", "ssh", "webshell"],
                   }})
                }
            }

        }

        function setupListContainers(){
             app.docker.listContainers = function(optsf){
               // console.inspect(optsf)
               assert.deepEqual(optsf, { 
                  all: true,
                  filters: { label: [ 'com.monster-cloud.webstore='+webhosting_id,
                    'com.monster-cloud.mc-app',
                  ] } 
               });
               return Promise.resolve(
                  [ { Id: '73b2040f3d2da845c6f1f285820f9753d16378cc00aac02a729069ef162782a9',
                      Names: [ '/adoring_leavitt' ],
                      Image: 'alpine',
                      ImageID: 'sha256:3fd9065eaf02feaf94d68376da52541925650b81698c53c6824d92ff63f98353',
                      Command: '/bin/sh',
                      Created: 1529237902,
                      Ports: [],
                      Labels: {
                         'com.monster-cloud.webstore': ""+webhosting_id,
                         'com.monster-cloud.user': c_user_id,
                      },
                      State: 'running',
                      Status: 'Up 31 seconds',
                      HostConfig: { NetworkMode: 'default' },
                      NetworkSettings: { Networks: [{foo:"bar"}] },
                      Mounts: [] } ]
                )
            }
        }

        it('listing all webapp containers that belong to a webstore', function() {

            setupMonsterInfo();

             app.docker.listContainers = function(optsf){
               // console.inspect(optsf)
               assert.deepEqual(optsf, { 
                  all: true,
                  filters: { label: [ 
                    'com.monster-cloud.webstore='+webhosting_id,
                    'com.monster-cloud.mc-webapp',
                    'com.monster-cloud.mc-app',
                   ] } 
               });
               return Promise.resolve([])
            }


            return mapi.postAsync("/webhosting/"+webhosting_id+"/containers/list", {webapp: true})
              .then(re => {
                 app.docker.listContainers = oCoLi;

                 // note: the docker response is filtered, some of the fields are stripped
                 assert.deepEqual(re.result, []);
              })

        })

        it('listing all containers that belong to a webstore', function() {

            setupListContainers();

            return mapi.postAsync("/webhosting/"+webhosting_id+"/containers/list", {})
              .then(re => {
                 // note: the docker response is filtered, some of the fields are stripped
                 assert.deepEqual(re.result, [{
                      Id: '73b2040f3d2da845c6f1f285820f9753d16378cc00aac02a729069ef162782a9',
                      Names: [ '/adoring_leavitt' ],
                      PrimaryName: "adoring_leavitt",
                      Image: 'alpine',
                      Command: '/bin/sh',
                      Labels: {
                         'com.monster-cloud.webstore': ""+webhosting_id,
                         'com.monster-cloud.user': c_user_id,
                      },
                      State: 'running',
                      Status: 'Up 31 seconds',
                      c_user_id: c_user_id,
                      c_webhosting: ""+webhosting_id,

                 }]);
              })

        })

        it('listing available images (with unexpected labels, the result shall be empty)', function() {

            app.docker.listImages = function(optsf) {
               // console.inspect(optsf)
               assert.deepEqual(optsf, { filters: { label: [ 'com.monster-cloud.mc-app' ] } });
               return Promise.resolve([{ Containers: -1,
    Created: 1529237078,
    Id: 'sha256:e189adde95ba093f3a41e918c4734ae6a0cf04f9be962a8ee4d85dc6bfa61543',
    Labels: { foo1: 'bar2', foo2: 'bar2' },
    ParentId: 'sha256:3fd9065eaf02feaf94d68376da52541925650b81698c53c6824d92ff63f98353',
    RepoDigests: null,
    RepoTags: [ 'test-image:latest' ],
    SharedSize: -1,
    Size: 4147781,
    VirtualSize: 4147781 },

     ]);
            }

            return mapi.postAsync("/webhosting/"+webhosting_id+"/images/available", {})
              .then(re => {
                 // note: the docker response is filtered, some of the fields are stripped
                 assert.deepEqual(re.result, {images:[], canCreate: true});

                 app.docker.listImages = oImages;
              })

        })

        it('listing available images (the mc-app labeled ones shall be returned)', function() {

            app.docker.listImages = function(optsf) {
               // console.inspect(optsf)
               assert.deepEqual(optsf, { filters: { label: [ 'com.monster-cloud.mc-app' ] } });
               return Promise.resolve([{ Containers: -1,
    Created: 1529237078,
    Id: 'sha256:e189adde95ba093f3a41e918c4734ae6a0cf04f9be962a8ee4d85dc6bfa61543',
    Labels: { "com.monster-cloud.mc-app": 'php', 'com.monster-cloud.mc-params': '{"foo":"bar"}' },
    ParentId: 'sha256:3fd9065eaf02feaf94d68376da52541925650b81698c53c6824d92ff63f98353',
    RepoDigests: null,
    RepoTags: [ 'php-5.6:5.6.31-2' ],
    SharedSize: -1,
    Size: 4147781,
    VirtualSize: 4147781 } ,
{ Containers: -1,
    Created: 1529237078,
    Id: 'sha256:e189adde95ba093f3a41e918c4734ae6a0cf04f9be962a8ee4d85dc6bfa61543',
    Labels: { "com.monster-cloud.mc-app": 'java' },
    ParentId: 'sha256:3fd9065eaf02feaf94d68376da52541925650b81698c53c6824d92ff63f98353',
    RepoDigests: null,
    RepoTags: [ 'java:1.8.0.182-3' ],
    SharedSize: -1,
    Size: 4147781,
    VirtualSize: 4147781 } ,

{ Containers: -1,
    Created: 1529237078,
    Id: 'sha256:e189adde95ba093f3a41e918c4734ae6a0cf04f9be962a8ee4d85dc6bfa61543',
    Labels: { "com.monster-cloud.mc-app": 'php', 'com.monster-cloud.mc-params': '{"foo":"bar"}' },
    ParentId: 'sha256:3fd9065eaf02feaf94d68376da52541925650b81698c53c6824d92ff63f98353',
    RepoDigests: null,
    RepoTags: [ 'stretch-php-base:2018-03-01-1' ],
    SharedSize: -1,
    Size: 4147781,
    VirtualSize: 4147781 } ,

    ]);
            }

            return mapi.postAsync("/webhosting/"+webhosting_id+"/images/available", {})
              .then(re => {
                 // note: the docker response is filtered, some of the fields are stripped
                 assert.deepEqual(re.result, {images:[
{   Id: 'sha256:e189adde95ba093f3a41e918c4734ae6a0cf04f9be962a8ee4d85dc6bfa61543',
    Labels: { "com.monster-cloud.mc-app": 'php', 'com.monster-cloud.mc-params': {"foo":"bar"} },
    RepoTags: [ 'php-5.6:5.6.31-2' ],
    PrimaryTag: "php-5.6:5.6.31-2",
    VirtualSize: 4147781 } 
                  ], canCreate: true});

                 app.docker.listImages = oImages;
              })

        })


        it('listing the latest available version of images', function() {

            app.docker.listImages = function(optsf) {
               // console.inspect(optsf)
               assert.deepEqual(optsf, { filters: { label: [ 'com.monster-cloud.mc-app' ] } });
               return Promise.resolve([{ Containers: -1,
    Created: 1529237078,
    Id: 'sha256:e189adde95ba093f3a41e918c4734ae6a0cf04f9be962a8ee4d85dc6bfa61543',
    Labels: { "com.monster-cloud.mc-app": 'php', 'com.monster-cloud.mc-params': '{"foo":"bar"}' },
    ParentId: 'sha256:3fd9065eaf02feaf94d68376da52541925650b81698c53c6824d92ff63f98353',
    RepoDigests: null,
    RepoTags: [ 'php-5.6:5.6.31-2' ],
    SharedSize: -1,
    Size: 4147781,
    VirtualSize: 4147781 } ,
{ Containers: -1,
    Created: 1529237078,
    Id: 'sha256:e189adde95ba093f3a41e918c4734ae6a0cf04f9be962a8ee4d85dc6bfa61543',
    Labels: { "com.monster-cloud.mc-app": 'java' },
    ParentId: 'sha256:3fd9065eaf02feaf94d68376da52541925650b81698c53c6824d92ff63f98353',
    RepoDigests: null,
    RepoTags: [ 'java:1.8.0.182-3' ],
    SharedSize: -1,
    Size: 4147781,
    VirtualSize: 4147781 } ,
{ Containers: -1,
    Created: 1529237078,
    Id: 'sha256:e189adde95ba093f3a41e918c4734ae6a0cf04f9be962a8ee4d85dc6bfa61543',
    Labels: { "com.monster-cloud.mc-app": 'php', 'com.monster-cloud.mc-params': '{"foo":"bar"}' },
    ParentId: 'sha256:3fd9065eaf02feaf94d68376da52541925650b81698c53c6824d92ff63f98353',
    RepoDigests: null,
    RepoTags: [ 'php-5.6:5.6.31-1' ],
    SharedSize: -1,
    Size: 4147781,
    VirtualSize: 4147781 } ,

    ]);
            }

            return mapi.postAsync("/root/images/latest", {})
              .then(re => {
                 // note: the docker response is filtered, some of the fields are stripped
                 assert.deepEqual(re.result, {"php-5.6":"5.6.31-2", "java":"1.8.0.182-3"});

                 app.docker.listImages = oImages;
              })

        })


        it('trying to create a new container with an image there is no permission', function() {

            app.docker.listImages = function(optsf) {
               // console.inspect(optsf)
               assert.deepEqual(optsf, { filters: { label: [ 'com.monster-cloud.mc-app' ] } });
               return Promise.resolve([{ Containers: -1,
    Created: 1529237078,
    Id: 'sha256:e189adde95ba093f3a41e918c4734ae6a0cf04f9be962a8ee4d85dc6bfa61543',
    Labels: { "com.monster-cloud.mc-app": 'unauthorized' },
    ParentId: 'sha256:3fd9065eaf02feaf94d68376da52541925650b81698c53c6824d92ff63f98353',
    RepoDigests: null,
    RepoTags: [ 'alpine-3.7-dropbear:2018-06-28--1' ],
    SharedSize: -1,
    Size: 4147781,
    VirtualSize: 4147781 } 
    ]);
            }

            return mapi.postAsync("/webhosting/"+webhosting_id+"/containers/new", {
               image: "alpine-3.7-dropbear:2018-06-28--1",
               start: true,
            })
            .then(()=>{
               throw new Error("Should have failed");
            })
            .catch(ex=>{
               assert.propertyVal(ex, "message", "VALIDATION_ERROR")
               assert.ok(ex.errorParameters);
               assert.deepEqual(ex.errorParameters.image, [ 'unsupported image' ])
               app.docker.listImages = oImages;
            })

        })


        it('trying to create a new dropbear container with invalid params (no type)', function() {

            app.docker.listImages = function(optsf) {
               // console.inspect(optsf)
               assert.deepEqual(optsf, { filters: { label: [ 'com.monster-cloud.mc-app' ] } });
               return Promise.resolve([{ Containers: -1,
    Created: 1529237078,
    Id: 'sha256:e189adde95ba093f3a41e918c4734ae6a0cf04f9be962a8ee4d85dc6bfa61543',
    Labels: { "com.monster-cloud.mc-app": 'ssh' },
    ParentId: 'sha256:3fd9065eaf02feaf94d68376da52541925650b81698c53c6824d92ff63f98353',
    RepoDigests: null,
    RepoTags: [ 'alpine-3.7-dropbear:2018-06-28--1' ],
    SharedSize: -1,
    Size: 4147781,
    VirtualSize: 4147781 } 
    ]);
            }


            return mapi.postAsync("/webhosting/"+webhosting_id+"/containers/new", {
               image: "alpine-3.7-dropbear:2018-06-28--1",
               start: true,
            })
            .then(()=>{
               throw new Error("should have failed");
            })
            .catch(ex=>{
               assert.propertyVal(ex, "message", "VALIDATION_ERROR");
            })

        })


        createDropBearContainer();


        it('webstore config shall be correct', function() {
            return mapi.getAsync("/root/webstores/config")
              .then(re=>{
                  console.inspect(re.result);
                  assert.ok(re.result.config);
                  assert.ok(re.result.config[webhosting_id]);
                  assert.ok(re.result.config[webhosting_id]["new_container_id"]);
                  assert.ok(re.result.config[webhosting_id]["new_container_id"].createdAs);
                  assert.equal(re.result.config[webhosting_id]["new_container_id"].createdAs.publicKeys, goodSshKeyLine);
                  assert.ok(re.result.config[webhosting_id]["new_container_id"].hostPort, app.config.get("open_ports_min"));
              })

        })



        it('saving global ssh key', function() {

            return mapi.postAsync("/webhosting/"+webhosting_id+"/ssh/authz", {authz: goodSshKeyLine})
              .then(re => {
                 assert.equal(re.result, "ok");
                 var c = fs.readFileSync("tmp/home/"+webhosting_id+"/.ssh/authorized_keys", "utf8");
                 assert.equal(goodSshKeyLine.trim(), c.trim());
              })

        })

        it('trying to save container id belonging to somewhere else', function() {

            return mapi.postAsync("/webhosting/"+webhosting_id+"/ssh/target", {targetContainer: "foo"})              
              .then(re => {
                 throw new Error("should have fialed");
              })
              .catch(ex=>{
                  assert.equal(ex.message, "PERMISSION_DENIED")
              })

        })

        it('saving target container', function() {

            return mapi.postAsync("/webhosting/"+webhosting_id+"/ssh/target", {targetContainer: "new_container_id"})
              .then(re => {
                 assert.equal(re.result, "ok");
                 var c = fs.readFileSync("tmp/etc/passwd", "utf8");
                 assert.equal("10001:x:10001:999::/host/home/10001:/bin/dockersh\n", c);
              })

        })

        it('reading back ssh stuff', function() {

            return mapi.getAsync("/webhosting/"+webhosting_id+"/ssh/")
              .then(re => {
                 assert.deepEqual(re.result, {targetContainer: "new_container_id", authz: goodSshKeyLine});
              })

        })


        it('some ssh cleanup', function() {

            return mapi.postAsync("/webhosting/"+webhosting_id+"/ssh/cleanup", {})
              .then(re => {
                 assert.equal(re.result, "ok");
                 fs.readFileSync("tmp/etc/passwd", "utf8");
                 var c = fs.readFileSync("tmp/etc/passwd", "utf8");
                 assert.equal("", c);

                 try {
                    fs.statSync("tmp/home/10001");                  
                    throw new Error("dir should have beenc leaned")
                 }catch(x) {
                    assert.equal(x.code, "ENOENT");
                 }
              })

        })


        it('deleting a running container shall be refused', function() {

             app.docker.listContainers = function(optsf){
               // console.inspect(optsf)
               assert.deepEqual(optsf, { 
                  all: true,
                  filters: { label: [ 
                    'com.monster-cloud.webstore='+webhosting_id,
                    'com.monster-cloud.mc-app'
                  ] } 
               });
               return Promise.resolve(
                  [ { Id: 'new_container_id',
                      Names: [ '/adoring_leavitt' ],
                      Image: 'alpine',
                      ImageID: 'sha256:3fd9065eaf02feaf94d68376da52541925650b81698c53c6824d92ff63f98353',
                      Command: '/bin/sh',
                      Created: 1529237902,
                      Ports: [],
                      Labels: {},
                      State: 'running',
                      Status: 'Up 31 seconds',
                      HostConfig: { NetworkMode: 'default' },
                      NetworkSettings: { Networks: [{foo:"bar"}] },
                      Mounts: [] } ]
                )
            }

            return mapi.postAsync("/webhosting/"+webhosting_id+"/container/new_container_id/remove", {})
              .then(() => {
                  throw new Error("should have failed")
              })
              .catch(ex=>{
                  assert.propertyVal(ex, "message", "NOT_STOPPED");
              })

        })

        it('stopping a running container', function() {

             const containerInfo = { Id: 'new_container_id',
                      Names: [ '/adoring_leavitt' ],
                      Image: 'alpine',
                      ImageID: 'sha256:3fd9065eaf02feaf94d68376da52541925650b81698c53c6824d92ff63f98353',
                      Command: '/bin/sh',
                      Created: 1529237902,
                      Ports: [],
                      Labels: {},
                      State: 'running',
                      Status: 'Up 31 seconds',
                      HostConfig: { NetworkMode: 'default' },
                      NetworkSettings: { 
                        Networks: [{foo:"bar"}] ,
                        IPAddress: "123.123.123.123",
                      },
                      Mounts: [] }


             app.docker.listContainers = function(optsf){
               // console.inspect(optsf)
               assert.deepEqual(optsf, { 
                  all: true,
                  filters: { label: [ 
                    'com.monster-cloud.webstore='+webhosting_id,
                    'com.monster-cloud.mc-app', 
                  ] } 
               });
               return Promise.resolve(
                  [ containerInfo ]
                )
            }

            var oGetContainer = app.docker.getContainer;
            app.docker.getContainer = function(id){
               assert.equal(id, "new_container_id");
               return {
                 inspect: function(){
                  console.log("inspect called")
                    return Promise.resolve({ Id: 'new_container_id',
                      Config: containerInfo,
                      Names: [ '/adoring_leavitt' ],
                      Image: 'alpine',
                      ImageID: 'sha256:3fd9065eaf02feaf94d68376da52541925650b81698c53c6824d92ff63f98353',
                      Command: '/bin/sh',
                      Created: 1529237902,
                      Ports: [],
                      Labels: {},
                      State: 'running',
                      Status: 'Up 31 seconds',
                      HostConfig: { NetworkMode: 'default' },
                      NetworkSettings: { 
                        IPAddress: "123.123.123.123",
                        Networks: [{foo:"bar"}] 
                      },
                      Mounts: [] })
                 },
                 stop: function(){
                    return Promise.resolve();
                 }
               }
            }

            return mapi.postAsync("/webhosting/"+webhosting_id+"/container/new_container_id/stop", {})
              .then((r) => {
                  assert.deepEqual(r.result, "ok");
                  app.docker.getContainer = oGetContainer;

              })

        })


        deleteContainer("new_container_id");


        it('creating a new dropbear container in attach mode with invalid target container', function() {

            setupListContainers();

            app.docker.listImages = function(optsf) {
               // console.inspect(optsf)
               assert.deepEqual(optsf, { filters: { label: [ 'com.monster-cloud.mc-app' ] } });
               return Promise.resolve([{ Containers: -1,
    Created: 1529237078,
    Id: 'sha256:e189adde95ba093f3a41e918c4734ae6a0cf04f9be962a8ee4d85dc6bfa61543',
    Labels: { "com.monster-cloud.mc-app": 'ssh' },
    ParentId: 'sha256:3fd9065eaf02feaf94d68376da52541925650b81698c53c6824d92ff63f98353',
    RepoDigests: null,
    RepoTags: [ 'alpine-3.7-dropbear:2018-06-28--1' ],
    SharedSize: -1,
    Size: 4147781,
    VirtualSize: 4147781 } 
    ]);
            }



            return mapi.postAsync("/webhosting/"+webhosting_id+"/containers/new", {
               image: "alpine-3.7-dropbear:2018-06-28--1",
               type: "attach",
               container: "foobar",
               start: true,
            })
            .then(()=>{
               throw new Error("should have failed")
            })
            .catch(ex=>{
                assert.propertyVal(ex, "message", "PERMISSION_DENIED");
            })

        })



        it('creating a new dropbear container in attach mode', function() {

            var targetContainer = "73b2040f3d2da845c6f1f285820f9753d16378cc00aac02a729069ef162782a9";

            app.docker.listImages = function(optsf) {
               // console.inspect(optsf)
               assert.deepEqual(optsf, { filters: { label: [ 'com.monster-cloud.mc-app' ] } });
               return Promise.resolve([{ Containers: -1,
    Created: 1529237078,
    Id: 'sha256:e189adde95ba093f3a41e918c4734ae6a0cf04f9be962a8ee4d85dc6bfa61543',
    Labels: { "com.monster-cloud.mc-app": 'ssh' },
    ParentId: 'sha256:3fd9065eaf02feaf94d68376da52541925650b81698c53c6824d92ff63f98353',
    RepoDigests: null,
    RepoTags: [ 'alpine-3.7-dropbear:2018-06-28--1' ],
    SharedSize: -1,
    Size: 4147781,
    VirtualSize: 4147781 } 
    ]);
            }


            var oCommander = app.commander.spawn;
            app.commander.spawn = function(p1, p2){
               // console.inspect(p1, "XXX", p2);
               assert.deepEqual(p1, { 
  executeImmediately: true,
  omitControlMessages: true,
  removeImmediately: true,
  chain:
   { executable: '[socat_path]',
     args:
      [ 'UNIX-LISTEN:[unix_listen_socket],fork,user=[user]',
        'exec:docker exec -it [target_container_id] /bin/sh,pty,setsid,setpgid,stderr,ctty,sane' ] } })

               assert.ok(p2[0].unix_listen_socket.indexOf("docker-attach.sock") > 0)
               delete p2[0].unix_listen_socket;
               
               assert.deepEqual(p2, [ { user: ""+webhosting_id, socat_path: '/usr/bin/socat',
    target_container_id: targetContainer 
               } ])
               return Promise.resolve({
                  executionPromise: Promise.resolve()
               });
            }



            var labels;
            var containerInspected = 0;
            var containerCreated = 0;
            var containerStarted = 0;
            var oCreate = app.docker.createContainer;
            app.docker.createContainer = function(aoptsf){
               // console.inspect(aoptsf, 123)
               containerCreated++;

               var optsf = simpleCloneObject(aoptsf);

               var hostDir = optsf.HostConfig.Binds[0];
               var hostIndex = hostDir.indexOf(":/host");
               assert.ok(hostIndex > -1);
               optsf.HostConfig.Binds[0] = "/hosto:/host";
               hostDir = hostDir.substr(0, hostIndex);

               var pwdFile = optsf.HostConfig.Binds[1];
               var passwdIndex = pwdFile.indexOf(":/etc/passwd");
               assert.ok(passwdIndex > -1);
               pwdFile = pwdFile.substr(0, passwdIndex);
               optsf.HostConfig.Binds[1] = "/passwdo:/etc/passwd";

               assert.ok(optsf.Labels[app.mcContainerNameLabel]);
               delete optsf.Labels[app.mcContainerNameLabel];

               var pwd = fs.readFileSync(pwdFile);
               assert.ok(pwd.indexOf(webhosting_id+":") == 0);

               assert.deepEqual(
                  optsf,
                  { AttachStdin: false,
  AttachStdout: false,
  AttachStderr: false,
  Tty: false,
  OpenStdin: false,
  StdinOnce: false,
  Labels: { 
     'com.monster-cloud.shellType': 'attach',
     'com.monster-cloud.mc-app': 'ssh',
     'com.monster-cloud.webstore': ""+webhosting_id,
     'com.monster-cloud.user': c_user_id,
     'com.monster-cloud.targetContainer': targetContainer,
     'com.monster-cloud.hostPort': "50001",
  },
  ExposedPorts: {},
  HostConfig:
   { 
     Dns: ["1.2.3.4"],
     DnsSearch: ["."],     
     NetworkMode: "host",
     ExtraHosts:
      [ 'server-name:9.8.7.6',
        'server-name.fooo.hu:9.8.7.6',
        'extra.foobar.hu:9.8.7.6' ],
     LogConfig: { "Type": "json-file", "Config": {"max-size": "3m", "max-file": "2"} },
     // Tmpfs: { '/tmp': 'rw,noexec,nosuid,size=65536k' },
     ShmSize: 4194304,
     Memory: 268435456,
     MemorySwap: 268435456,
     Ulimits: [ { Name: 'nproc', Soft: 64, Hard: 64 } ] ,
     Mounts: [],
     Binds: [
       "/hosto:/host",
       "/passwdo:/etc/passwd",
       "/opt/MonsterDocker/lib/misc/cpuinfo.txt:/proc/cpuinfo:ro",
       "/web/w3/12345-abvashgdasda:/web"
     ],
     PortBindings: {},
     RestartPolicy: { Name: 'unless-stopped' },
  },
  User: webhosting_id+':1002',
  Env: [],
  Cmd: ["50001"],
  Image: 'alpine-3.7-dropbear:2018-06-28--1' }

               );

               labels = optsf.Labels;

               return Promise.resolve({
                  id: "new_container_id2",
                  inspect: function(){
                     containerInspected++;
                     return Promise.resolve({
                        Id: "new_container_id2",
                        State: {
                           Status: "running",
                        },
                        Config: {
                           Labels: labels,
                        }
                     });
                  },
                  start: function(){
                     containerStarted++;
                     return Promise.resolve();
                  }
               }); // note the lowercased id here
            }

            return mapi.postAsync("/webhosting/"+webhosting_id+"/containers/new", {
               image: "alpine-3.7-dropbear:2018-06-28--1",
               type: "attach",
               container: targetContainer,
               publicKeys: goodSshKeyLine,
               start: true,
            })
              .then(re => {
                 // note: the docker response is filtered, some of the fields are stripped
                 assert.deepEqual(re.result, {hostPort: 50001, ssh:{StatusCode:0, stdout:"", stderr:""},container:"new_container_id2"});
                 assert.equal(containerCreated, 1);
                 assert.equal(containerStarted, 1);
                 // assert.equal(containerInspected, 3);

                 app.docker.listImages = oImages;
                 app.docker.createContainer = oCreate;
                 app.commander.spawn = oCommander;
              })

        })

        deleteContainer("new_container_id2");



        it('creating a new shellinabox container in simple mode', function() {

            app.docker.listImages = function(optsf) {
               // console.inspect(optsf)
               assert.deepEqual(optsf, { filters: { label: [ 'com.monster-cloud.mc-app' ] } });
               return Promise.resolve([{ Containers: -1,
    Created: 1529237078,
    Id: 'sha256:e189adde95ba093f3a41e918c4734ae6a0cf04f9be962a8ee4d85dc6bfa61543',
    Labels: { "com.monster-cloud.mc-app": 'webshell' },
    ParentId: 'sha256:3fd9065eaf02feaf94d68376da52541925650b81698c53c6824d92ff63f98353',
    RepoDigests: null,
    RepoTags: [ 'alpine-3.7-shellinabox:2018-06-28--1' ],
    SharedSize: -1,
    Size: 4147781,
    VirtualSize: 4147781 } 
    ]);
            }

            var labels;
            var containerCreated = 0;
            var containerStarted = 0;
            var oCreate = app.docker.createContainer;
            app.docker.createContainer = function(aoptsf){
               

               var optsf = simpleCloneObject(aoptsf);

               assert.ok(optsf.Env[0].match(new RegExp('SERVICE_URL=/docker/shellinabox/.+')));
               optsf.Env[0] = "SERVICE_URL=foobar";

               var pwdFile = optsf.HostConfig.Binds[0];
               console.log("passwd line", pwdFile);
               var passwdIndex = pwdFile.indexOf(":/etc/passwd");
               assert.ok(passwdIndex > -1);
               pwdFile = pwdFile.substr(0, passwdIndex);
               optsf.HostConfig.Binds[0] = "/passwdo:/etc/passwd";


                console.inspect(optsf, 123)

               containerCreated++;

               assert.ok(optsf.HostConfig.Mounts[0].Source);
               delete optsf.HostConfig.Mounts[0].Source;

               assert.ok(optsf.Labels[app.mcContainerNameLabel]);
               delete optsf.Labels[app.mcContainerNameLabel];

               assert.deepEqual(
                  optsf,
                  { AttachStdin: false,
  AttachStdout: false,
  AttachStderr: false,
  Tty: false,
  OpenStdin: false,
  StdinOnce: false,
  Labels: { 
    'com.monster-cloud.shellType': 'simple',
    "com.monster-cloud.mc-app": 'webshell', // this label was inherited from the image
    'com.monster-cloud.webstore': ""+webhosting_id , // this one was added to indicate the container it belongs to
    'com.monster-cloud.user': c_user_id,
  },
  ExposedPorts: {},
  HostConfig:
   { 
     Dns: ["1.2.3.4"],
     DnsSearch: ["."],
     NetworkMode: "host",
     ExtraHosts:
      [ 'server-name:9.8.7.6',
        'server-name.fooo.hu:9.8.7.6',
        'extra.foobar.hu:9.8.7.6' ],
     LogConfig: { "Type": "json-file", "Config": {"max-size": "3m", "max-file": "2"} },
     // Tmpfs: { '/tmp': 'rw,noexec,nosuid,size=65536k' },
     ShmSize: 4194304,
     Memory: 268435456,
     MemorySwap: 268435456,
     Ulimits: [ { Name: 'nproc', Soft: 64, Hard: 64 } ],
     Binds: [
       '/passwdo:/etc/passwd',
       '/opt/MonsterDocker/lib/misc/cpuinfo.txt:/proc/cpuinfo:ro',
       '/web/w3/12345-abvashgdasda:/web'  
     ],
     Mounts: [ 
       {
          Type: 'bind',
          Target: '/var/run/mc-app-socket' 
       } 
     ],
     PortBindings: { },
     RestartPolicy: { Name: 'no', MaximumRetryCount: 0 },
  },
  User: webhosting_id+':1002',
  Cmd: [],
  Env:
   [ 'SERVICE_URL=foobar' ],
  Image: 'alpine-3.7-shellinabox:2018-06-28--1' }

               );

               labels = optsf.Labels;

               return Promise.resolve({
                  id: "new_container_id3",
                  inspect: function(){
                      return Promise.resolve({
                         Id: "new_container_id3",
                         State: {
                           Status: "running",
                         },
                         Config: {
                            Labels: labels,
                         }
                      })
                  },
                  start: function(){
                     containerStarted++;
                     return Promise.resolve();
                  }
               }); // note the lowercased id here
            }

            return mapi.postAsync("/webhosting/"+webhosting_id+"/containers/new", {
               image: "alpine-3.7-shellinabox:2018-06-28--1",
               type: "simple",
               start: true,
            })
              .then(re => {
                 // note: the docker response is filtered, some of the fields are stripped
                 assert.deepEqual(re.result, {container:"new_container_id3"});
                 assert.equal(containerCreated, 1);
                 assert.equal(containerStarted, 1);

                 app.docker.listImages = oImages;
                 app.docker.createContainer = oCreate;
              })

        })



        it('listing all containers', function() {

             app.docker.listContainers = function(optsf){
               // console.inspect(optsf)
               assert.deepEqual(optsf, { 
                  all: true,
                  filters: { label: [ 
                      'com.monster-cloud.webstore='+webhosting_id,
                      'com.monster-cloud.mc-app', 
                  ] } 
               });
               return Promise.resolve(
                  [ { Id: 'new_container_id3',
                      Names: [ '/adoring_leavitt' ],
                      Image: 'alpine',
                      ImageID: 'sha256:3fd9065eaf02feaf94d68376da52541925650b81698c53c6824d92ff63f98353',
                      Command: '/bin/sh',
                      Created: 1529237902,
                      Ports: [],
                      Labels: {
                         "com.monster-cloud.mc-app": "webshell",
                         'com.monster-cloud.webstore': ""+webhosting_id,
                      },
                      State: 'running',
                      Status: 'Up 31 seconds',
                      HostConfig: { NetworkMode: 'default' },
                      NetworkSettings: { Networks: [{foo:"bar"}] },
                      Mounts: [] } ]
                )
            }

            return mapi.postAsync("/webhosting/"+webhosting_id+"/containers/list", {})
              .then(re => {
                 // note: the docker response is filtered, some of the fields are stripped
                 assert.ok(re.result[0].shutdownAt);
                 delete re.result[0].shutdownAt;
                 assert.ok(re.result[0].webshellSecret);
                 delete re.result[0].webshellSecret;

                 assert.deepEqual(re.result, [{
                      Id: 'new_container_id3',
                      Names: [ '/adoring_leavitt' ],
                      PrimaryName: "adoring_leavitt",
                      Image: 'alpine',
                      Command: '/bin/sh',
                      Labels: {
                         "com.monster-cloud.mc-app": "webshell",
                         'com.monster-cloud.webstore': ""+webhosting_id,
                      },
                      State: 'running',
                      Status: 'Up 31 seconds',
                      c_webhosting: ""+webhosting_id,
                 }]);
              })

        })


        deleteContainer("new_container_id3");


        createDropBearContainer("new_container_id4");

        it("trying to upgrade a dropbear container to an image that is not present", function(){
             app.docker.listContainers = function(optsf){
               // console.inspect(optsf)
               assert.deepEqual(optsf, { 
                  all: true,
                  filters: { label: [ 
                    'com.monster-cloud.webstore='+webhosting_id,
                    'com.monster-cloud.mc-app', 
                  ] } 
               });
               return Promise.resolve(
                  [ { Id: 'new_container_id4',
                      Names: [ '/adoring_leavitt' ],
                      Image: "alpine-3.7-dropbear:2018-06-28--1",
                      ImageID: 'sha256:3fd9065eaf02feaf94d68376da52541925650b81698c53c6824d92ff63f98353',
                      Command: '/bin/sh',
                      Created: 1529237902,
                      Ports: [],
                      Labels: {
                            'com.monster-cloud.webstore': ""+webhosting_id,
                            'com.monster-cloud.user': c_user_id,
                      },
                      State: 'running',
                      Status: 'Up 31 seconds',
                      HostConfig: { NetworkMode: 'default' },
                      NetworkSettings: { Networks: [{foo:"bar"}] },
                      Mounts: [] } ]
                )
            }

            return mapi.postAsync("/webhosting/"+webhosting_id+"/container/new_container_id4/upgrade", {destination_image: "alpine-3.7-dropbear:2018-06-28--2"})
              .then(() => {
                 throw new Error("Should have failed")
              })
              .catch(ex=>{
                  assert.propertyVal(ex, "message", "VALIDATION_ERROR")
              })
        });

        it("listing the compatbile images", function(){

            app.docker.listImages = function(optsf) {
               // console.inspect(optsf)
               assert.deepEqual(optsf, { filters: { label: [ 'com.monster-cloud.mc-app' ] } });
               return Promise.resolve([
{ Containers: -1,
    Created: 1529237078,
    Id: 'sha256:e189adde95ba093f3a41e918c4734ae6a0cf04f9be962a8ee4d85dc6bfa61543',
    Labels: { "com.monster-cloud.mc-app": 'ssh' },
    ParentId: 'sha256:3fd9065eaf02feaf94d68376da52541925650b81698c53c6824d92ff63f98353',
    RepoDigests: null,
    RepoTags: [ 'alpine-3.7-dropbear:2018-06-28--1' ],
    SharedSize: -1,
    Size: 4147781,
    VirtualSize: 4147781 },
{ Containers: -1,
    Created: 1529237078,
    Id: 'sha256:e189adde95ba093f3a41e918c4734ae6a0cf04f9be962a8ee4d85dc6bfa61543',
    Labels: { "com.monster-cloud.mc-app": 'ssh' },
    ParentId: 'sha256:3fd9065eaf02feaf94d68376da52541925650b81698c53c6824d92ff63f98353',
    RepoDigests: null,
    RepoTags: [ 'alpine-3.7-dropbear:2018-06-28--2' ],
    SharedSize: -1,
    Size: 4147781,
    VirtualSize: 4147781 },
                { Containers: -1,
    Created: 1529237078,
    Id: 'sha256:e189adde95ba093f3a41e918c4734ae6a0cf04f9be962a8ee4d85dc6bfa61543',
    Labels: { "com.monster-cloud.mc-app": 'webshell' },
    ParentId: 'sha256:3fd9065eaf02feaf94d68376da52541925650b81698c53c6824d92ff63f98353',
    RepoDigests: null,
    RepoTags: [ 'alpine-3.7-shellinabox:2018-06-28--1' ],
    SharedSize: -1,
    Size: 4147781,
    VirtualSize: 4147781 } 
    ]);
            }


            return mapi.getAsync("/webhosting/"+webhosting_id+"/container/new_container_id4/info")
              .then((re) => {
                 assert.deepEqual(re.result, 
                      {
                        "Command": "/bin/sh",
                        "Id": "new_container_id4",
                        "Image": "alpine-3.7-dropbear:2018-06-28--1",
                        "Labels": {
                            'com.monster-cloud.webstore': ""+webhosting_id,
                            'com.monster-cloud.user': c_user_id,
                        },
                        "Names": [
                          "/adoring_leavitt"
                        ],
                        "PrimaryName": "adoring_leavitt",
                        "State": "running",
                        "Status": "Up 31 seconds",
                        c_user_id: c_user_id,
                        c_webhosting: ""+webhosting_id,
                        "compatibleImages": [
                          "alpine-3.7-dropbear:2018-06-28--1",
                          "alpine-3.7-dropbear:2018-06-28--2",
                        ]
                      }
                 )
              })
        });


        it("trying to upgrade a dropbear container to a different (non-compatible) image", function(){

            return mapi.postAsync("/webhosting/"+webhosting_id+"/container/new_container_id4/upgrade", {destination_image: "alpine-3.7-shellinabox:2018-06-28--1"})
              .then(() => {
                 throw new Error("Should have failed")
              })
              .catch(ex=>{
                  assert.propertyVal(ex, "message", "VALIDATION_ERROR")
              })
        });

        it("upgrading a dropbear container", function(){

            const old_container_name = "new_container_id4";
            const new_container_name = "new_container_id5";

            var destination_image = "alpine-3.7-dropbear:2018-06-28--2"


            var labels;
            var containerInspected = 0;
            var containerCreated = 0;
            var containerStarted = 0;
            var containerStopped = 0;
            var containerRemoved = 0;

             var oGet = app.docker.getContainer;
             app.docker.getContainer = function(optsf){
               assert.equal(optsf, old_container_name);
               return {
                  inspect: function(){
                      containerInspected++;
                      return Promise.resolve({
                        Id: old_container_name,
                        Image: 'alpine',
                        State: {
                           Status: "running",
                        },
                        Config: {
                        }
                     });
                  },
                  stop: function(){
                      containerStopped++;
                      return Promise.resolve();
                  },
                  remove: function(){
                      containerRemoved++;
                      return Promise.resolve();                    
                  }
               };
            }

            var oCreate = app.docker.createContainer;
            app.docker.createContainer = function(aoptsf){
               // console.inspect(aoptsf, 123)
               containerCreated++;

               var optsf = simpleCloneObject(aoptsf);

               var hostDir = optsf.HostConfig.Binds[0];
               var hostIndex = hostDir.indexOf(":/host");
               assert.ok(hostIndex > -1);
               optsf.HostConfig.Binds[0] = "/hosto:/host";
               hostDir = hostDir.substr(0, hostIndex);

               var pwdFile = optsf.HostConfig.Binds[1];
               var passwdIndex = pwdFile.indexOf(":/etc/passwd");
               assert.ok(passwdIndex > -1);
               pwdFile = pwdFile.substr(0, passwdIndex);
               optsf.HostConfig.Binds[1] = "/passwdo:/etc/passwd";

               var pwd = fs.readFileSync(pwdFile);
               assert.ok(pwd.indexOf(webhosting_id+":") == 0);

               assert.ok(optsf.Labels[app.mcContainerNameLabel]);
               delete optsf.Labels[app.mcContainerNameLabel];

               assert.deepEqual(
                  optsf,
                  { AttachStdin: false,
  AttachStdout: false,
  AttachStderr: false,
  Tty: false,
  OpenStdin: false,
  StdinOnce: false,
  Labels: { 
     'com.monster-cloud.shellType': 'simple',
     'com.monster-cloud.mc-app': 'ssh',
     'com.monster-cloud.webstore': ""+webhosting_id,
     'com.monster-cloud.user': c_user_id,
     'com.monster-cloud.hostPort': "50001",
  },
  ExposedPorts: {},
  HostConfig:
   { 
     Dns: ["1.2.3.4"],
     DnsSearch: ["."],
     NetworkMode: "host",
     ExtraHosts:
      [ 'server-name:9.8.7.6',
        'server-name.fooo.hu:9.8.7.6',
        'extra.foobar.hu:9.8.7.6' ],
     LogConfig: { "Type": "json-file", "Config": {"max-size": "3m", "max-file": "2"} },
     // Tmpfs: { '/tmp': 'rw,noexec,nosuid,size=65536k' },
     ShmSize: 4194304,
     Memory: 268435456,
     MemorySwap: 268435456,
     Ulimits: [ { Name: 'nproc', Soft: 64, Hard: 64 } ] ,
     Binds: [
       "/hosto:/host",
       "/passwdo:/etc/passwd",
       "/opt/MonsterDocker/lib/misc/cpuinfo.txt:/proc/cpuinfo:ro",
       "/web/w3/12345-abvashgdasda:/web"
     ],
     Mounts: [],
     PortBindings: { },
     RestartPolicy: { Name: 'unless-stopped' },
  },
  User: webhosting_id+':1002',
  Env: [],
  Cmd: ["50001"],
  Image: destination_image }

               );

               labels = optsf.Labels;

               return Promise.resolve({
                  id: new_container_name,
                  inspect: function(){
                     containerInspected++;
                     return Promise.resolve({
                        Id: new_container_name,
                        Names: ["/upgraded_container_name"],
                        State: {
                           Status: "running",
                        },
                        Config: {
                           Labels: labels,
                        }
                     });
                  },
                  start: function(){
                     containerStarted++;
                     return Promise.resolve();
                  }
               }); // note the lowercased id here
            }


            return mapi.postAsync("/webhosting/"+webhosting_id+"/container/new_container_id4/upgrade", {destination_image: destination_image, removeOldContainerImmediately: true})
              .then(re => {
                 // note: the docker response is filtered, some of the fields are stripped
                 assert.deepEqual(re.result, {
                    hostPort: 50001,
                    container:new_container_name
                 });
                 assert.equal(containerCreated, 1);
                 assert.equal(containerStarted, 1);
                 assert.equal(containerStopped, 2);
                 assert.equal(containerRemoved, 1);
                 // assert.equal(containerInspected, 4);

                 app.docker.createContainer = oCreate;
                 app.docker.getContainer = oGet;

              })
        })

        deleteContainer("new_container_id5");


        // starting a new container without tag (just image name), shall be latest available
        createDropBearContainer("new_container_id6", "alpine-3.7-dropbear");
        deleteContainer("new_container_id6");



        it("listing images that could be purged", function(){

            app.docker.listImages = function(optsf) {
               // console.inspect(optsf)
               assert.deepEqual(optsf, { filters: { label: [ 'com.monster-cloud.mc-app' ] } });
               return Promise.resolve([
{ Containers: -1,
    Created: 1529237078,
    Id: 'sha256:e189adde95ba093f3a41e918c4734ae6a0cf04f9be962a8ee4d85dc6bfa61543',
    Labels: { "com.monster-cloud.mc-app": 'ssh' },
    ParentId: 'sha256:3fd9065eaf02feaf94d68376da52541925650b81698c53c6824d92ff63f98353',
    RepoDigests: null,
    RepoTags: [ 'alpine-3.7-dropbear:2018-06-28--1' ],
    SharedSize: -1,
    Size: 4147781,
    VirtualSize: 4147781 },
{ Containers: -1,
    Created: 1529237078,
    Id: 'sha256:e189adde95ba093f3a41e918c4734ae6a0cf04f9be962a8ee4d85dc6bfa61543',
    Labels: { "com.monster-cloud.mc-app": 'ssh' },
    ParentId: 'sha256:3fd9065eaf02feaf94d68376da52541925650b81698c53c6824d92ff63f98353',
    RepoDigests: null,
    RepoTags: [ 'alpine-3.7-dropbear:2018-06-28--2' ],
    SharedSize: -1,
    Size: 4147781,
    VirtualSize: 4147781 },
                { Containers: -1,
    Created: 1529237078,
    Id: 'sha256:e189adde95ba093f3a41e918c4734ae6a0cf04f9be962a8ee4d85dc6bfa61543',
    Labels: { "com.monster-cloud.mc-app": 'webshell' },
    ParentId: 'sha256:3fd9065eaf02feaf94d68376da52541925650b81698c53c6824d92ff63f98353',
    RepoDigests: null,
    RepoTags: [ 'alpine-3.7-shellinabox:2018-06-28--1' ],
    SharedSize: -1,
    Size: 4147781,
    VirtualSize: 4147781 } 
    ]);
            }


            return mapi.getAsync("/root/images/list/upgradable")
              .then((re) => {
                 // console.inspect(re.result);
                 assert.deepEqual(re.result, [ { Containers: -1,
    Created: 1529237078,
    Id: 'sha256:e189adde95ba093f3a41e918c4734ae6a0cf04f9be962a8ee4d85dc6bfa61543',
    Labels: { 'com.monster-cloud.mc-app': 'ssh' },
    ParentId: 'sha256:3fd9065eaf02feaf94d68376da52541925650b81698c53c6824d92ff63f98353',
    RepoDigests: null,
    RepoTags: [ 'alpine-3.7-dropbear:2018-06-28--1' ],
    SharedSize: -1,
    Size: 4147781,
    VirtualSize: 4147781,
    PrimaryTag: 'alpine-3.7-dropbear:2018-06-28--1',
    upgradeableTo: "alpine-3.7-dropbear:2018-06-28--2"
     } ]);
              })
        });


        it('creating a new php container', function() {
            var container_id = "php-container-id1";

            app.docker.listImages = function(optsf) {
               // console.inspect(optsf)
               assert.deepEqual(optsf, { filters: { label: [ 'com.monster-cloud.mc-app' ] } });
               return Promise.resolve([{ Containers: -1,
    Created: 1529237078,
    Id: 'sha256:e189adde95ba093f3a41e918c4734ae6a0cf04f9be962a8ee4d85dc6bfa61543',
    Labels: { "com.monster-cloud.mc-app": 'php',"com.monster-cloud.mc-webapp": 'php-fpm' },
    ParentId: 'sha256:3fd9065eaf02feaf94d68376da52541925650b81698c53c6824d92ff63f98353',
    RepoDigests: null,
    RepoTags: [ 'stretch-php-7.2:2018-06-28--1' ],
    SharedSize: -1,
    Size: 4147781,
    VirtualSize: 4147781 } 
    ]);
            }

            var labels;
            var containerCreated = 0;
            var containerStarted = 0;
            var containerRan = 0;

             var hostAddEntry = 0;
             var oHostsLib = app.hostsLib;
             app.hostsLib = {
                appendEntry: function(hostname, ip){
                   assert.equal(hostname, "php_leavitt");
                   assert.equal(ip, "123.123.123.123");
                   hostAddEntry++;
                   return Promise.resolve();
                },
             };

            var emailCalled = 0;
            var oGetEmail = app.GetEmail;
            app.GetEmail = function(){
               return {
                  postAsync: function(url, data){
                     emailCalled++;
                     assert.equal(url, "/s/[this]/email/accounts/mail-service-account/fetch");
                     assert.deepEqual(data, { wh_id: webhosting_id });
                     return Promise.resolve({result:{ea_email: "email", ea_password: "password"}})
                  }
               }
            }

                       
            var oCreate = app.docker.createContainer;
            app.docker.createContainer = function(aoptsf){

               var optsf = simpleCloneObject(aoptsf);

               // console.inspect(optsf, 123)

               containerCreated++;

               assert.ok(optsf.HostConfig.Mounts[0].Source);
               delete optsf.HostConfig.Mounts[0].Source;

               assert.ok(optsf.HostConfig.Binds[0].endsWith(":/host"));
               optsf.HostConfig.Binds[0] = "ok";

               assert.ok(optsf.Labels[app.mcContainerNameLabel]);
               delete optsf.Labels[app.mcContainerNameLabel];

               assert.deepEqual(
                  optsf,
                  { AttachStdin: false,
  AttachStdout: false,
  AttachStderr: false,
  Tty: false,
  OpenStdin: false,
  StdinOnce: false,
  Labels: { 
    "com.monster-cloud.mc-app": 'php', // this label was inherited from the image
    "com.monster-cloud.mc-webapp": 'php-fpm', // this label was inherited from the image
    'com.monster-cloud.webstore': ""+webhosting_id , // this one was added to indicate the container it belongs to
    'com.monster-cloud.user': c_user_id,
  },
  ExposedPorts: {},
  HostConfig:
   { 
     Dns: ["1.2.3.4"],
     DnsSearch: ["."],
     NetworkMode: "host",
     CapAdd: ["SYS_PTRACE"],
     ExtraHosts:
      [ 'server-name:9.8.7.6',
        'server-name.fooo.hu:9.8.7.6',
        'extra.foobar.hu:9.8.7.6' ],
     LogConfig: { "Type": "json-file", "Config": {"max-size": "3m", "max-file": "2"} },
     // Tmpfs: { '/tmp': 'rw,noexec,nosuid,size=65536k' },
     ShmSize: 4194304,
     Memory: 1073741824,
     MemorySwap: 1073741824,
     Ulimits: [ { Name: 'nproc', Soft: 64, Hard: 64 } ] ,
     Binds: [
     'ok',
     '/opt/MonsterDocker/lib/misc/cpuinfo.txt:/proc/cpuinfo:ro',
     '/web/w3/12345-abvashgdasda:/web',
     '/var/run/mysqld:/var/run/mysqld'
     ],
     Mounts: [
       {
          Type: 'bind',
          Target: '/var/run/mc-app-socket' 
       } 
     ],
     PortBindings: { },
     RestartPolicy: { Name: 'unless-stopped' },
  },
  User: webhosting_id+':1002',
  Env: [],
  Cmd: [],
  Image: 'stretch-php-7.2:2018-06-28--1' }

               );

               labels = optsf.Labels;

               return Promise.resolve({
                  id: container_id,
                  inspect: function(){
                      return Promise.resolve({
                         Id: container_id,
                         Names: [ "/php_leavitt", ],
                        State: {
                           Status: "running",
                        },
                           NetworkSettings: {
                              IPAddress: "123.123.123.123",
                           },
                         Config: {
                           Labels: labels,
                         }
                      })
                  },
                  start: function(){
                     containerStarted++;
                     return Promise.resolve();
                  }
               }); // note the lowercased id here
            }

            return mapi.postAsync("/webhosting/"+webhosting_id+"/containers/new", {
               image: "stretch-php-7.2:2018-06-28--1",
               start: true,
            })
              .then(re => {
                 // note: the docker response is filtered, some of the fields are stripped
                 assert.deepEqual(re.result, {container: container_id});
                 assert.equal(containerCreated, 1);
                 assert.equal(containerStarted, 1);
                 assert.equal(hostAddEntry, 1);
                 assert.equal(emailCalled, 1);

                 app.hostsLib = oHostsLib;

                 app.docker.listImages = oImages;
                 app.docker.createContainer = oCreate;

                 app.GetEmail = oGetEmail;

                 const expectedFpmConf = 'some-line\n\nrequest_terminate_timeout = 300s\nphp_value[max_execution_time] = 300\n';
                 var webstoreConfig = app.getWebstoreConfig(webhosting_id, container_id);
                 assert.equal(webstoreConfig.wh_php_fpm_conf_extra_lines, expectedFpmConf)
                 assert.ok(webstoreConfig.docrootParams.unix_socket_path);
                 var phpfpm = fs.readFileSync(path.join(webstoreConfig.volumeInfo.path, "host", "php-fpm.conf"), "utf8");
                 assert.ok(phpfpm.indexOf("slowlog = /web/php-fpm-logs/") > 0);
                 assert.ok(phpfpm.indexOf(expectedFpmConf) > 0);
                 var msmtp = fs.readFileSync(path.join(webstoreConfig.volumeInfo.path, "host","msmtp.conf"), "utf8");
                 var expectedMsmtpConf= "\naccount provider\nhost server-name.fooo.hu\nauth plain\nfrom "+webhosting_id+"@server-name.fooo.hu\nuser email\npassword password\n\n# Set a default account\naccount default : provider\n";
                 assert.equal(msmtp, expectedMsmtpConf);
              })

        });


        it("upgrading the php container just created", function(){

            const old_container_id = "php-container-id1";
            const new_container_id = "php-container-id2";

            var destination_image = "stretch-php-7.2:2018-06-28--3";

            var labels;
            var containerInspected = 0;
            var containerCreated = 0;
            var containerStarted = 0;
            var containerStopped = 0;
            var containerRemoved = 0;

             var hostAddEntry = 0;
             var hostRemoveEntry = 0;

             var oHostsLib = app.hostsLib;
             app.hostsLib = {
                appendEntry: function(hostname, ip){
                   assert.equal(hostname, "php_leavitt_new");
                   assert.equal(ip, "123.123.123.123");
                   hostAddEntry++;
                   return Promise.resolve();
                },
                removeEntry: function(hostname){
                   assert.equal(hostname, "php_leavitt_old");
                   hostRemoveEntry++;
                   return Promise.resolve();
                },
             };

             var oList = app.docker.listContainers;
             app.docker.listContainers = function(optsf){
               // console.inspect(optsf)
               assert.deepEqual(optsf, { 
                  all: true,
                  filters: { label: [ 'com.monster-cloud.webstore='+webhosting_id,
                    'com.monster-cloud.mc-app',
                  ] } 
               });
               return Promise.resolve(
                  [ { Id: 'php-container-id1',
                      Names: [ '/php_leavitt_old' ],
                      Image: 'stretch-php-7.2:2018-06-28--2',
                      ImageID: 'sha256:3fd9065eaf02feaf94d68376da52541925650b81698c53c6824d92ff63f98353',
                      Command: '/bin/sh',
                      Created: 1529237902,
                      Ports: [],
                      Labels: {
                         'com.monster-cloud.webstore': ""+webhosting_id,
                         'com.monster-cloud.user': c_user_id,
                      },
                      State: 'running',
                      Status: 'Up 31 seconds',
                      HostConfig: { NetworkMode: 'default' },
                      NetworkSettings: { Networks: [{foo:"bar"}] },
                      Mounts: [] } ]
                )
            }

            var oListImages = app.docker.listImages;
            app.docker.listImages = function(optsf) {
               // console.inspect(optsf)
               assert.deepEqual(optsf, { filters: { label: [ 'com.monster-cloud.mc-app' ] } });
               return Promise.resolve([
{ Containers: -1,
    Created: 1529237078,
    Id: 'sha256:e189adde95ba093f3a41e918c4734ae6a0cf04f9be962a8ee4d85dc6bfa61543',
    Labels: { 
      "com.monster-cloud.mc-app": 'php',
      "com.monster-cloud.mc-webapp": 'php-fpm',
    },
    ParentId: 'sha256:3fd9065eaf02feaf94d68376da52541925650b81698c53c6824d92ff63f98353',
    RepoDigests: null,
    RepoTags: [ 'stretch-php-7.2:2018-06-28--2' ],
    SharedSize: -1,
    Size: 4147781,
    VirtualSize: 4147781 },
{ Containers: -1,
    Created: 1529237078,
    Id: 'sha256:e189adde95ba093f3a41e918c4734ae6a0cf04f9be962a8ee4d85dc6bfa61543',
    Labels: { 
      "com.monster-cloud.mc-app": 'php',
      "com.monster-cloud.mc-webapp": 'php-fpm',
    },
    ParentId: 'sha256:3fd9065eaf02feaf94d68376da52541925650b81698c53c6824d92ff63f98353',
    RepoDigests: null,
    RepoTags: [ 'stretch-php-7.2:2018-06-28--3' ],
    SharedSize: -1,
    Size: 4147781,
    VirtualSize: 4147781 },
    ]);
            }

             var oGet = app.docker.getContainer;
             app.docker.getContainer = function(optsf){
               assert.equal(optsf, old_container_id);
               return {
                  inspect: function(){
                      containerInspected++;
                      return Promise.resolve({
                        Id: old_container_id,
                        Names: ["/php_leavitt_old"],
                        Image: 'stretch-php-7.2',
                        State: {
                           Status: "running",
                        },
                        NetworkSettings: { 
                          IPAddress: "123.123.123.123",
                          Networks: [{foo:"bar"}] 
                        },
                        Config: {
                        }
                     });
                  },
                  stop: function(){
                      containerStopped++;
                      return Promise.resolve();
                  },
                  remove: function(){
                      containerRemoved++;
                      return Promise.resolve();                    
                  }
               };
            }

            var oCreate = app.docker.createContainer;
            app.docker.createContainer = function(aoptsf){
               // console.inspect(aoptsf, 123)
               containerCreated++;

               var optsf = simpleCloneObject(aoptsf);

               assert.ok(optsf.HostConfig.Binds[0].endsWith(":/host"));
               optsf.HostConfig.Binds[0] = "ok";

               assert.ok(optsf.HostConfig.Mounts[0].Source);
               delete optsf.HostConfig.Mounts[0].Source;

               assert.ok(optsf.Labels[app.mcContainerNameLabel]);
               delete optsf.Labels[app.mcContainerNameLabel];

               assert.deepEqual(
                  optsf,
                  { AttachStdin: false,
  AttachStdout: false,
  AttachStderr: false,
  Tty: false,
  OpenStdin: false,
  StdinOnce: false,
  Labels: { 
     'com.monster-cloud.mc-app': 'php',
     'com.monster-cloud.mc-webapp': 'php-fpm',
     'com.monster-cloud.webstore': ""+webhosting_id,
     'com.monster-cloud.user': c_user_id,
  },
  ExposedPorts: {},
  HostConfig:
   { 
     Dns: ["1.2.3.4"],
     DnsSearch: ["."],
     NetworkMode: "host",
     CapAdd: ["SYS_PTRACE"],
     ExtraHosts:
      [ 'server-name:9.8.7.6',
        'server-name.fooo.hu:9.8.7.6',
        'extra.foobar.hu:9.8.7.6' ],
     LogConfig: { "Type": "json-file", "Config": {"max-size": "3m", "max-file": "2"} },
     // Tmpfs: { '/tmp': 'rw,noexec,nosuid,size=65536k' },
     ShmSize: 4194304,
     Memory: 1073741824,
     MemorySwap: 1073741824,
     Ulimits: [ { Name: 'nproc', Soft: 64, Hard: 64 } ] ,
     Binds: [
       "ok",
       "/opt/MonsterDocker/lib/misc/cpuinfo.txt:/proc/cpuinfo:ro",
       "/web/w3/12345-abvashgdasda:/web",
       '/var/run/mysqld:/var/run/mysqld'
     ],
     Mounts: [
       {
          Type: 'bind',
          Target: '/var/run/mc-app-socket' 
       } 
     ],
     PortBindings: {  },
     RestartPolicy: { Name: 'unless-stopped' },
  },
  User: webhosting_id+':1002',
  Env: [],
  Cmd: [],
  Image: destination_image }

               );

               labels = optsf.Labels;

               return Promise.resolve({
                  id: new_container_id,
                  inspect: function(){
                     containerInspected++;
                     return Promise.resolve({
                        Id: new_container_id,
                        NetworkSettings: { 
                          IPAddress: "123.123.123.123",
                          Networks: [{foo:"bar"}] 
                        },
                        Names: ["/php_leavitt_new"],
                        State: {
                           Status: "running",
                        },
                        Config: {
                           Labels: labels,
                        }
                     });
                  },
                  start: function(){
                     containerStarted++;
                     return Promise.resolve();
                  }
               }); // note the lowercased id here
            }


            var docrootCalls = 0;
            var oDocroot = app.GetDocroot;
            app.GetDocroot = function(){
               return {
                  postAsync: function(url, payload){
                      console.log("docroot call", url, payload);
                      assert.equal(url, "/s/[this]/docrootapi/docroots/webapps/10001/upgrade");
                      assert.ok(payload.newParams.unix_socket_path);
                      assert.ok(payload.newParams.unix_socket_path.startsWith('/var/run/mc-app-sockets/10001-'));
                      assert.ok(payload.newParams.unix_socket_path.endsWith('/socket'));
                      delete payload.newParams.unix_socket_path;

                      assert.deepEqual(payload, 
                        { oldContainer: 'php_leavitt_old',
                          newParams: { 
                            container: 'php_leavitt_new',
                          } 
                        }
                      );
                      docrootCalls++;
                      return Promise.resolve();
                  }
               }
            }


            return mapi.postAsync("/webhosting/"+webhosting_id+"/container/"+old_container_id+"/upgrade", 
                {destination_image: destination_image, removeOldContainerImmediately: true}
              )
              .then(re => {
                 assert.deepEqual(re.result, {
                    container:new_container_id
                 });
                 assert.equal(containerCreated, 1);
                 assert.equal(containerStarted, 1);
                 assert.equal(containerStopped, 1);
                 assert.equal(containerRemoved, 1);
                 assert.equal(containerInspected, 6);
                 assert.equal(docrootCalls, 1);
                 assert.equal(hostRemoveEntry, 1);
                 assert.equal(hostAddEntry, 1);

                 app.docker.createContainer = oCreate;
                 app.docker.getContainer = oGet;
                 app.docker.listContainers = oList;
                 app.docker.listImages = oListImages;

                 app.GetDocroot = oDocroot;

              })
        })

        deleteContainer("php-container-id2");



        function deleteContainer(container_id) {

            it('deleting a container: '+container_id, function() {

                 var hostRemoveEntry = 0;
                 var oHostsLib = app.hostsLib;
                 app.hostsLib = {
                    removeEntry: function(hostname){
                       assert.equal(hostname, "adoring_leavitt");
                       hostRemoveEntry++;
                       return Promise.resolve();
                    }
                 };

                 const containerInfo = { Id: container_id,
                          Names: [ '/adoring_leavitt' ],
                          Image: 'alpine',
                          ImageID: 'sha256:3fd9065eaf02feaf94d68376da52541925650b81698c53c6824d92ff63f98353',
                          Command: '/bin/sh',
                          Created: 1529237902,
                          Ports: [],
                          Labels: {
                            'com.monster-cloud.webstore': ""+webhosting_id,
                            'com.monster-cloud.user': c_user_id,
                            'com.monster-cloud.mc-webapp': 'webserver',
                          },
                          State: 'stopped',
                          Status: 'stopped 31 seconds',
                          HostConfig: { NetworkMode: 'default' },
                          NetworkSettings: { Networks: [{foo:"bar"}] },
                          Mounts: [] } ;
                 containerInfo.Config = containerInfo;

                  var docrootCalls = 0;
                  var oDocroot = app.GetDocroot;
                  app.GetDocroot = function(){
                     return {
                        postAsync: function(url, payload){
                            console.log("docroot call", url, payload);
                            assert.equal(url, "/s/[this]/docrootapi/docroots/webapps/detach");

                            assert.deepEqual(payload, 
                              { oldContainer: 'adoring_leavitt' }
                            );
                            docrootCalls++;
                            return Promise.resolve();
                        }
                     }
                  }



                 app.docker.listContainers = function(optsf){
                   // console.inspect(optsf)
                   assert.deepEqual(optsf, { 
                      all: true,
                      filters: { label: [ 
                         'com.monster-cloud.webstore='+webhosting_id,
                         'com.monster-cloud.mc-app', 
                      ] } 
                   });
                   return Promise.resolve(
                      [ containerInfo ]
                    )
                }

                var oGetContainer = app.docker.getContainer;
                app.docker.getContainer = function(id){
                   assert.equal(id, container_id);
                   return {
                     remove: function(){
                        return Promise.resolve();
                     },
                     inspect: function(){
                        return Promise.resolve(containerInfo)
                     }
                   }
                }

                return mapi.postAsync("/webhosting/"+webhosting_id+"/container/"+container_id+"/remove", {})
                  .then(re => {
                     assert.equal(re.result, "ok");
                     app.docker.getContainer = oGetContainer;
                     assert.equal(hostRemoveEntry, 1);
                     assert.equal(docrootCalls, 1);
                     app.hostsLib = oHostsLib;
                     app.GetDocroot = oDocroot;
                  })

            })

        }


        function createDropBearContainer(container_name, imageWanted){
          if(!container_name) container_name = "new_container_id";
        it('creating a new dropbear container in regular simple mode: '+container_name, function() {

            app.docker.listImages = function(optsf) {
               // console.inspect(optsf)
               assert.deepEqual(optsf, { filters: { label: [ 'com.monster-cloud.mc-app' ] } });
               return Promise.resolve([{ Containers: -1,
    Created: 1529237078,
    Id: 'sha256:e189adde95ba093f3a41e918c4734ae6a0cf04f9be962a8ee4d85dc6bfa61543',
    Labels: { "com.monster-cloud.mc-app": 'ssh' },
    ParentId: 'sha256:3fd9065eaf02feaf94d68376da52541925650b81698c53c6824d92ff63f98353',
    RepoDigests: null,
    RepoTags: [ 'alpine-3.7-dropbear:2018-06-28--1' ],
    SharedSize: -1,
    Size: 4147781,
    VirtualSize: 4147781 } 
    ]);
            }

            var labels;
            var containerCreated = 0;
            var containerStarted = 0;
            var containerRan = 0;

             var hostAddEntry = 0;
             var oHostsLib = app.hostsLib;
             app.hostsLib = {
                appendEntry: function(hostname, ip){
                   assert.equal(hostname, "fadoring_leavitt");
                   assert.equal(ip, "123.123.123.123");
                   hostAddEntry++;
                   return Promise.resolve();
                }
             };

            var oRun = app.docker.run;
            app.docker.run = function(image, someparam, streams, extraparams, callback){
               assert.ok(extraparams.HostConfig.Binds[0]);
               extraparams.HostConfig.Binds[0] = "/hosto:/host";
               assert.equal(image, "alpine-3.7-dropbear:2018-06-28--1")
               // console.inspect(extraparams);
               assert.deepEqual(extraparams, { EntryPoint: ['/usr/local/bin/genkeys', ""+webhosting_id],
                  Tty: false,
                  HostConfig: {
                     Binds: ["/hosto:/host"]
                  }
               });
               containerRan++;
               callback(null, {StatusCode: 0});
            } 
                       
            var oCreate = app.docker.createContainer;
            app.docker.createContainer = function(aoptsf){

               var optsf = simpleCloneObject(aoptsf);

               var hostDir = optsf.HostConfig.Binds[0];
               var hostIndex = hostDir.indexOf(":/host");
               assert.ok(hostIndex > -1);
               hostDir = hostDir.substr(0, hostIndex);
               console.log("hostDir", hostDir);
               optsf.HostConfig.Binds[0] = "/hosto:/host"

               var pwdFile = optsf.HostConfig.Binds[1];
               console.log("passwd line", pwdFile);
               var passwdIndex = pwdFile.indexOf(":/etc/passwd");
               assert.ok(passwdIndex > -1);
               pwdFile = pwdFile.substr(0, passwdIndex);
               optsf.HostConfig.Binds[1] = "/passwdo:/etc/passwd";

               assert.ok(optsf.Labels[app.mcContainerNameLabel]);
               delete optsf.Labels[app.mcContainerNameLabel];

               // console.inspect(optsf, 123)

               containerCreated++;

               assert.deepEqual(
                  optsf,
                  { AttachStdin: false,
  AttachStdout: false,
  AttachStderr: false,
  Tty: false,
  OpenStdin: false,
  StdinOnce: false,
  Labels: { 
    'com.monster-cloud.shellType': 'simple',
    "com.monster-cloud.mc-app": 'ssh', // this label was inherited from the image
    'com.monster-cloud.webstore': ""+webhosting_id , // this one was added to indicate the container it belongs to
    'com.monster-cloud.user': c_user_id,
    'com.monster-cloud.hostPort': "50001",
  },
  ExposedPorts: {},
  HostConfig:
   { 
     Dns: ["1.2.3.4"],
     DnsSearch: ["."],
     NetworkMode: "host",
     ExtraHosts:
      [ 'server-name:9.8.7.6',
        'server-name.fooo.hu:9.8.7.6',
        'extra.foobar.hu:9.8.7.6' ],
     LogConfig: { "Type": "json-file", "Config": {"max-size": "3m", "max-file": "2"} },
     // Tmpfs: { '/tmp': 'rw,noexec,nosuid,size=65536k' },
     ShmSize: 4194304,
     Memory: 268435456,
     MemorySwap: 268435456,
     Ulimits: [ { Name: 'nproc', Soft: 64, Hard: 64 } ] ,
     Binds: [
     '/hosto:/host',
     '/passwdo:/etc/passwd',
     '/opt/MonsterDocker/lib/misc/cpuinfo.txt:/proc/cpuinfo:ro',
     '/web/w3/12345-abvashgdasda:/web'
     ],
     Mounts: [],
     PortBindings: { },
     RestartPolicy: { Name: 'unless-stopped' },
  },
  User: webhosting_id+':1002',
  Env: [],
  Cmd: ["50001"],
  Image: 'alpine-3.7-dropbear:2018-06-28--1' }

               );

               var publicKeysPath = path.join(hostDir, "home/.ssh/authorized_keys");
               var actualPublicKeys = fs.readFileSync(publicKeysPath, "utf8");
               assert.equal(actualPublicKeys.trim(), goodSshKeyLine.trim());

               labels = optsf.Labels;

               return Promise.resolve({
                  id: container_name,
                  inspect: function(){
                      return Promise.resolve({
                         Id: container_name,
                         Name: "/fadoring_leavitt",
                        State: {
                           Status: "running",
                        },
                           NetworkSettings: {
                              IPAddress: "123.123.123.123",
                           },
                         Config: {
                           Labels: labels,
                         }
                      })
                  },
                  start: function(){
                     containerStarted++;
                     return Promise.resolve();
                  }
               }); // note the lowercased id here
            }

            return mapi.postAsync("/webhosting/"+webhosting_id+"/containers/new", {
               image: imageWanted || "alpine-3.7-dropbear:2018-06-28--1",
               type: "simple",
               publicKeys: goodSshKeyLine,
               start: true,
            })
              .then(re => {
                 // note: the docker response is filtered, some of the fields are stripped
                 assert.deepEqual(re.result, {hostPort:50001,ssh:{StatusCode:0, stdout:"", stderr:""},container:container_name});
                 assert.equal(containerCreated, 1);
                 assert.equal(containerStarted, 1);
                 assert.equal(containerRan, 1);
                 assert.equal(hostAddEntry, 1);

                 app.hostsLib = oHostsLib;

                 app.docker.listImages = oImages;
                 app.docker.createContainer = oCreate;
                 app.docker.oRun = oRun;
              })

        })
        }

    })

    describe("mass upgrades", function(){

        it("listing containers that could be upgraded", function(){

             app.docker.listContainers = function(optsf){
               // console.inspect(optsf)
               assert.deepEqual(optsf, { 
                  all: true,
                  filters: { label: [
                    'com.monster-cloud.mc-app', 
                  ] } 
               });
               return Promise.resolve(
                  [ { Id: '73b2040f3d2da845c6f1f285820f9753d16378cc00aac02a729069ef162782a9',
                      Names: [ '/adoring_leavitt' ],
                      Image: 'alpine-3.7-dropbear:2018-06-28--1',
                      ImageID: 'sha256:3fd9065eaf02feaf94d68376da52541925650b81698c53c6824d92ff63f98353',
                      Command: '/bin/sh',
                      Created: 1529237902,
                      Ports: [],
                      Labels: {},
                      State: 'running',
                      Status: 'Up 31 seconds',
                      HostConfig: { NetworkMode: 'default' },
                      NetworkSettings: { 
                        Networks: [{foo:"bar"}] 
                      },
                      Mounts: [] } ]
                )
            }

            app.docker.listImages = function(optsf) {
               // console.inspect(optsf)
               assert.deepEqual(optsf, { filters: { label: [ 'com.monster-cloud.mc-app' ] } });
               return Promise.resolve([{ Containers: -1,
    Created: 1529237078,
    Id: 'sha256:e189adde95ba093f3a41e918c4734ae6a0cf04f9be962a8ee4d85dc6bfa61543',
    Labels: { "com.monster-cloud.mc-app": 'ssh' },
    ParentId: 'sha256:3fd9065eaf02feaf94d68376da52541925650b81698c53c6824d92ff63f98353',
    RepoDigests: null,
    RepoTags: [ 'alpine-3.7-dropbear:2018-06-28--1' ],
    SharedSize: -1,
    Size: 4147781,
    VirtualSize: 4147781 } ,
  { Containers: -1,
    Created: 1529237078,
    Id: 'sha256:a189adde95ba093f3a41e918c4734ae6a0cf04f9be962a8ee4d85dc6bfa61543',
    Labels: { "com.monster-cloud.mc-app": 'ssh' },
    ParentId: 'sha256:afd9065eaf02feaf94d68376da52541925650b81698c53c6824d92ff63f98353',
    RepoDigests: null,
    RepoTags: [ 'alpine-3.7-dropbear:2018-06-28--2' ],
    SharedSize: -1,
    Size: 4147781,
    VirtualSize: 4147781 } 
    ]);
            }


            return mapi.getAsync("/root/containers/list/upgradable")
              .then((re) => {
                 // console.inspect(re.result);
                 assert.deepEqual(re.result, [ { Id: '73b2040f3d2da845c6f1f285820f9753d16378cc00aac02a729069ef162782a9',
    Names: [ '/adoring_leavitt' ],
    PrimaryName: "adoring_leavitt",
    Image: 'alpine-3.7-dropbear:2018-06-28--1',
    ImageID: 'sha256:3fd9065eaf02feaf94d68376da52541925650b81698c53c6824d92ff63f98353',
    Command: '/bin/sh',
    Created: 1529237902,
    Ports: [],
    Labels: {},
    State: 'running',
    Status: 'Up 31 seconds',
    HostConfig: { NetworkMode: 'default' },
    NetworkSettings: { Networks: [ { foo: 'bar' } ] },
    Mounts: [],
    upgradeableTo: 'alpine-3.7-dropbear:2018-06-28--2' } ])
              })
        });

    })


}, 10000)


