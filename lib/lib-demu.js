module.exports = function() {

// cut from here
	const EXIT_CODE = 0;
	const STDOUT_CODE = 1;
	const STDERR_CODE = 2;

	function demu(stream, out, err, exitCallback){
	  if(!out) out = process.stdout;
	  if(!err) err = process.stderr;

	  var window = new Buffer(0);
	  stream.on("data", function(buf) {

	     window = Buffer.concat([window, buf]);
	     processWindow();
	  });

	  function processWindow() {
	     if(window.byteLength <= 4) return; // not long enough
	     var frameType = window.readUInt8(0);
	     var frameSize = window.readUInt32LE(1);
	     var endPos = frameSize + 5;
	     if(endPos > window.byteLength) return; // not received the whole stuff yet
	     // we have got a complete frame!
	     if(frameType == EXIT_CODE) {
	       var ec = window.readUInt32LE(5);
	       if(exitCallback) return exitCallback(ec);
	       return process.exit(ec);
	     }

	     var rawData = window.slice(5, endPos);
	     (frameType == STDOUT_CODE ? out : err).write(rawData);
	     window = window.slice(endPos);
	     return processWindow(); // there might be one more in the window
	  }
	}
// cut to here

   return demu;	
}
