#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <time.h>
#include <stdarg.h>
#include <errno.h>

#include <netdb.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/un.h>

#include <sys/select.h>

#define MAX_MSG_SIZE 8192
#define DEFAULT_DSH_SOCKET_PATH "/var/run/dsh/unix.sock"

#define STDIN_NO 0

#define ATTACH_STDIN -255
#define EXIT_CODE_DEFAULT -254

#define CODE_EXIT 255
#define CODE_STDIN  0
#define CODE_STDOUT 1
#define CODE_STDERR 2

#define CODE_AUTH 0

static int DEBUG_MODE = 0;

void debug(const char *fmt, ...) {
    struct timespec ts;

    if(!DEBUG_MODE) return;

    timespec_get(&ts, TIME_UTC);
    char time_buf[100];
    size_t rc = strftime(time_buf, sizeof time_buf, "%D %T", gmtime(&ts.tv_sec));
    snprintf(time_buf + rc, sizeof time_buf - rc, ".%06ld UTC", ts.tv_nsec / 1000);
 
    va_list args1;
    va_start(args1, fmt);
    va_list args2;
    va_copy(args2, args1);
    char buf[1+vsnprintf(NULL, 0, fmt, args1)];
    va_end(args1);
    vsnprintf(buf, sizeof buf, fmt, args2);
    va_end(args2);

    fprintf(stderr, "%s [debug]: %s\n", time_buf, buf);
}

int unixBuildAddress(const char *path, struct sockaddr_un *addr)
{
    if (addr == NULL || path == NULL) {
        errno = EINVAL;
        return -1;
    }

    memset(addr, 0, sizeof(struct sockaddr_un));
    addr->sun_family = AF_UNIX;
    if (strlen(path) < sizeof(addr->sun_path)) {
        strncpy(addr->sun_path, path, sizeof(addr->sun_path) - 1);
        return 0;
    } else {
        errno = ENAMETOOLONG;
        return -1;
	}
}

int connectToUnixSocket(const char* path) {

    int sd, savedErrno;
    struct sockaddr_un addr;

    if (unixBuildAddress(path, &addr) == -1) {
    	perror("unixBuildAddress");
        return -1;
    }

    sd = socket(AF_UNIX, SOCK_STREAM, 0);
    if (sd == -1) {
    	perror("socket");
        return -1;
    }

    if (connect(sd, (struct sockaddr *) &addr,
                sizeof(struct sockaddr_un)) == -1) {
        savedErrno = errno;
        close(sd);                      /* Might change 'errno' */
        errno = savedErrno;
        perror("connect");
        return -1;
    }

    return sd;	
}

int connectToDshServer(){
   char * path = getenv("DSH_SOCKET_PATH");
   if(path == NULL) path = DEFAULT_DSH_SOCKET_PATH;
   return connectToUnixSocket(path);
}


char * addParams(char *target, int *bytesWritten, int *remainingSpace, ...) {
	va_list args;
    va_start(args, remainingSpace);
    int nk;
    while(1) {    	
        char * key = va_arg(args, char*);
        if(key == NULL) break;
        nk = strlen(key) + 1;

        if(nk >= *remainingSpace) {
        	debug("Not enough space for constructing the params");
        	return NULL;
        }

        memcpy(target, key, nk);
        target += nk;

        *remainingSpace = *remainingSpace - nk;
        *bytesWritten = *bytesWritten + nk;
    }
    va_end(args);

    return target;
}

int buildCommand(char *target, int *msgSize, int argc, char* argv[]){
   char* secret;
   char* rest;
   int n;
   int32_t keyValueSize = 0;
   int size = *msgSize;

   if(argc < 2) {
   	  fprintf(stderr, "(experimental) Docker Shell client helper utility on the Monster Media shared webhosting platform\n");
   	  fprintf(stderr, "\n");
   	  fprintf(stderr, "Usage:\n");
      fprintf(stderr, "dsh ps                        : list running containers\n");
      fprintf(stderr, "dsh exec <cid> <command>      : execute command in remote container\n");
      fprintf(stderr, "dsh exec dc4916105033 ls -la  : list files in dc4916105033\n");
      fprintf(stderr, "\n");
      return 1;
   }

   secret = getenv("DSH_SECRET");
   if (secret == NULL) {
   	  fprintf(stderr, "dsh: secret not found\n");   	
   	  return 2;
   }

   target[0] = CODE_AUTH;
   size = size - 10;


   if((0 == strcmp(argv[1], "ps")) || (0 == strcmp(argv[1], "ping"))) {
   	  if(argc != 2) {
   	     fprintf(stderr, "dsh: %s does not support any arguments\n", argv[1]);
   	  	 return 3;
   	  }
   	  if(NULL == addParams(target+5, &keyValueSize, &size, "secret", secret, "cmd", argv[1], NULL)) {
   	  	 return 4;
   	  }
   }
   else
   if(0 == strcmp(argv[1], "exec")) {
   	  if(argc <= 3) {
   	     fprintf(stderr, "dsh: exec expects additional parameters (container id and the command to be executed)\n");
   	  	 return 3;
   	  }

      rest = addParams(target+5, &keyValueSize, &size, "secret", secret, "cmd", "exec", "container", argv[2], NULL);
   	  if(NULL == rest) {
   	  	 return 4;
   	  }
   	  for(n = 3; n < argc; n++) {
          rest = addParams(rest, &keyValueSize, &size, "p", argv[n], NULL);
	   	  if(NULL == rest) {
	   	  	 return 4;
	   	  }
   	  }
   }
   else {
   	 fprintf(stderr, "dsh: Unsupported command type: %s\n", argv[1]);
   	 return 4;
   }

   memcpy(target+1, &keyValueSize, 4);
   *msgSize = keyValueSize + 5;
   return 0;
}

int writeFully(int sockfd, char *buf, size_t nbytes) {
   int n;

   while(1) {
   	  debug("writeFully: %d bytes", nbytes);
   	  n = write(sockfd, buf, nbytes);
   	  if(n < 0) {
   	  	perror("write");
   	  	return n;
   	  }

   	  nbytes -= n;
   	  if(nbytes == 0) {
   	  	 // complete!
   	  	 return 0;
   	  }

      debug("writeFully: %d bytes remaining", nbytes);

   	  buf += nbytes;
   }

}

int readAndForward(int srcSocket, int dstSocket) {
	char tmp[MAX_MSG_SIZE];

	int d = read(srcSocket, tmp, MAX_MSG_SIZE);
    debug("readAndForward: recv returned %d",d);

	if(d < 0) {
		perror("recv stdin");
	}
	if(d <= 0) {
		return d;
	}

	// just read some data from stdin, need to forward it to the unix socket
	d = writeFully(dstSocket, tmp, d);
	if(d == 0) return 1;
	return d;
}

int dispatchData(int *re, unsigned char *window, int maxlen, int *windowOffset) {
  int type;
  int32_t msgSize;
  char * msgStart;
  int remainingBytes;

again:

  debug("dispatchData: re %d, windowOffset %d", *re, *windowOffset);

  if(*windowOffset < 5) {
  	debug("dispatchData: data is not even enough for the frame header");
    return 1; 
  }

  type = (int) window[0];
  memcpy(&msgSize, window+1, 4);
  msgStart = window+5;

  debug("dispatchData: msgSize %d, type %d", msgSize, type);

  if(msgSize +5 > *windowOffset) {
  	debug("dispatchData: not enough data yet, keep reading");
  	return 1;
  }

  if(type == CODE_EXIT) {
  	memcpy(re, msgStart, 4);
  	debug("dispatchData: exit code received: %d", *re);
  	return 0;
  }

  if((type < CODE_STDIN) || (type > CODE_STDERR)) {
  	fprintf(stderr, "dsh: Invalid frame, unsupported type: %d\n", type);
  	return -2;
  }

  if(type == CODE_STDIN) {
  	 debug("dispatchData: Server indicated that stdin can be attached");
     *re = ATTACH_STDIN;
  } 

  if((msgSize > 0)&&(0 != writeFully(type, msgStart, msgSize))) {
   	 debug("dispatchData: couldnt write it fully");
  	 return -1; // could write to the output?!
  }

  // need to move the remaining stuff back to the beginning of the window
  remainingBytes = *windowOffset - msgSize - 5;
  debug("dispatchData: remainingBytes %d", remainingBytes);
  if(remainingBytes > 0) {
     memcpy(window, window+*windowOffset, remainingBytes);
  }
  *windowOffset = remainingBytes;

  // and lets see whether there are any more frames in the buffer
  goto again;
}

int readAndDispatch(int sockfd, int *re, char *window, int maxlen, int *windowOffset) {
	int justRead;
	int remainingSpace = maxlen - *windowOffset;
	if(remainingSpace <= 0) {
		fprintf(stderr, "dsh: Not enough room in the receiver buffer\n");
		return -1;
	}
	justRead = recv(sockfd, window+*windowOffset, remainingSpace, 0);
	if(justRead < 0) 
		perror("recv unix socket");
	if(justRead <= 0)
		return justRead;
	// just read some bytes
    *windowOffset = *windowOffset+justRead;
    return dispatchData(re, window, maxlen, windowOffset);
}

int main(int argc, char* argv[]) {
   unsigned char msg[MAX_MSG_SIZE];
   int windowOffset;
   int unix_sock, i, re, tmp, msgSize;
   fd_set active_fd_set, read_fd_set;

   if(getenv("DEBUG") != NULL) {
   	  DEBUG_MODE = 1;
   }

   msgSize = MAX_MSG_SIZE;
   if(0 != buildCommand(msg, &msgSize, argc, argv)) {
   	  return -1;
   }

   unix_sock = connectToDshServer();
   if(unix_sock <= 0) {
   	  fprintf(stderr, "dsh: Unable to connect to DSH server\n");
   	  return -2;
   }

   debug("initial message: %d", msgSize);
   if(0 != writeFully(unix_sock, msg, msgSize)) {
   	  return -3;   	
   }

 
   windowOffset = 0;
   re = EXIT_CODE_DEFAULT;
   FD_ZERO (&active_fd_set);
   FD_SET (unix_sock, &active_fd_set);
   // we dont monitor stdin until the server signals us to do so
   while (1)
   {
      /* Block until input arrives on one or more active sockets. */
      read_fd_set = active_fd_set;
      if (select (FD_SETSIZE, &read_fd_set, NULL, NULL, NULL) < 0)
      {
         perror ("select");
         return -4;
      }

      debug("main: select just returned");

      /* Service all the sockets with input pending. */
      for (i = 0; i < FD_SETSIZE; ++i) {
        if (FD_ISSET (i, &read_fd_set))
        {
            debug("main: selected %d", i);
            // FD_CLR (i, &active_fd_set);

            if (i == unix_sock)
            {
        		debug("main: main unix socket selected");

            	// read something from the server
            	tmp = readAndDispatch(unix_sock, &re, msg, MAX_MSG_SIZE, &windowOffset);
            	if(0 == tmp) {
            		debug("Unix socket disconnected");
            	}
            	if(0 > tmp)
            		re = -6;
            	if(0 >= tmp)
            		goto exit;

            	if(re == ATTACH_STDIN) {
            		debug("main: Attaching stdin");
            		re = EXIT_CODE_DEFAULT;
				    FD_ZERO (&active_fd_set);
				    FD_SET (unix_sock, &active_fd_set);
				    FD_SET (STDIN_NO, &active_fd_set);
            	}

            	// otherwise keep reading.
                debug("main: keep reading from unix socket");
            }
            else
            {
        		debug("main: stdin selected");

            	// read something from stdin, need to forward it to the server
            	tmp = readAndForward(STDIN_NO, unix_sock);
            	if(0 < tmp) {
                   debug("main: keep reading from stdin");
                   continue;
            	}
            	if(0 > tmp) {
            		re = -7;
            		goto exit;
            	}

           		debug("main: stdin got closed, indicating on the socket that we are not writing it anymore");
           		shutdown(unix_sock, SHUT_WR);

           		// removing stdin from the monitored file descriptors
			    FD_ZERO (&active_fd_set);
			    FD_SET (unix_sock, &active_fd_set);

            }
        }
      }
   }

exit:
   debug("and exiting with %d", re);
   close(unix_sock);
   return re;
}
